//
//  DataCacheTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 9/8/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class DataCacheTests: XCTestCase {
  private let directory = WWCacheDirectory()
  
  class MockDirectory: CacheDirectory {
    var url: URL? = nil
    var items: [String] = []
    
    func url(for item: String) -> URL? {
      return nil
    }
    
    func clear() {
      
    }
  }
  
  func test_SaveData_GivenCachesWithSameName_ThenLoadsProperData() {
    let name = "cache"
    let expectedData = "hello world".data(using: .utf8)!
    let firstCache = WWDataCache(name: name, directory: directory)
    let secondCache = WWDataCache(name: name, directory: directory)
    let exp = expectation(description: "Data loaded")
    
    firstCache.saveData(data: expectedData).then({ data in
      return secondCache.loadData()
    }).then({ actualData in 
      XCTAssertEqual(expectedData, actualData)
      exp.fulfill()
    })
    
    waitForExpectations(timeout: 1.0, handler: nil)
  }
  
  func test_SaveData_GivenCachesWithDifferentName_ThenLoadsNoData() {
    let expectedData = "hello world".data(using: .utf8)!
    let firstCache = WWDataCache(name: "something", directory: directory)
    let secondCache = WWDataCache(name: "someotherthing", directory: directory)
    let exp = expectation(description: "Data loaded")
    
    firstCache.saveData(data: expectedData).then({ data in 
      return secondCache.loadData()
    }).then({ data in 
      XCTFail("Should fail")
    }).catch({ error in 
      exp.fulfill()
    })
    
    waitForExpectations(timeout: 1.0, handler: nil)
  }
  
  func test_ClearData_GivenCachesWithSameName_ThenRemovesData() {
    let name = "cache"
    let expectedData = "hello world".data(using: .utf8)!
    let firstCache = WWDataCache(name: name, directory: directory)
    let secondCache = WWDataCache(name: name, directory: directory)
    let exp = expectation(description: "Data cleared")
    
    firstCache.saveData(data: expectedData).then({ data in 
      return firstCache.clearData()
    }).then({
      return secondCache.loadData()
    }).then({ data in 
      XCTFail("Should fail")
    }).catch({ error in 
      exp.fulfill()
    })
    
    waitForExpectations(timeout: 1.0, handler: nil)
  }
  
  func test_ClearData_GivenCachesWithDifferentName_ThenDoesNotRemoveData() {
    let expectedData = "hello world".data(using: .utf8)!
    let firstCache = WWDataCache(name: "something", directory: directory)
    let secondCache = WWDataCache(name: "someotherthing", directory: directory)
    
    let exp = expectation(description: "Data loaded")
    
    firstCache.saveData(data: expectedData).then({ data in
      return secondCache.clearData()
    }).then({
      return firstCache.loadData()
    }).then({ actualData in 
      XCTAssertEqual(expectedData, actualData)
      exp.fulfill()
    }).catch({ error in 
      XCTFail("Should succeed")
    })
    
    waitForExpectations(timeout: 1.0, handler: nil)
  }
  
  func test_SaveData_GivenInvalidDirectory_ThenFails() {
    let cache = WWDataCache(name: "cache", directory: MockDirectory())
    let exp = expectation(description: "Data loaded")
    
    cache.saveData(data: Data()).then({ data in 
      XCTFail("Should not succeed")
    }).catch({ error in 
      if case DataCacheError.invalidUrl = error {
        exp.fulfill()
      }
    })
    
    waitForExpectations(timeout: 1.0, handler: nil)
  }
  
  func test_LoadData_GivenInvalidDirectory_ThenFails() {
    let cache = WWDataCache(name: "cache", directory: MockDirectory())
    let exp = expectation(description: "Data loaded")
    
    cache.loadData().then({ data in 
      XCTFail("Should not succeed")
    }).catch({ error in 
      if case DataCacheError.invalidUrl = error {
        exp.fulfill()
      }
    })
    
    waitForExpectations(timeout: 1.0, handler: nil)
  }
  
  func test_ClearData_GivenInvalidDirectory_ThenFails() {
    let cache = WWDataCache(name: "cache", directory: MockDirectory())
    let exp = expectation(description: "Data loaded")
    
    cache.clearData().then({ data in 
      XCTFail("Should not succeed")
    }).catch({ error in 
      if case DataCacheError.invalidUrl = error {
        exp.fulfill()
      }
    })
    
    waitForExpectations(timeout: 1.0, handler: nil)
  }
}

