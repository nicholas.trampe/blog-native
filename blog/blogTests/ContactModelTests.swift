//
//  ContactModelTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import Promises
@testable import blog

class ContactModelTests: XCTestCase {
  class MockContactService: ContactService, Mock {
    var calls: [Call] = []
    var promise = Promise<Data>(Data())
    
    func contact(name: String, email: String, message: String) -> Promise<Data> {
      calls.append(Call(name: #function, arguments: [name, email, message]))
      return promise
    }
  }
  
  class MockObserver: ContactModelObserver, Mock {
    var calls: [Call] = []
    
    func didMoveToNextField(field: ContactField) {
      calls.append(Call(name: #function, arguments: [field]))
    }
    
    func didFailToSubmitField(field: ContactField) {
      calls.append(Call(name: #function, arguments: [field]))
    }
    
    func didFinishFields() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func didSendContact() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func didFailToSendContact() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func didUpdateKeyboardHeight(height: Float) {
      calls.append(Call(name: #function, arguments: [height]))
    }
  }
  
  func test_startEditingFields_NotifiesObserver() {
    let mockService = MockContactService()
    let mockObserver = MockObserver()
    let testObject = WWContactModel(contactService: mockService)
    testObject.observer = mockObserver
    
    testObject.startEditingFields()
    
    if let field = mockObserver.arguments(for: "didMoveToNextField(field:)")?[0] as? ContactField {
      verifyName(field)
    } else {
      XCTFail("Could not find arguments")
    }
  }
  
  func test_submitFieldValue_Name_GivenEmptyString_NotifiesObserver() {
    let mockService = MockContactService()
    let mockObserver = MockObserver()
    let testObject = WWContactModel(contactService: mockService)
    testObject.observer = mockObserver
    
    testObject.submitFieldValue("")
    
    if let field = mockObserver.arguments(for: "didFailToSubmitField(field:)")?[0] as? ContactField {
      verifyName(field)
    } else {
      XCTFail("Could not find arguments")
    }
  }
  
  func test_submitFieldValue_Name_GivenNonEmptyString_NotifiesObserver() {
    let mockService = MockContactService()
    let mockObserver = MockObserver()
    let testObject = WWContactModel(contactService: mockService)
    testObject.observer = mockObserver
    
    testObject.submitFieldValue("Jeffery")
    
    if let field = mockObserver.arguments(for: "didMoveToNextField(field:)")?[0] as? ContactField {
      verifyEmail(field)
    } else {
      XCTFail("Could not find arguments")
    }
  }
  
  func test_submitFieldValue_Email_GivenEmptyString_NotifiesObserver() {
    let mockService = MockContactService()
    let mockObserver = MockObserver()
    let testObject = WWContactModel(contactService: mockService)
    testObject.observer = mockObserver
    
    testObject.submitFieldValue("Jeffery")
    testObject.submitFieldValue("")
    
    mockObserver.verify(function: "didMoveToNextField(field:)")
    if let field = mockObserver.arguments(for: "didFailToSubmitField(field:)")?[0] as? ContactField {
      verifyEmail(field)
    } else {
      XCTFail("Could not find arguments")
    }
  }
  
  func test_submitFieldValue_Email_GivenNonEmptyString_NotifiesObserver() {
    let mockService = MockContactService()
    let mockObserver = MockObserver()
    let testObject = WWContactModel(contactService: mockService)
    testObject.observer = mockObserver
    
    testObject.submitFieldValue("Jeffery")
    testObject.submitFieldValue("invalid email")
    
    mockObserver.verify(function: "didMoveToNextField(field:)")
    if let field = mockObserver.arguments(for: "didFailToSubmitField(field:)")?[0] as? ContactField {
      verifyEmail(field)
    } else {
      XCTFail("Could not find arguments")
    }
  }
  
  func test_submitFieldValue_Email_GivenNonEmptyValidString_NotifiesObserver() {
    let mockService = MockContactService()
    let mockObserver = MockObserver()
    let testObject = WWContactModel(contactService: mockService)
    testObject.observer = mockObserver
    
    testObject.submitFieldValue("Jeffery")
    testObject.submitFieldValue("pickles@sbcglobal.net")
    
    mockObserver.verify(function: "didMoveToNextField(field:)", numberOf: 2)
    if let field = mockObserver.arguments(for: "didMoveToNextField(field:)", at: 1)?[0] as? ContactField {
      verifyMessage(field)
    } else {
      XCTFail("Could not find arguments")
    }
  }
  
  func test_submitFieldValue_Message_GivenEmptyString_NotifiesObserver() {
    let mockService = MockContactService()
    let mockObserver = MockObserver()
    let testObject = WWContactModel(contactService: mockService)
    testObject.observer = mockObserver
    
    testObject.submitFieldValue("Jeffery")
    testObject.submitFieldValue("pickles@sbcglobal.net")
    testObject.submitFieldValue("")
    
    mockObserver.verify(function: "didMoveToNextField(field:)", numberOf: 2)
    if let field = mockObserver.arguments(for: "didFailToSubmitField(field:)")?[0] as? ContactField {
      verifyMessage(field)
    } else {
      XCTFail("Could not find arguments")
    }
  }
  
  func test_submitFieldValue_Message_GivenNonEmptyString_NotifiesObserver() {
    let mockService = MockContactService()
    let mockObserver = MockObserver()
    let testObject = WWContactModel(contactService: mockService)
    testObject.observer = mockObserver
    
    testObject.submitFieldValue("Jeffery")
    testObject.submitFieldValue("pickles@sbcglobal.net")
    testObject.submitFieldValue("hi")
    
    mockObserver.verify(function: "didMoveToNextField(field:)", numberOf: 2)
    mockObserver.verify(function: "didFinishFields()")
  }
  
  func test_send_GivenSuccess_NotifiesObserver() {
    let mockService = MockContactService()
    let testObject = WWContactModel(contactService: mockService)
    let mockObserver = MockObserver()
    testObject.observer = mockObserver
    mockService.promise = Promise<Data>(Data())
    
    testObject.send()
    
    _ = waitForPromises(timeout: 1)
    mockObserver.verify(function: "didSendContact()")
  }
  
  func test_send_GivenFailure_NotifiesObserver() {
    let mockService = MockContactService()
    let testObject = WWContactModel(contactService: mockService)
    let mockObserver = MockObserver()
    testObject.observer = mockObserver
    mockService.promise = Promise<Data>(ServiceError.invalidUrl)
    
    testObject.send()
    
    _ = waitForPromises(timeout: 1)
    mockObserver.verify(function: "didFailToSendContact()")
  }
  
  func testKeyboardWillShow() {
    let mockService = MockContactService()
    let testObject = WWContactModel(contactService: mockService)
    let mockObserver = MockObserver()
    testObject.observer = mockObserver
    
    let rect = CGRect(x: 0, y: 0, width: 0, height: 54321)
    let value = NSValue(cgRect: rect)
    
    NotificationCenter.default.post(name: UIResponder.keyboardWillShowNotification, object: nil, userInfo: [UIResponder.keyboardFrameEndUserInfoKey: value])
    
    mockObserver.verify(function: "didUpdateKeyboardHeight(height:)", with: Float(rect.height))
  }
  
  func testKeyboardWillHide() {
    let mockService = MockContactService()
    let testObject = WWContactModel(contactService: mockService)
    let mockObserver = MockObserver()
    testObject.observer = mockObserver
    
    NotificationCenter.default.post(name: UIResponder.keyboardWillHideNotification, object: nil, userInfo: [:])
    
    mockObserver.verify(function: "didUpdateKeyboardHeight(height:)", with: Float(0))
  }
  
  private func verifyName(_ field: ContactField) {
    XCTAssertEqual(field.heading, "I'm glad you would like to talk!\nWhat's your name?")
    XCTAssertEqual(field.error, "Please enter your name")
    XCTAssertEqual(field.keyboardType, .default)
    XCTAssertEqual(field.capitalizationType, .words)
    XCTAssertEqual(field.returnKeyType, .next)
  }
  
  private func verifyEmail(_ field: ContactField) {
    XCTAssertEqual(field.heading, "How can I reach you?")
    XCTAssertEqual(field.error, "Please enter a valid email")
    XCTAssertEqual(field.keyboardType, .emailAddress)
    XCTAssertEqual(field.capitalizationType, .none)
    XCTAssertEqual(field.returnKeyType, .next)
  }
  
  private func verifyMessage(_ field: ContactField) {
    XCTAssertEqual(field.heading, "What would you like to talk about?")
    XCTAssertEqual(field.error, "Please enter a message")
    XCTAssertEqual(field.keyboardType, .default)
    XCTAssertEqual(field.capitalizationType, .sentences)
    XCTAssertEqual(field.returnKeyType, .send)
  }
}
