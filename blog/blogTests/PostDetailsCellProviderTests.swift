//
//  PostDetailsCellProviderTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/15/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import Promises
@testable import blog

class PostDetailsCellProviderTests: XCTestCase {
  func testNumberOfSectionsAndRows() {
    let table = UITableView()
    let viewModel = mockPostViewModel()
    let mockPhotoService = MockPhotoService()
    let testObject = WWPostDetailsCellProvider(viewModel: viewModel, photoService: mockPhotoService)
    
    XCTAssertEqual(testObject.numberOfSections(in: table), 3)
    XCTAssertEqual(testObject.tableView(table, numberOfRowsInSection: 0), 1)
    XCTAssertEqual(testObject.tableView(table, numberOfRowsInSection: 1), 5)
  }
  
  func testCellForHeading_SetsText() {
    let table = UITableView()
    let viewModel = mockPostViewModel()
    let mockPhotoService = MockPhotoService()
    let testObject = WWPostDetailsCellProvider(viewModel: viewModel, photoService: mockPhotoService)
    
    testObject.registerCells(for: table)
    
    let cell = testObject.tableView(table, cellForRowAt: IndexPath(row: 0, section: 0)) as! PostCell
    
    XCTAssertEqual(cell.heading.headingLabel.text, "hello")
    XCTAssertEqual(cell.heading.subheadingLabel.text, "world")
  }
  
  func testCellForHeading_SetsCover_Success() {
    let image = UIImage()
    let table = UITableView()
    let viewModel = mockPostViewModel()
    let mockPhotoService = MockPhotoService()
    let testObject = WWPostDetailsCellProvider(viewModel: viewModel, photoService: mockPhotoService)
    
    testObject.registerCells(for: table)
    mockPhotoService.promise = Promise<UIImage>(image)
    
    let cell = testObject.tableView(table, cellForRowAt: IndexPath(row: 0, section: 0)) as! PostCell
    
    XCTAssert(waitForPromises(timeout: 1))
    XCTAssertEqual(cell.heading.backgroundImage.image, image)
    mockPhotoService.verify(function: "get(id:)", with: "!!")
  }
  
  func testCellForHeading_SetsCover_Failure() {
    let table = UITableView()
    let viewModel = mockPostViewModel()
    let mockPhotoService = MockPhotoService()
    let testObject = WWPostDetailsCellProvider(viewModel: viewModel, photoService: mockPhotoService)
    
    testObject.registerCells(for: table)
    mockPhotoService.promise = Promise<UIImage>(ServiceError.invalidData)
    
    let cell = testObject.tableView(table, cellForRowAt: IndexPath(row: 0, section: 0)) as! PostCell
    
    XCTAssert(waitForPromises(timeout: 1))
    XCTAssertNil(cell.heading.backgroundImage.image)
  }
  
  func testCellForTextComponent_SetsTextAndFont() {
    let table = UITableView()
    let viewModel = mockPostViewModel()
    let mockPhotoService = MockPhotoService()
    let testObject = WWPostDetailsCellProvider(viewModel: viewModel, photoService: mockPhotoService)
    
    testObject.registerCells(for: table)
    
    let cell = testObject.tableView(table, cellForRowAt: IndexPath(row: 0, section: 1)) as! TextComponentCell
    
    XCTAssertEqual(cell.label.text, "I am some text")
    XCTAssertEqual(cell.label.font, UIFont.openSansLight(of: 18))
  }
  
  func testCellForHeadingComponent_SetsTextAndFont() {
    let table = UITableView()
    let viewModel = mockPostViewModel()
    let mockPhotoService = MockPhotoService()
    let testObject = WWPostDetailsCellProvider(viewModel: viewModel, photoService: mockPhotoService)
    
    testObject.registerCells(for: table)
    
    let cell = testObject.tableView(table, cellForRowAt: IndexPath(row: 1, section: 1)) as! TextComponentCell
    
    XCTAssertEqual(cell.label.text, "I am a heading")
    XCTAssertEqual(cell.label.font, UIFont.openSansBold(of: 24))
  }
  
  func testCellForVideoComponent_SetsVideo_Success() {
    let table = UITableView()
    let viewModel = mockPostViewModel()
    let mockPhotoService = MockPhotoService()
    let testObject = WWPostDetailsCellProvider(viewModel: viewModel, photoService: mockPhotoService)
    
    testObject.registerCells(for: table)
    
    let cell = testObject.tableView(table, cellForRowAt: IndexPath(row: 3, section: 1)) as! VideoComponentCell
    
    XCTAssertEqual(cell.player.id, "I am a video")
  }
  
  private func mockPostViewModel() -> MockPostViewModel {
    return MockPostViewModel(headingText: "hello", subheadingText: "world", cover: "!!", components: [
        MockPostComponentViewModel(type: "text", content: "I am some text"),
        MockPostComponentViewModel(type: "heading", content: "I am a heading"),
        MockPostComponentViewModel(type: "image", content: "I am an image"),
        MockPostComponentViewModel(type: "youtube", content: "I am a video"),
        MockPostComponentViewModel(type: "location", content: "I am a map"),
      ])
  }
}
