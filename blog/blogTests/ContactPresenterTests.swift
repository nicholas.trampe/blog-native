//
//  ContactPresenterTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class ContactPresenterTests: XCTestCase {
  class MockView: ContactView, Mock {
    var calls: [Call] = []
    var observer: ContactViewObserver? = nil
    
    func setSpinning(_ isSpinning: Bool) {
      calls.append(Call(name: #function, arguments: [isSpinning]))
    }
    
    func setHeading(_ text: String) {
      calls.append(Call(name: #function, arguments: [text]))
    }
    
    func setError(_ text: String?) {
      let args: [Any] = text != nil ? [text!] : []
      calls.append(Call(name: #function, arguments: args))
    }
    
    func setKeyboardType(_ type: UIKeyboardType) {
      calls.append(Call(name: #function, arguments: [type]))
    }
    
    func setCapitalizationType(_ type: UITextAutocapitalizationType) {
      calls.append(Call(name: #function, arguments: [type]))
    }
    
    func setReturnType(_ type: UIReturnKeyType) {
      calls.append(Call(name: #function, arguments: [type]))
    }
    
    func setBottomOffset(_ offset: CGFloat) {
      calls.append(Call(name: #function, arguments: [offset]))
    }
    
    func focus() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func unfocus() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  class MockModel: ContactModel, Mock {
    var observer: ContactModelObserver? = nil
    var calls: [Call] = []
    
    func send() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func startEditingFields() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func submitFieldValue(_ text: String) {
      calls.append(Call(name: #function, arguments: [text]))
    }
  }
  
  class MockObserver: ContactPresenterObserver, Mock {
    var calls: [Call] = []
    
    func didClose() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func didSend() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  class MockField: ContactField {
    var value: String = ""
    var heading: String = ""
    var error: String = ""
    var keyboardType: UIKeyboardType = .default
    var capitalizationType: UITextAutocapitalizationType = .none
    var returnKeyType: UIReturnKeyType = .default
    var isValid: Bool = false
  }
  
  func test_start() {
    let mockView = MockView()
    let mockModel = MockModel()
    let testObject = WWContactPresenter(view: mockView, model: mockModel)
    
    testObject.start()
    
    mockModel.verify(function: "startEditingFields()")
  }
  
  func testLeaveButton() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockObserver = MockObserver()
    let testObject = WWContactPresenter(view: mockView, model: mockModel)
    testObject.observer = mockObserver
    
    mockView.observer?.didPressLeaveButton()
   
    mockView.verify(function: "unfocus()")
    mockObserver.verify(function: "didClose()")
  }
  
  func testEnterButton_GivenModelIsValid_ThenSendsAndNotifiesObserver() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockObserver = MockObserver()
    let testObject = WWContactPresenter(view: mockView, model: mockModel)
    testObject.observer = mockObserver
    
    mockView.observer?.didPressEnterButton("next me")
    
    mockModel.verify(function: "submitFieldValue", with: "next me")
  }
  
  func testDidUpdateField() {
    let mockView = MockView()
    let mockModel = MockModel()
    _ = WWContactPresenter(view: mockView, model: mockModel)
    
    mockView.observer?.didUpdateField()
    
    mockView.verify(function: "focus()")
  }
  
  func test_DidSendContact_NotifiesObserver() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockObserver = MockObserver()
    let testObject = WWContactPresenter(view: mockView, model: mockModel)
    testObject.observer = mockObserver
    
    mockModel.observer?.didSendContact()
    
    mockObserver.verify(function: "didSend()")
  }
  
  func test_DidFailToSendContact_UpdatesView() {
    let mockView = MockView()
    let mockModel = MockModel()
    _ = WWContactPresenter(view: mockView, model: mockModel)
    
    mockModel.observer?.didFailToSendContact()
    
    mockView.verify(function: "setSpinning", with: false)
  }
  
  func test_didUpdateKeyboardHeight_UpdatesView() {
    let height: CGFloat = 123
    let mockView = MockView()
    let mockModel = MockModel()
    _ = WWContactPresenter(view: mockView, model: mockModel)
    
    mockModel.observer?.didUpdateKeyboardHeight(height: Float(height))
    
    mockView.verify(function: "setBottomOffset", with: height)
  }
  
  func test_didMoveToNextField_UpdatesView() {
    let mockView = MockView()
    let mockModel = MockModel()
    _ = WWContactPresenter(view: mockView, model: mockModel)
    
    let field = MockField()
    field.heading = "heading"
    field.keyboardType = .numberPad
    field.capitalizationType = .allCharacters
    field.returnKeyType = .emergencyCall
    
    mockModel.observer?.didMoveToNextField(field: field)
    
    mockView.verify(function: "unfocus()")
    mockView.verify(function: "setHeading", with: field.heading)
    mockView.verify(function: "setError")
    mockView.verify(function: "setKeyboardType", with: field.keyboardType)
    mockView.verify(function: "setCapitalizationType", with: field.capitalizationType)
    mockView.verify(function: "setReturnType", with: field.returnKeyType)
  }
  
  func test_didFinishFields_UpdatesView() {
    let mockView = MockView()
    let mockModel = MockModel()
    _ = WWContactPresenter(view: mockView, model: mockModel)
    
    mockModel.observer?.didFinishFields()
    
    mockView.verify(function: "setSpinning", with: true)
    mockModel.verify(function: "send()")
    mockView.verify(function: "unfocus()")
  }
}

