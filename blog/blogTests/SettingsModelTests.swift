//
//  SettingsModelTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/6/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class SettingsModelTests: XCTestCase {
  class MockDirectory: CacheDirectory, Mock {
    var calls: [Call] = []
    var url: URL? = nil
    var items: [String] = []
    var itemURL: URL? = nil
    
    func url(for item: String) -> URL? {
      calls.append(Call(name: #function, arguments: [item]))
      return itemURL
    }
    
    func clear() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  func test_ClearCache() {
    let mockDirectory = MockDirectory()
    let testObject = WWSettingsModel(cachesDirectory: mockDirectory)
    
    testObject.clearCache()
    
    mockDirectory.verify(function: "clear()")
  }
}
