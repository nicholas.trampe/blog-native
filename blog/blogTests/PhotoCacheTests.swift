//
//  PhotoCacheTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import Promises
@testable import blog

class PhotoCacheTests: XCTestCase {
  private let directory = WWCacheDirectory()
  
  func testInsertion() {
    let id = "123"
    let expectedImage = UIImage(named: "test")!
    let testObject = WWPhotoCache(directory: directory)
    let exp = expectation(description: "photo saved")
    
    testObject.savePhoto(for: id, data: expectedImage.pngData()!).then({ actualImage in
      let data1 = expectedImage.pngData()
      let data2 = actualImage.pngData()
      
      XCTAssertEqual(data1, data2)
      exp.fulfill()
    }).catch({ error in 
      XCTFail("Should not fail")
    })
    
    waitForExpectations(timeout: 1.0, handler: nil)
  }
}
