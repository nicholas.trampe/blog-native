//
//  MainModelTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import Promises
@testable import blog

class MainModelTests: XCTestCase {
  
  class MockPostService: PostService, Mock {
    var calls: [Call] = []
    var promise = Promise<[Post]>(ServiceError.invalidUrl)
    
    func get() -> Promise<[Post]> {
      calls.append(Call(name: #function, arguments: []))
      return promise
    }
  }
  
  class MockObserver: MainModelObserver, Mock {
    var calls: [Call] = []
    
    func didUpdatePosts(posts: [Post]) {
      calls.append(Call(name: #function, arguments: [posts]))
    }
    
    func didFailToUpdatePosts(error: Error) {
      calls.append(Call(name: #function, arguments: [error]))
    }
  }
  
  func testUpdatePostsServiceFailure() {
    let postService = MockPostService()
    let mockObserver = MockObserver()
    let testObject = WWMainModel(postService: postService)
    testObject.observer = mockObserver
    
    postService.promise = Promise<[Post]>(ServiceError.invalidUrl)
    
    testObject.updatePosts()
    
    XCTAssert(waitForPromises(timeout: 1))
    mockObserver.verify(function: "didFailToUpdatePosts(error:)")
    
    if let error = mockObserver.calls.first?.arguments.first as? ServiceError {
      switch error {
      case .invalidUrl: break
      default:
        XCTFail("Error types are not equal")
      }
    } else {
      XCTFail("Argument is not of type ServiceError")
    }
  }
  
  func testUpdatePostsDataFailure() {
    let postService = MockPostService()
    let mockObserver = MockObserver()
    let testObject = WWMainModel(postService: postService)
    
    testObject.observer = mockObserver
    postService.promise = Promise<[Post]>(ServiceError.invalidData)
    
    testObject.updatePosts()
    
    XCTAssert(waitForPromises(timeout: 1))
    mockObserver.verify(function: "didFailToUpdatePosts(error:)")
    
    if let error = mockObserver.calls.first?.arguments.first as? ServiceError {
      switch error {
      case .invalidData: break
      default:
        XCTFail("Error types are not equal")
      }
    } else {
      XCTFail("Argument is not of type ServiceError")
    }
  }
  
  func testUpdatePostsSuccess() {
    let posts = [Post(_id: "123", heading: "hello", subheading: "world", cover: "!!", date: Date(), body: [Post.BodyComponent(_id: "456", type: "type", content: "content")])]
    let encoder = JSONEncoder()
    encoder.dateEncodingStrategy = .formatted(.jsonDateFormatter)
    let data = try! encoder.encode(posts)
    let postService = MockPostService()
    let mockObserver = MockObserver()
    let testObject = WWMainModel(postService: postService)
    
    testObject.observer = mockObserver
    postService.promise = Promise<[Post]>(posts)
    
    testObject.updatePosts()
    
    XCTAssert(waitForPromises(timeout: 1))
    XCTAssertEqual(posts, testObject.posts)
    mockObserver.verify(function: "didUpdatePosts(posts:)")
    
    if let actualPosts = mockObserver.calls.first?.arguments.first as? [Post] {
      XCTAssertEqual(posts, actualPosts)
    } else {
      XCTFail("Argument is not of type [Post]")
    }
  }
  
  func testUpdatePostsSyncDate_GivenDateLessThanSyncInterval_ThenDoesNotCallService() {
    let postService = MockPostService()
    let mockObserver = MockObserver()
    let testObject = WWMainModel(postService: postService)
    
    testObject.observer = mockObserver
    testObject.syncExpirationInterval = 60
    testObject.lastSyncDate = Date()
    postService.promise = Promise<[Post]>(ServiceError.invalidData)
    
    testObject.updatePosts()
    
    XCTAssert(waitForPromises(timeout: 1))
    XCTAssertEqual(postService.calls.count, 0)
  }
  
  func testUpdatePostsSyncDate_GivenDateMoreThanSyncInterval_ThenCallsService() {
    let postService = MockPostService()
    let mockObserver = MockObserver()
    let testObject = WWMainModel(postService: postService)
    
    testObject.observer = mockObserver
    testObject.syncExpirationInterval = 60
    testObject.lastSyncDate = Date(timeIntervalSinceNow: -100)
    postService.promise = Promise<[Post]>(ServiceError.invalidData)
    
    testObject.updatePosts()
    
    XCTAssert(waitForPromises(timeout: 1))
    postService.verify(function: "get()")
  }
}
