//
//  MenuPresenterTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class MenuPresenterTests: XCTestCase {
  class MockView: MenuView, Mock {
    var calls: [Call] = []
    var observer: MenuViewObserver? = nil
    
    func setCellProvider(_ cellProvider: CellProvider) {
      calls.append(Call(name: #function, arguments: [cellProvider]))
    }
  }
  
  class MockModel: MenuModel, Mock {
    var calls: [Call] = []
    var observer: MenuModelObserver? = nil
    var items: [MenuItem]  = []
    
    func selectItem(at index: Int) {
      calls.append(Call(name: #function, arguments: [index]))
    }
  }
  
  class MockCellProvider: NSObject, MenuCellProvider, Mock {
    var calls: [Call] = []
    var items: [MenuItem] = []
    
    func registerCells(for tableView: UITableView) {
      calls.append(Call(name: #function, arguments: [tableView]))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      calls.append(Call(name: #function, arguments: [section]))
      return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      calls.append(Call(name: #function, arguments: [indexPath]))
      return UITableViewCell()
    }
  }
  
  class MockObserver: MenuPresenterObserver, Mock {
    var calls: [Call] = []
    
    func didSelectItem(at index: Int) {
      calls.append(Call(name: #function, arguments: [index]))
    }
  }
  
  func testStart() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockCellProvider = MockCellProvider()
    let testObject = WWMenuPresenter(view: mockView, model: mockModel, cellProvider: mockCellProvider)
    mockModel.items = [MenuItem(title: "test 1", icon: .info),
                       MenuItem(title: "test 2", icon: .mail),
                       MenuItem(title: "pickles", icon: .instagram)]
    
    testObject.start()
    
    XCTAssertEqual(mockCellProvider.items, mockModel.items)
    mockView.verify(function: "setCellProvider")
  }
  
  func test_GivenViewSelectedItem_UpdatesModel() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockCellProvider = MockCellProvider()
    _ = WWMenuPresenter(view: mockView, model: mockModel, cellProvider: mockCellProvider)
    let path = IndexPath(row: 123, section: 456)
    
    mockView.observer?.didSelectItem(at: path)
    
    mockModel.verify(function: "selectItem(at:)", with: path.row)
  }
  
  func test_GivenModelUpdated_NotifiesObserver() {
    let index = 123
    let mockView = MockView()
    let mockModel = MockModel()
    let mockCellProvider = MockCellProvider()
    let mockObserver = MockObserver()
    let testObject = WWMenuPresenter(view: mockView, model: mockModel, cellProvider: mockCellProvider)
    testObject.observer = mockObserver
    
    mockModel.observer?.didSelectItem(at: index)
    
    mockObserver.verify(function: "didSelectItem(at:)", with: index)
  }
}
