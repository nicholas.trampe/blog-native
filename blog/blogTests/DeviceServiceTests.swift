//
//  DeviceServiceTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 7/7/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class DeviceServiceTests: XCTestCase {
  func testRegister() {
    let mockService = MockService()
    let testObject = WWDeviceService(service: mockService)
    let exp = expectation(description: "Service called")
    
    let token = "Jeffery"
    let body = ["notificationToken": token]
    
    testObject.register(token: token).then { data in
      exp.fulfill()
    }
    
    waitForExpectations(timeout: 0.1, handler: nil)
    
    mockService.verify(function: "post(route:body:)", with: "registerDevice", at: 0)
    
    if let actualBody = mockService.call(for: "post(route:body:)")?.arguments[1] as? [String:String] {
      XCTAssertEqual(actualBody, body)
    } else {
      XCTFail("Body is not a string to string dictionary")
    }
  }
}
