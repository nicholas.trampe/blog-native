//
//  PostCellViewModelTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class PostCellViewModelTests: XCTestCase {
  func testHeadingText() {
    let heading = "heading"
    let post = Post(_id: "", heading: heading, subheading: "", cover: "", date: Date(), body: [])
    let testObject = WWPostCellViewModel(post: post)
    
    XCTAssertEqual(testObject.headingText, heading)
  }
  
  func testSubheadingText() {
    let subheading = "heading"
    let post = Post(_id: "", heading: "", subheading: subheading, cover: "", date: Date(), body: [])
    let testObject = WWPostCellViewModel(post: post)
    
    XCTAssertEqual(testObject.subheadingText, subheading)
  }
  
  func testCover() {
    let cover = "cover"
    let post = Post(_id: "", heading: "", subheading: "", cover: cover, date: Date(), body: [])
    let testObject = WWPostCellViewModel(post: post)
    
    XCTAssertEqual(testObject.cover, cover)
  }
  
  func testDateText_GivenDateIsSameYear_ThenExcludesDate() {
    let year = Calendar.current.component(.year, from: Date())
    let date = DateFormatter.monthDayYearFormatter.date(from: "Aug 18, \(year)")!
    let post = Post(_id: "", heading: "", subheading: "", cover: "", date: date, body: [])
    let testObject = WWPostCellViewModel(post: post)
    
    XCTAssertEqual(testObject.dateText, "Aug 18")
  }
  
  func testDateText_GivenDateIsDifferentYear_ThenIncludesDate() {
    let date = DateFormatter.monthDayYearFormatter.date(from: "Aug 18, 1492")!
    let post = Post(_id: "", heading: "", subheading: "", cover: "", date: date, body: [])
    let testObject = WWPostCellViewModel(post: post)
    
    XCTAssertEqual(testObject.dateText, "Aug 18, 1492")
  }
}

