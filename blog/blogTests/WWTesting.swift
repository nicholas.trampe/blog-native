//
//  WWTesting.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import Promises
@testable import blog

struct Call {
  let name: String
  let arguments: [Any]
}

protocol Mock {
  var calls: [Call] { get set }
}

extension Mock {  
  func call(for name: String, at index: Int = 0) -> Call? {
    let found = calls.filter { (call) -> Bool in
      call.name == name
    }
    
    if found.count >= index && index >= 0 {
      return found[index]
    }
    
    return nil
  }
  
  func arguments(for name: String, at index: Int = 0) -> [Any]? {
    return call(for: name, at: index)?.arguments
  }
  
  func verify(function called: String, numberOf times: Int = 1) {
    XCTAssertEqual(functionCalled(called), times)
  }
  
  func verifyNot(function called: String) {
    XCTAssertEqual(functionCalled(called), 0)
  }
  
  func verify<T: Equatable>(function called: String, with arg: T, at index: Int = 0) {
    
    let matchingCalls = calls.filter { (call) -> Bool in
      call.name == called
    }
    
    if matchingCalls.count == 0 {
      XCTFail("Call to \(called) not found")
      return
    }
    
    let matchingArguments = matchingCalls.filter { call -> Bool in
      if let actualArg = call.arguments[index] as? T {
        return actualArg == arg
      }
      
      return false
    }
    
    XCTAssertGreaterThanOrEqual(matchingArguments.count, 1, "Of the calls named \(called) found, none had matching arguments")
  }
  
  func expectation(for function: String, with number: Int) -> XCTestExpectation {
    let predicate = NSPredicate { (evaluatedObject, _) -> Bool in
      guard let evaluatedObject = evaluatedObject as? Mock else { return false }
      
      return evaluatedObject.functionCalled(function) >= number
    }
    return XCTNSPredicateExpectation(predicate: predicate, object: self)
  }
  
  private func functionCalled(_ name: String) -> Int {
    
    let found = calls.filter { (call) -> Bool in
      call.name == name
    }
    
    return found.count
  }
}

class MockService: Service, Mock {
  var calls: [Call] = []
  var baseURL: String = "base"
  var promise = Promise<Data>(Data())
  var authenticationPromise = Promise<Void>(())
  
  func get(route: String) -> Promise<Data> {
    calls.append(Call(name: #function, arguments: [route]))
    return promise
  }
  
  func post(route: String, body: Any) -> Promise<Data> {
    calls.append(Call(name: #function, arguments: [route, body]))
    return promise
  }
  
  func authenticate(username: String, password: String) -> Promise<Void> {
    calls.append(Call(name: #function, arguments: [username, password]))
    return authenticationPromise
  }
}

class MockPhotoService: PhotoService, Mock {
  var calls: [Call] = []
  var promise = Promise<UIImage>(UIImage())
  
  func get(id: String) -> Promise<UIImage> {
    calls.append(Call(name: #function, arguments: [id]))
    return promise
  }
}

class MockDataCache: DataCache, Mock {
  var calls: [Call] = []
  var name: String = "cache"
  var savePromise = Promise<Data>(Data())
  var loadPromise = Promise<Data>(Data())
  var clearPromise = Promise<Void>(())
  
  func saveData(data: Data) -> Promise<Data> {
    calls.append(Call(name: #function, arguments: [data]))
    return savePromise
  }
  
  func loadData() -> Promise<Data> {
    calls.append(Call(name: #function, arguments: []))
    return loadPromise
  }
  
  func clearData() -> Promise<Void> {
    calls.append(Call(name: #function, arguments: []))
    return clearPromise
  }
}

class MockReachability: Reachability {
  var isReachable: Bool = true
}

struct MockPostComponentViewModel: PostComponentViewModel {
  let type: String
  let content: String
}

struct MockPostViewModel: PostViewModel {
  let headingText: String
  let subheadingText: String
  let cover: String
  let components: [PostComponentViewModel]
}
