//
//  PlacesModelTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 9/10/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import Promises
@testable import blog

class PlacesModelTests: XCTestCase {
  
  class MockPlaceService: PlaceService, Mock {
    var calls: [Call] = []
    var promise = Promise<[Place]>([])
    
    func get() -> Promise<[Place]> {
      calls.append(Call(name: #function, arguments: []))
      return promise
    }
  }
  
  class MockObserver: PlacesModelObserver, Mock {
    var calls: [Call] = []
    
    func didUpdatePlaces(places: [Place]) {
      calls.append(Call(name: #function, arguments: [places]))
    }
    
    func didFailToUpdateUpdatePlaces(error: Error) {
      calls.append(Call(name: #function, arguments: [error]))
    }
    
    func didLoad(photo: UIImage, for place: Place) {
      calls.append(Call(name: #function, arguments: [photo, place]))
    }
    
    func didFailToLoadPhotos(for place: Place, error: Error) {
      calls.append(Call(name: #function, arguments: [place, error]))
    }
  }
  
  func testUpdatePlacesServiceFailure() {
    let photoService = MockPhotoService()
    let placeService = MockPlaceService()
    let mockObserver = MockObserver()
    let testObject = WWPlacesModel(placeService: placeService, photoService: photoService)
    
    placeService.promise = Promise<[Place]>(ServiceError.invalidUrl)
    testObject.observer = mockObserver
    
    testObject.updatePlaces()
    
    XCTAssert(waitForPromises(timeout: 1))
    mockObserver.verify(function: "didFailToUpdateUpdatePlaces(error:)")
    
    if let error = mockObserver.calls.first?.arguments.first as? ServiceError {
      switch error {
      case .invalidUrl: break
      default:
        XCTFail("Error types are not equal")
      }
    } else {
      XCTFail("Argument is not of type ServiceError")
    }
  }
  
  func testUpdatePlacesDataFailure() {
    let photoService = MockPhotoService()
    let placeService = MockPlaceService()
    let mockObserver = MockObserver()
    let testObject = WWPlacesModel(placeService: placeService, photoService: photoService)
    
    placeService.promise = Promise<[Place]>(ServiceError.invalidData)
    testObject.observer = mockObserver
    
    testObject.updatePlaces()
    
    XCTAssert(waitForPromises(timeout: 1))
    mockObserver.verify(function: "didFailToUpdateUpdatePlaces(error:)")
    
    if let error = mockObserver.calls.first?.arguments.first as? ServiceError {
      switch error {
      case .invalidData: break
      default:
        XCTFail("Error types are not equal")
      }
    } else {
      XCTFail("Argument is not of type ServiceError")
    }
  }
  
  func testUpdatePostsSuccess() {
    let places = [Place(_id: "123", name: "names", description: "stuffs", latitude: 123, longitude: 456, date: "dater", photos: [])]
    let placeService = MockPlaceService()
    let photoService = MockPhotoService()
    let mockObserver = MockObserver()
    let testObject = WWPlacesModel(placeService: placeService, photoService: photoService)
    
    placeService.promise = Promise<[Place]>(places)
    testObject.observer = mockObserver
    
    testObject.updatePlaces()
    
    XCTAssert(waitForPromises(timeout: 1))
    XCTAssertEqual(places, testObject.places)
    mockObserver.verify(function: "didUpdatePlaces(places:)")
    
    if let actualPlaces = mockObserver.calls.first?.arguments.first as? [Place] {
      XCTAssertEqual(places, actualPlaces)
    } else {
      XCTFail("Argument is not of type [Post]")
    }
  }
  
  func test_loadPhotos_GivenPlaceNotFound_ThenDoesNothing() {
    let places = [Place(_id: "123", name: "names", description: "stuffs", latitude: 123, longitude: 456, date: "dater", photos: [])]
    let placeService = MockPlaceService()
    let photoService = MockPhotoService()
    let mockObserver = MockObserver()
    let testObject = WWPlacesModel(placeService: placeService, photoService: photoService)
    
    placeService.promise = Promise<[Place]>(places)
    testObject.observer = mockObserver
    testObject.places = places
    
    testObject.loadPhotos(for: "456")
    
    XCTAssert(waitForPromises(timeout: 1))
    XCTAssertEqual(mockObserver.calls.count, 0)
    XCTAssertEqual(photoService.calls.count, 0)
  }
  
  func test_loadPhotos_GivenPlaceFound_ThenIteratesThroughPhotos_Success() {
    let image = UIImage()
    let photos = [Photo(_id: "123", filename: "f1", uploaded: Date()),Photo(_id: "456", filename: "f2", uploaded: Date()),Photo(_id: "789", filename: "f3", uploaded: Date())]
    let places = [Place(_id: "123", name: "names", description: "stuffs", latitude: 123, longitude: 456, date: "dater", photos: photos)]
    let placeService = MockPlaceService()
    let photoService = MockPhotoService()
    let mockObserver = MockObserver()
    let testObject = WWPlacesModel(placeService: placeService, photoService: photoService)
    
    placeService.promise = Promise<[Place]>(places)
    testObject.observer = mockObserver
    testObject.places = places
    photoService.promise = Promise<UIImage>(image)
    
    testObject.loadPhotos(for: "123")
    
    XCTAssert(waitForPromises(timeout: 1))
    photoService.verify(function: "get(id:)", numberOf: 3)
    
    for (index, call) in photoService.calls.enumerated() {
      XCTAssertEqual(photos[index]._id, call.arguments[0] as? String)
    }
    
    mockObserver.verify(function: "didLoad(photo:for:)", numberOf: 3)
    
    for call in mockObserver.calls {
      XCTAssertEqual(image, call.arguments[0] as? UIImage)
      XCTAssertEqual(places[0], call.arguments[1] as? Place)
    }
  }
  
  func test_loadPhotos_GivenPlaceFound_ThenIteratesThroughPhotos_Failure() {
    let error = ServiceError.invalidUrl
    let photos = [Photo(_id: "123", filename: "f1", uploaded: Date()),Photo(_id: "456", filename: "f2", uploaded: Date()),Photo(_id: "789", filename: "f3", uploaded: Date())]
    let places = [Place(_id: "123", name: "names", description: "stuffs", latitude: 123, longitude: 456, date: "dater", photos: photos)]
    let placeService = MockPlaceService()
    let photoService = MockPhotoService()
    let mockObserver = MockObserver()
    let testObject = WWPlacesModel(placeService: placeService, photoService: photoService)
    
    placeService.promise = Promise<[Place]>(places)
    testObject.observer = mockObserver
    testObject.places = places
    photoService.promise = Promise<UIImage>(error)
    
    testObject.loadPhotos(for: "123")
    
    XCTAssert(waitForPromises(timeout: 1))
    photoService.verify(function: "get(id:)", numberOf: 3)
    
    for (index, call) in photoService.calls.enumerated() {
      XCTAssertEqual(photos[index]._id, call.arguments[0] as? String)
    }
    
    mockObserver.verify(function: "didFailToLoadPhotos(for:error:)", numberOf: 3)
    
    for call in mockObserver.calls {
      XCTAssertEqual(places[0], call.arguments[0] as? Place)
      XCTAssertEqual(error, call.arguments[1] as? ServiceError)
    }
  }
}
