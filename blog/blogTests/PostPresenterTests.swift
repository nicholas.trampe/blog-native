//
//  PostPresenterTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/14/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class PostPresenterTests: XCTestCase {
  class MockObserver: PostPresenterObserver, Mock {
    var calls: [Call] = []
    
    func didPressCloseButton() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  class MockPostView: PostView, Mock {
    var calls: [Call] = []
    var observer: PostViewObserver? = nil
    var headerHeight: CGFloat = 0
    var closeButtonFrame: CGRect = .zero
    
    
    func setCellProvider(_ cellProvider: CellProvider) {
      calls.append(Call(name: #function, arguments: [cellProvider]))
    }
    
    func updateData() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func setScrollPosition(_ position: CGFloat, animated: Bool) {
      calls.append(Call(name: #function, arguments: [position, animated]))
    }
    
    func setCloseButtonHidden(_ hidden: Bool, animated: Bool) {
      calls.append(Call(name: #function, arguments: [hidden, animated]))
    }
    
    func setCloseButtonBackgroundColor(_ color: UIColor, for state: UIControl.State) {
      calls.append(Call(name: #function, arguments: [color, state]))
    }
    
    func setCloseButtonForegroundColor(_ color: UIColor, for state: UIControl.State) {
      calls.append(Call(name: #function, arguments: [color, state]))
    }
    
    func setTopOffset(_ offset: CGFloat) {
      calls.append(Call(name: #function, arguments: [offset]))
    }
  }
  
  func testInit_SetsCellProvider() {
    let mockView = MockPostView()
    let mockPhotoService = MockPhotoService()
    let mockPostViewModel = MockPostViewModel(headingText: "hello", subheadingText: "world", cover: "!!", components: [])
    _ = WWPostPresenter(view: mockView, cellProvider: WWPostDetailsCellProvider(viewModel: mockPostViewModel, photoService: mockPhotoService))
    
    mockView.verify(function: "setCellProvider")
  }
  
  func testInit_SetsTopOffset() {
    let mockView = MockPostView()
    let mockPhotoService = MockPhotoService()
    let mockPostViewModel = MockPostViewModel(headingText: "hello", subheadingText: "world", cover: "!!", components: [])
    _ = WWPostPresenter(view: mockView, cellProvider: WWPostDetailsCellProvider(viewModel: mockPostViewModel, photoService: mockPhotoService))
    
    mockView.verify(function: "setTopOffset", with: UIApplication.shared.statusBarFrame.size.height)
  }
  
  func testPeek_SetsTopOffsetAndHidesCloseButton() {
    let mockView = MockPostView()
    let mockPhotoService = MockPhotoService()
    let mockPostViewModel = MockPostViewModel(headingText: "hello", subheadingText: "world", cover: "!!", components: [])
    let testObject = WWPostPresenter(view: mockView, cellProvider: WWPostDetailsCellProvider(viewModel: mockPostViewModel, photoService: mockPhotoService))
    
    testObject.peek()
    
    mockView.verify(function: "setTopOffset", with: CGFloat(0))
    mockView.verify(function: "setCloseButtonHidden(_:animated:)", with: true, at: 0)
    mockView.verify(function: "setCloseButtonHidden(_:animated:)", with: false, at: 1)
  }
  
  func testPop_SetsTopOffsetAndShowsCloseButton() {
    let mockView = MockPostView()
    let mockPhotoService = MockPhotoService()
    let mockPostViewModel = MockPostViewModel(headingText: "hello", subheadingText: "world", cover: "!!", components: [])
    let testObject = WWPostPresenter(view: mockView, cellProvider: WWPostDetailsCellProvider(viewModel: mockPostViewModel, photoService: mockPhotoService))
    
    testObject.pop()
    
    mockView.verify(function: "setTopOffset", with: UIApplication.shared.statusBarFrame.size.height)
    mockView.verify(function: "setCloseButtonHidden(_:animated:)", with: false, at: 0)
    mockView.verify(function: "setCloseButtonHidden(_:animated:)", with: false, at: 1)
  }
  
  func testDidClose_NotifiesObserver() {
    let mockObserver = MockObserver()
    let mockView = MockPostView()
    let mockPhotoService = MockPhotoService()
    let mockPostViewModel = MockPostViewModel(headingText: "hello", subheadingText: "world", cover: "!!", components: [])
    let testObject = WWPostPresenter(view: mockView, cellProvider: WWPostDetailsCellProvider(viewModel: mockPostViewModel, photoService: mockPhotoService))
    testObject.observer = mockObserver
    
    mockView.observer?.didPressCloseButton()
    
    mockObserver.verify(function: "didPressCloseButton()")
  }
  
  func testDidUpdateScroll_GivenScrollIsInsideHeader_SetsColors() {
    let mockObserver = MockObserver()
    let mockView = MockPostView()
    let mockPhotoService = MockPhotoService()
    let mockPostViewModel = MockPostViewModel(headingText: "hello", subheadingText: "world", cover: "!!", components: [])
    let testObject = WWPostPresenter(view: mockView, cellProvider: WWPostDetailsCellProvider(viewModel: mockPostViewModel, photoService: mockPhotoService))
    testObject.observer = mockObserver
    
    mockView.observer?.didUpdateScroll(with: 0)
    
    mockView.verify(function: "setCloseButtonBackgroundColor(_:for:)", with: UIColor(red: 1, green: 1, blue: 1, alpha: 0.8), at: 0)
    mockView.verify(function: "setCloseButtonBackgroundColor(_:for:)", with: UIControl.State.normal, at: 1)
    mockView.verify(function: "setCloseButtonForegroundColor(_:for:)", with: UIColor(red: 0, green: 0, blue: 0, alpha: 1.0), at: 0)
    mockView.verify(function: "setCloseButtonForegroundColor(_:for:)", with: UIControl.State.normal, at: 1)
  }
  
  func testDidUpdateScroll_GivenScrollIsOutsideHeader_SetsColors() {
    let mockObserver = MockObserver()
    let mockView = MockPostView()
    let mockPhotoService = MockPhotoService()
    let mockPostViewModel = MockPostViewModel(headingText: "hello", subheadingText: "world", cover: "!!", components: [])
    let testObject = WWPostPresenter(view: mockView, cellProvider: WWPostDetailsCellProvider(viewModel: mockPostViewModel, photoService: mockPhotoService))
    testObject.observer = mockObserver
    mockView.headerHeight = 100
    mockView.closeButtonFrame = CGRect(x: 0, y: 0, width: 100, height: 100)
    
    let height = UIApplication.shared.statusBarFrame.size.height + 100
    
    mockView.observer?.didUpdateScroll(with: height)
    
    mockView.verify(function: "setCloseButtonBackgroundColor(_:for:)", with: UIColor(red: 0, green: 0, blue: 0, alpha: 0.8), at: 0)
    mockView.verify(function: "setCloseButtonBackgroundColor(_:for:)", with: UIControl.State.normal, at: 1)
    mockView.verify(function: "setCloseButtonForegroundColor(_:for:)", with: UIColor(red: 1, green: 1, blue: 1, alpha: 1.0), at: 0)
    mockView.verify(function: "setCloseButtonForegroundColor(_:for:)", with: UIControl.State.normal, at: 1)
  }
  
  func testDidUpdateScroll_GivenScrollIsInbetweenHeader_SetsColors() {
    let mockObserver = MockObserver()
    let mockView = MockPostView()
    let mockPhotoService = MockPhotoService()
    let mockPostViewModel = MockPostViewModel(headingText: "hello", subheadingText: "world", cover: "!!", components: [])
    let testObject = WWPostPresenter(view: mockView, cellProvider: WWPostDetailsCellProvider(viewModel: mockPostViewModel, photoService: mockPhotoService))
    testObject.observer = mockObserver
    
    let navBarHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height
    let headerHeight: CGFloat = 100
    let closeButtonHeight: CGFloat = 100
    let closeButtonPosition: CGFloat = 20
    
    mockView.headerHeight = headerHeight
    mockView.closeButtonFrame = CGRect(x: 0, y: closeButtonPosition, width: closeButtonHeight, height: closeButtonHeight)
    
    let height = navBarHeight + headerHeight - (closeButtonPosition + closeButtonHeight / 2.0)
    
    mockView.observer?.didUpdateScroll(with: height)
    
    mockView.verify(function: "setCloseButtonBackgroundColor(_:for:)", with: UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.8), at: 0)
    mockView.verify(function: "setCloseButtonBackgroundColor(_:for:)", with: UIControl.State.normal, at: 1)
    mockView.verify(function: "setCloseButtonForegroundColor(_:for:)", with: UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0), at: 0)
    mockView.verify(function: "setCloseButtonForegroundColor(_:for:)", with: UIControl.State.normal, at: 1)
  }
  
  func testDidClose_ScrollsTable() {
    let mockView = MockPostView()
    let mockPhotoService = MockPhotoService()
    let mockPostViewModel = MockPostViewModel(headingText: "hello", subheadingText: "world", cover: "!!", components: [])
    _ = WWPostPresenter(view: mockView, cellProvider: WWPostDetailsCellProvider(viewModel: mockPostViewModel, photoService: mockPhotoService))
    
    mockView.headerHeight = 123
    
    mockView.observer?.didPressCloseButton()
    
    mockView.verify(function: "setScrollPosition(_:animated:)", numberOf: 2)
    
    if let position = mockView.arguments(for: "setScrollPosition(_:animated:)", at: 0)?[0] as? CGFloat  {
      XCTAssertEqual(position, mockView.headerHeight + 200)
    } else {
      XCTFail("Must scroll to header height first")
    }
    
    if let position = mockView.arguments(for: "setScrollPosition(_:animated:)", at: 1)?[0] as? CGFloat  {
      XCTAssertEqual(position, 0)
    } else {
      XCTFail("Must scroll to top second")
    }
  }
  
  func testDidClose_HidesCloseButton() {
    let mockView = MockPostView()
    let mockPhotoService = MockPhotoService()
    let mockPostViewModel = MockPostViewModel(headingText: "hello", subheadingText: "world", cover: "!!", components: [])
    _ = WWPostPresenter(view: mockView, cellProvider: WWPostDetailsCellProvider(viewModel: mockPostViewModel, photoService: mockPhotoService))
    
    mockView.observer?.didPressCloseButton()
    
    mockView.verify(function: "setCloseButtonHidden(_:animated:)", with: true, at: 0)
    mockView.verify(function: "setCloseButtonHidden(_:animated:)", with: true, at: 1)
  }
}

