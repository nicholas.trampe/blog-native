//
//  GalleryPresenterTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 7/5/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class GalleryPresenterTests: XCTestCase {
  class MockView: UIView, GalleryView, Mock {
    var observer: GalleryViewObserver? = nil
    var visibleImage: UIImageView? = nil
    var calls: [Call] = []
    
    func set(image: UIImage, at index: Int) {
      calls.append(Call(name: #function, arguments: [image, index]))
    }
    
    func removeAll() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func scrollTo(page: UInt) {
      calls.append(Call(name: #function, arguments: [page]))
    }
    
    func setPageControlTotal(page: Int) {
      calls.append(Call(name: #function, arguments: [page]))
    }
    
    func setPageControlCurrent(page: Int) {
      calls.append(Call(name: #function, arguments: [page]))
    }
    
    func setPageControlHidden(hidden: Bool) {
      calls.append(Call(name: #function, arguments: [hidden]))
    }
    
    func setSpinnerHidden(hidden: Bool) {
      calls.append(Call(name: #function, arguments: [hidden]))
    }
    
    func updateVisibleImageTransform(_ transform: CGAffineTransform, animated: Bool) {
      calls.append(Call(name: #function, arguments: [transform, animated]))
    }
    
    func bringVisibleImageToFront() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  class MockModel: GalleryModel, Mock {
    var observer: GalleryModelObserver? = nil
    var photoIds: [String] = []
    var currentPage: UInt = 0
    var interval: TimeInterval = 5.0
    var calls: [Call] = []
    
    func retrieveImages() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  class MockHaptics: HapticGenerator, Mock {
    var calls: [Call] = []
    
    func prepare() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func generate() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  class MockTransformer: GalleryImageTransformer, Mock {
    var calls: [Call] = []
    var result: CGAffineTransform = .identity
    
    func scaleTransform(with force: CGFloat) -> CGAffineTransform {
      calls.append(Call(name: #function, arguments: [force]))
      return result
    }
    
    func popTransform(toView: UIView, fromView: UIView) -> CGAffineTransform {
      calls.append(Call(name: #function, arguments: [toView, fromView]))
      return result
    }
    
    func slideTransform(distance: CGPoint, currentTransform: CGAffineTransform, initialTransform: CGAffineTransform) -> CGAffineTransform {
      calls.append(Call(name: #function, arguments: [distance, currentTransform, initialTransform]))
      return result
    }
  }
  
  class MockObserver: GalleryPresenterObserver, Mock {
    var calls: [Call] = []
    
    func didPeek() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func didUnPeek() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  func test_Init_GivenMultiplePages_SetsPageControl() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    
    mockModel.photoIds = ["1","1","1","1"]
    
    _ = WWGalleryPresenter(view: mockView,
                           model: mockModel,
                           hapticGenerator: mockHaptics,
                           imageTransformer: mockTransformer)
    
    mockView.verify(function: "setPageControlTotal(page:)", with: 4)
    mockView.verify(function: "setPageControlCurrent(page:)", with: 0)
    mockView.verify(function: "setPageControlHidden(hidden:)", with: false)
  }
  
  func test_Init_GivenSinglePage_SetsPageControl() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    
    mockModel.photoIds = ["1"]
    
    _ = WWGalleryPresenter(view: mockView,
                           model: mockModel,
                           hapticGenerator: mockHaptics,
                           imageTransformer: mockTransformer)
    
    mockView.verify(function: "setPageControlTotal(page:)", with: 1)
    mockView.verify(function: "setPageControlCurrent(page:)", with: 0)
    mockView.verify(function: "setPageControlHidden(hidden:)", with: true)
  }
  
  func test_Init_StartsSpinner() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    
    _ = WWGalleryPresenter(view: mockView,
                           model: mockModel,
                           hapticGenerator: mockHaptics,
                           imageTransformer: mockTransformer)
    
    mockView.verify(function: "setSpinnerHidden(hidden:)", with: false)
  }
  
  func test_Start_RetrievesImages() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    let testObject = WWGalleryPresenter(view: mockView,
                                        model: mockModel,
                                        hapticGenerator: mockHaptics,
                                        imageTransformer: mockTransformer)
    
    testObject.start()
    
    mockModel.verify(function: "retrieveImages()")
  }
  
  func test_Start_AutoScrolls() {
    let interval = 1.0
    let expectationInterval = 1.1
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    let testObject = WWGalleryPresenter(view: mockView,
                                        model: mockModel,
                                        hapticGenerator: mockHaptics,
                                        imageTransformer: mockTransformer)
    
    mockModel.photoIds = ["1","2","3"]
    mockModel.interval = interval
    
    XCTAssertFalse(testObject.isAutomaticallyScrolling)
    
    testObject.start()
    
    XCTAssertTrue(testObject.isAutomaticallyScrolling)
    mockView.verifyNot(function: "scrollTo(page:)")
    
    wait(for: [mockView.expectation(for: "scrollTo(page:)", with: 1)], timeout: expectationInterval, enforceOrder: false)
    
    mockView.verify(function: "scrollTo(page:)", with: UInt(1))
    mockModel.currentPage = 1
    
    wait(for: [mockView.expectation(for: "scrollTo(page:)", with: 2)], timeout: expectationInterval, enforceOrder: false)
    
    mockView.verify(function: "scrollTo(page:)", with: UInt(2))
    mockModel.currentPage = 2
    
    wait(for: [mockView.expectation(for: "scrollTo(page:)", with: 3)], timeout: expectationInterval, enforceOrder: false)
    
    mockView.verify(function: "scrollTo(page:)", with: UInt(0))
  }
  
  func test_Reset_RemovesImages() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    let testObject = WWGalleryPresenter(view: mockView,
                                        model: mockModel,
                                        hapticGenerator: mockHaptics,
                                        imageTransformer: mockTransformer)
    
    testObject.reset()
    
    mockView.verify(function: "removeAll()")
  }
  
  func test_Reset_StopsAutoScrolling() {
    let interval = 1.0
    let expectationInterval = 1.1
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    let testObject = WWGalleryPresenter(view: mockView,
                                        model: mockModel,
                                        hapticGenerator: mockHaptics,
                                        imageTransformer: mockTransformer)
    
    mockModel.photoIds = ["1","2","3"]
    mockModel.interval = interval
    
    testObject.start()
    
    XCTAssertTrue(testObject.isAutomaticallyScrolling)
    mockView.verifyNot(function: "scrollTo(page:)")
    
    wait(for: [mockView.expectation(for: "scrollTo(page:)", with: 1)], timeout: expectationInterval, enforceOrder: false)
    
    mockView.verify(function: "scrollTo(page:)", with: UInt(1))
    
    testObject.reset()
    
    XCTAssertFalse(testObject.isAutomaticallyScrolling)
    
    usleep(UInt32(expectationInterval * 1000))
    
    mockView.verify(function: "scrollTo(page:)", numberOf: 1)
  }
  
  func test_DidRetrieveImage_UpdatesView() {
    let image = UIImage()
    let index: Int = 123
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    _ = WWGalleryPresenter(view: mockView,
                           model: mockModel,
                           hapticGenerator: mockHaptics,
                           imageTransformer: mockTransformer)
    
    mockModel.observer?.didRetrieve(image: image, at: index)
    
    mockView.verify(function: "set(image:at:)", with: image, at: 0)
    mockView.verify(function: "set(image:at:)", with: index, at: 1)
  }
  
  func test_didScroll_UpdatesCurrentPage() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    _ = WWGalleryPresenter(view: mockView,
                           model: mockModel,
                           hapticGenerator: mockHaptics,
                           imageTransformer: mockTransformer)
    
    mockView.frame.size.width = 100
    mockModel.photoIds = ["1","2","3"]
    
    mockView.observer?.didUpdateScroll(to: 0)
    XCTAssertEqual(mockModel.currentPage, 0)
    
    mockView.observer?.didUpdateScroll(to: 100)
    XCTAssertEqual(mockModel.currentPage, 1)
    
    mockView.observer?.didUpdateScroll(to: 200)
    XCTAssertEqual(mockModel.currentPage, 2)
  }
  
  func test_didScroll_GivenNoPhotos_ThenDoesNotUpdateCurrentPage() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    _ = WWGalleryPresenter(view: mockView,
                           model: mockModel,
                           hapticGenerator: mockHaptics,
                           imageTransformer: mockTransformer)
    
    mockView.frame.size.width = 100
    mockModel.photoIds = []
    mockModel.currentPage = 123
    
    mockView.observer?.didUpdateScroll(to: 300)
    
    XCTAssertEqual(mockModel.currentPage, 123)
  }
  
  func test_willBeginDragging_StopsScroll() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    let testObject = WWGalleryPresenter(view: mockView,
                                        model: mockModel,
                                        hapticGenerator: mockHaptics,
                                        imageTransformer: mockTransformer)
    
    mockModel.photoIds = ["1","2","3"]
    
    testObject.start()
    
    XCTAssertTrue(testObject.isAutomaticallyScrolling)
    
    mockView.observer?.willBeginDragging()
    
    XCTAssertFalse(testObject.isAutomaticallyScrolling)
  }
  
  func test_didEndDragging_StartsScroll() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    let testObject = WWGalleryPresenter(view: mockView,
                                        model: mockModel,
                                        hapticGenerator: mockHaptics,
                                        imageTransformer: mockTransformer)
    
    mockModel.photoIds = ["1","2","3"]
    
    XCTAssertFalse(testObject.isAutomaticallyScrolling)
    
    mockView.observer?.didEndDragging()
    
    XCTAssertTrue(testObject.isAutomaticallyScrolling)
  }
  
  func test_didForceTouchImage_OnBegin_PreparesHaptics() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    _ = WWGalleryPresenter(view: mockView,
                           model: mockModel,
                           hapticGenerator: mockHaptics,
                           imageTransformer: mockTransformer)
    
    mockView.observer?.didForceTouchImage(with: 1.0, traveled: .zero, at: .began)
    
    mockHaptics.verify(function: "prepare()")
  }
  
  func test_didForceTouchImage_OnBegin_StopsAutoScroll() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    let testObject = WWGalleryPresenter(view: mockView,
                                        model: mockModel,
                                        hapticGenerator: mockHaptics,
                                        imageTransformer: mockTransformer)
    
    mockView.observer?.didForceTouchImage(with: 1.0, traveled: .zero, at: .began)
    
    XCTAssertFalse(testObject.isAutomaticallyScrolling)
  }
  
  func test_didForceTouchImage_OnBegin_UpdatesViewTransformWithForce() {
    let force: CGFloat = 0.85
    let transform = CGAffineTransform(a: 1, b: 1, c: 1, d: 1, tx: 1, ty: 1)
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    _ = WWGalleryPresenter(view: mockView,
                           model: mockModel,
                           hapticGenerator: mockHaptics,
                           imageTransformer: mockTransformer)
    
    mockTransformer.result = transform
    mockView.observer?.didForceTouchImage(with: force, traveled: .zero, at: .began)
    
    mockTransformer.verify(function: "scaleTransform(with:)", with: force)
    mockView.verify(function: "updateVisibleImageTransform(_:animated:)", with: transform, at: 0)
  }
  
  func test_didForceTouchImage_OnBegin_NotifiesObserver() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockObserver = MockObserver()
    let mockTransformer = MockTransformer()
    let testObject = WWGalleryPresenter(view: mockView,
                                        model: mockModel,
                                        hapticGenerator: mockHaptics,
                                        imageTransformer: mockTransformer)
    testObject.observer = mockObserver
    
    mockView.observer?.didForceTouchImage(with: 1.0, traveled: .zero, at: .began)
    
    mockObserver.verify(function: "didPeek()")
  }
  
  func test_didForceTouchImage_OnBegin_BringsVisibleImageToFront() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    _ = WWGalleryPresenter(view: mockView,
                                        model: mockModel,
                                        hapticGenerator: mockHaptics,
                                        imageTransformer: mockTransformer)
    
    mockView.observer?.didForceTouchImage(with: 1.0, traveled: .zero, at: .began)
    
    mockView.verify(function: "bringVisibleImageToFront()")
  }
  
  func test_didForceTouchImage_OnChanged_GivenForceIsLessThanThreshold_AndNotPopping_ThenUpdatesViewTransform() {
    let force: CGFloat = 0.65
    let transform = CGAffineTransform(a: 1, b: 1, c: 1, d: 1, tx: 1, ty: 1)
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    _ = WWGalleryPresenter(view: mockView,
                           model: mockModel,
                           hapticGenerator: mockHaptics,
                           imageTransformer: mockTransformer)
    
    mockTransformer.result = transform
    
    mockView.observer?.didForceTouchImage(with: force, traveled: .zero, at: .changed)
    
    mockTransformer.verify(function: "scaleTransform(with:)", with: force)
    mockView.verify(function: "updateVisibleImageTransform(_:animated:)", with: transform, at: 0)
  }
  
  func test_didForceTouchImage_OnChanged_GivenForceIsGreaterThanThreshold_AndNotPopping_ThenGeneratesHaptics() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    let image = UIImageView(frame: .zero)
    _ = WWGalleryPresenter(view: mockView,
                           model: mockModel,
                           hapticGenerator: mockHaptics,
                           imageTransformer: mockTransformer)
    mockView.visibleImage = image
    
    mockView.observer?.didForceTouchImage(with: 1.0, traveled: .zero, at: .changed)
    
    mockHaptics.verify(function: "generate()")
  }
  
  func test_didForceTouchImage_OnChanged_GivenForceIsGreaterThanThreshold_AndNotPopping_ThenUpdatesViewTransform() {
    let window = UIApplication.shared.keyWindow!
    let transform = CGAffineTransform(a: 1, b: 1, c: 1, d: 1, tx: 1, ty: 1)
    let currentTransform = CGAffineTransform(a: 2, b: 3, c: 2, d: 2, tx: 2, ty: 2)
    let image = UIImageView(frame: .zero)
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    _ = WWGalleryPresenter(view: mockView,
                           model: mockModel,
                           hapticGenerator: mockHaptics,
                           imageTransformer: mockTransformer)
    
    image.transform = currentTransform
    mockTransformer.result = transform
    mockView.visibleImage = image
    
    mockView.observer?.didForceTouchImage(with: 1.0, traveled: .zero, at: .changed)
    
    mockTransformer.verify(function: "popTransform(toView:fromView:)", with: window, at: 0)
    mockTransformer.verify(function: "popTransform(toView:fromView:)", with: image, at: 1)
    mockView.verify(function: "updateVisibleImageTransform(_:animated:)", with: transform, at: 0)
  }
  
  func test_didForceTouchImage_OnChanged_GivenForceIsGreaterThanThreshold_AndNotPopping_ThenHidesPageControl() {
    let image = UIImageView(frame: .zero)
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    _ = WWGalleryPresenter(view: mockView,
                           model: mockModel,
                           hapticGenerator: mockHaptics,
                           imageTransformer: mockTransformer)
    
    mockView.visibleImage = image
    
    mockView.observer?.didForceTouchImage(with: 1.0, traveled: .zero, at: .changed)
    
    mockView.verify(function: "setPageControlHidden(hidden:)", with: true)
  }
  
  func test_didForceTouchImage_OnChanged_GivenPopping_ThenUpdatesViewTransform() {
    let initialTransform = CGAffineTransform(a: 1, b: 1, c: 1, d: 1, tx: 1, ty: 1)
    let currentTransform = CGAffineTransform(a: 2, b: 2, c: 2, d: 2, tx: 2, ty: 2)
    let expectedTransform = CGAffineTransform(a: 3, b: 3, c: 3, d: 3, tx: 3, ty: 3)
    let distance = CGPoint(x: 12, y: 34)
    let image = UIImageView(frame: .zero)
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    _ = WWGalleryPresenter(view: mockView,
                           model: mockModel,
                           hapticGenerator: mockHaptics,
                           imageTransformer: mockTransformer)
    
    // Get into popping state by force touching over the threshold
    image.transform = currentTransform
    mockTransformer.result = initialTransform
    mockView.visibleImage = image
    mockView.observer?.didForceTouchImage(with: 1.0, traveled: .zero, at: .changed)
    mockView.verify(function: "updateVisibleImageTransform(_:animated:)", with: initialTransform, at: 0)
    
    // While in popping state, move finger
    mockTransformer.result = expectedTransform
    mockView.observer?.didForceTouchImage(with: 1.0, traveled: distance, at: .changed)
    
    // Check slide
    mockTransformer.verify(function: "slideTransform(distance:currentTransform:initialTransform:)", with: distance, at: 0)
    mockTransformer.verify(function: "slideTransform(distance:currentTransform:initialTransform:)", with: currentTransform, at: 1)
    mockTransformer.verify(function: "slideTransform(distance:currentTransform:initialTransform:)", with: initialTransform, at: 2)
    mockView.verify(function: "updateVisibleImageTransform(_:animated:)", with: expectedTransform, at: 0)
  }
  
  func test_didForceTouchImage_OnChanged_GivenNotPeeking_NotifiesObserver() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockObserver = MockObserver()
    let mockTransformer = MockTransformer()
    let testObject = WWGalleryPresenter(view: mockView,
                                        model: mockModel,
                                        hapticGenerator: mockHaptics,
                                        imageTransformer: mockTransformer)
    testObject.observer = mockObserver
    
    mockView.observer?.didForceTouchImage(with: 0.5, traveled: .zero, at: .changed)
    mockView.observer?.didForceTouchImage(with: 0.5, traveled: .zero, at: .changed)
    
    
    mockObserver.verify(function: "didPeek()", numberOf: 1)
  }
  
  func test_didForceTouchImage_OnChanged_GivenNotPeeking_BringsVisibleImageToFront() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockTransformer = MockTransformer()
    _ = WWGalleryPresenter(view: mockView,
                           model: mockModel,
                           hapticGenerator: mockHaptics,
                           imageTransformer: mockTransformer)
    
    mockView.observer?.didForceTouchImage(with: 0.5, traveled: .zero, at: .changed)
    mockView.observer?.didForceTouchImage(with: 0.5, traveled: .zero, at: .changed)
    
    mockView.verify(function: "bringVisibleImageToFront()")
  }
  
  func test_didForceTouchImage_OnEnded_GivenPeeking_NotifiesObserver() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockObserver = MockObserver()
    let mockTransformer = MockTransformer()
    let testObject = WWGalleryPresenter(view: mockView,
                                        model: mockModel,
                                        hapticGenerator: mockHaptics,
                                        imageTransformer: mockTransformer)
    testObject.observer = mockObserver
    
    mockView.observer?.didForceTouchImage(with: 0.5, traveled: .zero, at: .ended)
    
    mockObserver.verify(function: "didUnPeek()", numberOf: 0)
    
    mockView.observer?.didForceTouchImage(with: 0.5, traveled: .zero, at: .changed)
    mockView.observer?.didForceTouchImage(with: 0.5, traveled: .zero, at: .ended)
    
    mockObserver.verify(function: "didUnPeek()", numberOf: 1)
  }
  
  func test_didForceTouchImage_OnEnded_GivenPeeking_AndMultiplePages_ThenShowsPageControl() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockObserver = MockObserver()
    let mockTransformer = MockTransformer()
    let testObject = WWGalleryPresenter(view: mockView,
                                        model: mockModel,
                                        hapticGenerator: mockHaptics,
                                        imageTransformer: mockTransformer)
    testObject.observer = mockObserver
    mockModel.photoIds = ["1","2"]
    
    mockView.observer?.didForceTouchImage(with: 0.5, traveled: .zero, at: .changed)
    mockView.observer?.didForceTouchImage(with: 0.5, traveled: .zero, at: .ended)
    
    mockView.verify(function: "setPageControlHidden(hidden:)", with: false)
  }
  
  func test_didForceTouchImage_OnEnded_GivenPeeking_AndSinglePage_ThenHidesPageControl() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockHaptics = MockHaptics()
    let mockObserver = MockObserver()
    let mockTransformer = MockTransformer()
    let testObject = WWGalleryPresenter(view: mockView,
                                        model: mockModel,
                                        hapticGenerator: mockHaptics,
                                        imageTransformer: mockTransformer)
    testObject.observer = mockObserver
    mockModel.photoIds = ["1"]
    
    mockView.observer?.didForceTouchImage(with: 0.5, traveled: .zero, at: .changed)
    mockView.observer?.didForceTouchImage(with: 0.5, traveled: .zero, at: .ended)
    
    mockView.verify(function: "setPageControlHidden(hidden:)", with: true)
  }
}

