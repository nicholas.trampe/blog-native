//
//  MenuModelTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class MenuModelTests: XCTestCase {
  class MockObserver: MenuModelObserver, Mock {
    var calls: [Call] = []
    
    func didSelectItem(at index: Int) {
      calls.append(Call(name: #function, arguments: [index]))
    }
  }
  
  func testInit_Items() {
    let testObject = WWMenuModel()
    
    XCTAssertEqual(testObject.items, [MenuItem(title: "About", icon: .info),
                                      MenuItem(title: "Contact", icon: .mail),
                                      MenuItem(title: "Instagram", icon: .instagram),
                                      MenuItem(title: "Facebook", icon: .facebook),
                                      MenuItem(title: "Couchsurfing", icon: .earth),
                                      MenuItem(title: "GitLab", icon: .git),
                                      MenuItem(title: "Settings", icon: .cog)])
  }
  
  func testSelectItem_NotifiesObserver() {
    let index = 1
    let mockObserver = MockObserver()
    let testObject = WWMenuModel()
    testObject.observer = mockObserver
    
    testObject.selectItem(at: index)
    
    mockObserver.verify(function: "didSelectItem(at:)", with: index)
  }
}
