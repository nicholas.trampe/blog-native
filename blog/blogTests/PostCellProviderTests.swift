//
//  PostCellProviderTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/15/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import Promises
@testable import blog

class PostCellProviderTests: XCTestCase {  
  struct MockPostCellViewModel: PostCellViewModel {
    let headingText: String
    let subheadingText: String
    let cover: String
    let dateText: String
  }
  
  func testNumberOfRows() {
    let table = UITableView()
    let mockPhotoService = MockPhotoService()
    let testObject = WWPostCellProvider(photoService: mockPhotoService)
    let viewModels = sampleViewModels()
    
    testObject.viewModels = viewModels
    
    XCTAssertEqual(testObject.tableView(table, numberOfRowsInSection: 0), viewModels.count)
  }
  
  func testCellForRow_SetsText() {
    let table = UITableView()
    let mockPhotoService = MockPhotoService()
    let testObject = WWPostCellProvider(photoService: mockPhotoService)
    let viewModels = sampleViewModels()
    
    testObject.registerCells(for: table)
    testObject.viewModels = viewModels
    
    let cell = testObject.tableView(table, cellForRowAt: IndexPath(row: 1, section: 0)) as! PostCell
    
    XCTAssertEqual(cell.heading.headingLabel.text, "post 2")
    XCTAssertEqual(cell.heading.subheadingLabel.text, "sub 2")
    XCTAssertEqual(cell.heading.dateLabel.text, "date 2")
  }
  
  func testCellForRow_SetsCover_Success() {
    let image = UIImage()
    let table = UITableView()
    let mockPhotoService = MockPhotoService()
    let testObject = WWPostCellProvider(photoService: mockPhotoService)
    let viewModels = sampleViewModels()
    
    testObject.registerCells(for: table)
    testObject.viewModels = viewModels
    mockPhotoService.promise = Promise<UIImage>(image)
    
    let cell = testObject.tableView(table, cellForRowAt: IndexPath(row: 1, section: 0)) as! PostCell
    
    XCTAssert(waitForPromises(timeout: 1))
    XCTAssertEqual(cell.heading.backgroundImage.image, image)
    mockPhotoService.verify(function: "get(id:)", with: "cover 2")
  }
  
  func testCellForRow_SetsCover_Failure() {
    let table = UITableView()
    let mockPhotoService = MockPhotoService()
    let testObject = WWPostCellProvider(photoService: mockPhotoService)
    let viewModels = sampleViewModels()
    
    testObject.registerCells(for: table)
    testObject.viewModels = viewModels
    mockPhotoService.promise = Promise<UIImage>(ServiceError.invalidData)
    
    let cell = testObject.tableView(table, cellForRowAt: IndexPath(row: 1, section: 0)) as! PostCell
    
    XCTAssert(waitForPromises(timeout: 1))
    XCTAssertNil(cell.heading.backgroundImage.image)
  }
  
  private func sampleViewModels() -> [PostCellViewModel] {
    return [
      MockPostCellViewModel(headingText: "post 1", subheadingText: "sub 1", cover: "cover 1", dateText: "date 1"),
      MockPostCellViewModel(headingText: "post 2", subheadingText: "sub 2", cover: "cover 2", dateText: "date 2"),
      MockPostCellViewModel(headingText: "post 3", subheadingText: "sub 3", cover: "cover 3", dateText: "date 3")
    ]
  }
}

