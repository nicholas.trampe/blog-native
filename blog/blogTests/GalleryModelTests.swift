//
//  GalleryModelTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 7/4/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import Promises
@testable import blog

class GalleryModelTests: XCTestCase {
  
  class MockObserver: GalleryModelObserver, Mock {
    var calls: [Call] = []
    
    func didRetrieve(image: UIImage, at index: Int) {
      calls.append(Call(name: #function, arguments: [image, index]))
    }
    
    func didFailToRetrieveImage(at index: Int, with error: Error) {
      calls.append(Call(name: #function, arguments: [index, error]))
    }
  }
  
  func test_Init_SeparatesComponents() {
    let mockPhotoService = MockPhotoService()
    let testObject = WWGalleryModel(photoService: mockPhotoService, photoIdsComponent: "1,2,3,e,5,hi,7,8")
    
    XCTAssertEqual(testObject.photoIds, ["1","2","3","e","5","hi","7","8"])
  }
  
  func test_RetrieveImages_CallsPhotoService() {
    let mockPhotoService = MockPhotoService()
    let testObject = WWGalleryModel(photoService: mockPhotoService, photoIdsComponent: "1,2,3")
    
    testObject.retrieveImages()
    
    mockPhotoService.verify(function: "get(id:)", numberOf: 3)
    mockPhotoService.verify(function: "get(id:)", with: "1", at: 0)
    mockPhotoService.verify(function: "get(id:)", with: "2", at: 0)
    mockPhotoService.verify(function: "get(id:)", with: "3", at: 0)
  }
  
  func test_RetrieveImages_GivenPhotoServiceSucceeds_ThenNotifiesObserver() {
    let image = UIImage()
    let result = Promise<UIImage>(image)
    let mockPhotoService = MockPhotoService()
    let mockObserver = MockObserver()
    let testObject = WWGalleryModel(photoService: mockPhotoService, photoIdsComponent: "1")
    
    testObject.observer = mockObserver
    mockPhotoService.promise = result
    
    testObject.retrieveImages()
    
    _ = waitForPromises(timeout: 1)
    mockPhotoService.verify(function: "get(id:)", with: "1", at: 0)
    mockObserver.verify(function: "didRetrieve(image:at:)", with: image, at: 0)
    mockObserver.verify(function: "didRetrieve(image:at:)", with: 0, at: 1)
  }
  
  func test_RetrieveImages_GivenPhotoServiceFails_ThenNotifiesObserver() {
    let error = ServiceError.invalidData
    let result = Promise<UIImage>(error)
    let mockPhotoService = MockPhotoService()
    let mockObserver = MockObserver()
    let testObject = WWGalleryModel(photoService: mockPhotoService, photoIdsComponent: "1")
    
    testObject.observer = mockObserver
    mockPhotoService.promise = result
    
    testObject.retrieveImages()
    
    _ = waitForPromises(timeout: 1)
    mockPhotoService.verify(function: "get(id:)", with: "1", at: 0)
    mockObserver.verify(function: "didFailToRetrieveImage(at:with:)", with: 0, at: 0)
    mockObserver.verify(function: "didFailToRetrieveImage(at:with:)", with: error, at: 1)
  }
}
