//
//  ContactServiceTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class ContactServiceTests: XCTestCase {
  func testContact() {
    let mockService = MockService()
    let testObject = WWContactService(service: mockService)
    let exp = expectation(description: "Service called")
    
    let name = "Jeffery"
    let email = "cucumber"
    let message = "I like cucumbers"
    
    let body = ["name": name,
                "email": email,
                "message": message]
    
    testObject.contact(name: name, email: email, message: message).then { data in
      exp.fulfill()
    }
    
    waitForExpectations(timeout: 0.1, handler: nil)
    
    mockService.verify(function: "post(route:body:)", with: "deets", at: 0)
    
    if let actualBody = mockService.call(for: "post(route:body:)")?.arguments[1] as? [String:String] {
      XCTAssertEqual(actualBody, body)
    } else {
      XCTFail("Body is not a string to string dictionary")
    }
  }
}
