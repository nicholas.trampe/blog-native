//
//  PlacesPresenterTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 9/10/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class PlacesPresenterTests: XCTestCase {
  class MockView: PlacesView, Mock {
    var calls: [Call] = []
    var observer: PlacesViewObserver? = nil
    var isGestureInteractionEnabled: Bool = false
    var zoom: Float = 0
    var latitude: Double = 0
    var longitude: Double = 0
    
    func addMarker(marker: Marker) {
      calls.append(Call(name: #function, arguments: [marker]))
    }
    
    func moveCamera(latitude: Double, longitude: Double, zoom: Float, animated: Bool) {
      calls.append(Call(name: #function, arguments: [latitude, longitude, zoom, animated]))
    }
    
    func showContentView() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func updateContentView(heading: String?, subheading: String?, text: String?) {
      calls.append(Call(name: #function, arguments: [heading ?? "", subheading ?? "", text ?? ""]))
    }
    
    func insert(image: UIImage) {
      calls.append(Call(name: #function, arguments: [image]))
    }
    
    func removeAllImages() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func hideContentView() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func removeFocus() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  class MockModel: PlacesModel, Mock {
    var calls: [Call] = []
    var places: [Place] = []
    var observer: PlacesModelObserver? = nil
    var startingLocation: Location = Location(latitude: 0, longitude: 0)
    
    func updatePlaces() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func findPlaces() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func loadPhotos(for placeID: String) {
      calls.append(Call(name: #function, arguments: [placeID]))
    }
  }
  
  class MockObserver: PlacesPresenterObserver, Mock {
    var calls: [Call] = []
    
    func didPressCloseButton() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  func test_init_SetsCamera() {
    let mockView = MockView()
    let mockModel = MockModel()
    mockModel.startingLocation = Location(latitude: 123.0, longitude: 456.0)
    
    _ = WWPlacesPresenter(view: mockView, model: mockModel)
    
    if let call = mockView.call(for: "moveCamera(latitude:longitude:zoom:animated:)") {
      XCTAssertEqual(call.arguments[0] as? Double, 123)
      XCTAssertEqual(call.arguments[1] as? Double, 456)
      XCTAssertEqual(call.arguments[2] as? Float, 3)
      XCTAssertEqual(call.arguments[3] as? Bool, false)
    } else {
      XCTFail("Move not called")
    }
  }
  
  func test_start_CallsUpdate() {
    let mockView = MockView()
    let mockModel = MockModel()
    let testObject = WWPlacesPresenter(view: mockView, model: mockModel)
    
    testObject.start()
    
    mockModel.verify(function: "updatePlaces()")
  }
  
  func test_didUpdatePlaces_UpdatesView() {
    let photo = Photo(_id: "123", filename: "filename", uploaded: .distantPast)
    let places = [Place(_id: "123", name: "place 1", description: "stuffs 1", latitude: 123, longitude: 456, date: "dater 1", photos: [photo]),
                  Place(_id: "456", name: "place 2", description: "stuffs 2", latitude: 789, longitude: 10, date: "dater 2", photos: [photo])]
    let mockView = MockView()
    let mockModel = MockModel()
    _ = WWPlacesPresenter(view: mockView, model: mockModel)
    
    mockModel.observer?.didUpdatePlaces(places:places)
    
    let expectedMarker1 = Marker(id: "123", title: "place 1", description: "stuffs 1", date: "dater 1", latitude: 123, longitude: 456)
    let expectedMarker2 = Marker(id: "456", title: "place 2", description: "stuffs 2", date: "dater 2", latitude: 789, longitude: 10)
    
    mockView.verify(function: "addMarker(marker:)", with: expectedMarker1)
    mockView.verify(function: "addMarker(marker:)", with: expectedMarker2)
  }
  
  func test_didLoad_UpdatesView() {
    let image = UIImage()
    let photo = Photo(_id: "123", filename: "filename", uploaded: .distantPast)
    let place = Place(_id: "123", name: "place 1", description: "stuffs 1", latitude: 123, longitude: 456, date: "dater 1", photos: [photo])
    let mockView = MockView()
    let mockModel = MockModel()
    _ = WWPlacesPresenter(view: mockView, model: mockModel)
    
    mockModel.observer?.didLoad(photo: image, for: place)
    
    mockView.verify(function: "insert(image:)", with: image)
  }
  
  func testCloseButton() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockObserver = MockObserver()
    let testObject = WWPlacesPresenter(view: mockView, model: mockModel)
    testObject.observer = mockObserver
    
    mockView.observer?.didPressCloseButton()
    
    mockObserver.verify(function: "didPressCloseButton()")
  }
  
  func test_didTapMarker_MovesCamera() {
    let mockView = MockView()
    let mockModel = MockModel()
    let marker = Marker(id: "123", title: "title", description: "description", date: "date", latitude: 123, longitude: 456)
    _ = WWPlacesPresenter(view: mockView, model: mockModel)
    
    mockView.observer?.didTap(marker: marker)
    
    if let call = mockView.call(for: "moveCamera(latitude:longitude:zoom:animated:)", at: 1) {
      XCTAssertEqual(call.arguments[0] as? Double, 123)
      XCTAssertEqual(call.arguments[1] as? Double, 456)
      XCTAssertEqual(call.arguments[2] as? Float, 5)
      XCTAssertEqual(call.arguments[3] as? Bool, true)
    } else {
      XCTFail("Move not called")
    }
  }
  
  func test_didTapMarker_ShowsContentView() {
    let mockView = MockView()
    let mockModel = MockModel()
    let marker = Marker(id: "123", title: "title", description: "description", date: "date", latitude: 123, longitude: 456)
    _ = WWPlacesPresenter(view: mockView, model: mockModel)
    
    mockView.observer?.didTap(marker: marker)
    
    mockView.verify(function: "removeAllImages()")
    mockView.verify(function: "showContentView()")
    
    if let call = mockView.call(for: "updateContentView(heading:subheading:text:)") {
      XCTAssertEqual(call.arguments[0] as? String, marker.title)
      XCTAssertEqual(call.arguments[1] as? String, marker.date)
      XCTAssertEqual(call.arguments[2] as? String, marker.description)
    } else {
      XCTFail("Show not called")
    }
  }
  
  func test_didTapMarker_LoadsPhotos() {
    let mockView = MockView()
    let mockModel = MockModel()
    let marker = Marker(id: "123", title: "title", description: "description", date: "date", latitude: 123, longitude: 456)
    _ = WWPlacesPresenter(view: mockView, model: mockModel)
    
    mockView.observer?.didTap(marker: marker)
    
    mockModel.verify(function: "loadPhotos(for:)", with: "123")
  }
  
  func test_didCloseMarker_HidesContentView() {
    let mockView = MockView()
    let mockModel = MockModel()
    _ = WWPlacesPresenter(view: mockView, model: mockModel)
    
    mockView.observer?.didCloseMarker()
    
    mockView.verify(function: "hideContentView()")
  }
  
  func test_didCloseMarker_MovesCamera() {
    let mockView = MockView()
    let mockModel = MockModel()
    _ = WWPlacesPresenter(view: mockView, model: mockModel)
    
    mockView.latitude = 123
    mockView.longitude = 456
    
    mockView.observer?.didCloseMarker()
    
    if let call = mockView.call(for: "moveCamera(latitude:longitude:zoom:animated:)", at: 1) {
      XCTAssertEqual(call.arguments[0] as? Double, 123)
      XCTAssertEqual(call.arguments[1] as? Double, 456)
      XCTAssertEqual(call.arguments[2] as? Float, 3)
      XCTAssertEqual(call.arguments[3] as? Bool, true)
    } else {
      XCTFail("Move not called")
    }
  }
  
  func test_didCloseContentView_RemovesFocus() {
    let mockView = MockView()
    let mockModel = MockModel()
    _ = WWPlacesPresenter(view: mockView, model: mockModel)
    
    mockView.latitude = 123
    mockView.longitude = 456
    
    mockView.observer?.didCloseContentView()
    
    if let call = mockView.call(for: "moveCamera(latitude:longitude:zoom:animated:)", at: 1) {
      XCTAssertEqual(call.arguments[0] as? Double, 123)
      XCTAssertEqual(call.arguments[1] as? Double, 456)
      XCTAssertEqual(call.arguments[2] as? Float, 3)
      XCTAssertEqual(call.arguments[3] as? Bool, true)
    } else {
      XCTFail("Move not called")
    }
    
    mockView.verify(function: "removeFocus()")
  }
}


