//
//  PhotoServiceTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import Promises
@testable import blog

class PhotoServiceTests: XCTestCase {  
  class MockCache: PhotoCache, Mock {
    var calls: [Call] = []
    var loadPromise = Promise<UIImage>(UIImage())
    var savePromise = Promise<UIImage>(UIImage())
    
    func loadPhoto(for id: String) -> Promise<UIImage> {
      calls.append(Call(name: #function, arguments: [id]))
      return loadPromise
    }
    
    func savePhoto(for id: String, data: Data) -> Promise<UIImage> {
      calls.append(Call(name: #function, arguments: [id, data]))
      return savePromise
    }
  }
  
  func testGet_GivenPhotoNotInCache_ThenCallsService_GivenBadData_AndCallsFailure() {
    let id = "123"
    let mockService = MockService()
    let mockCache = MockCache()
    let testObject = WWPhotoService(service: mockService, cache: mockCache)
    let exp = expectation(description: "Completion called")
    mockService.promise = Promise<Data>(Data())
    mockCache.loadPromise = Promise<UIImage>(PhotoCacheError.invalidData)
    mockCache.savePromise = Promise<UIImage>(PhotoCacheError.invalidData)
    
    testObject.get(id: id).then { image in
      XCTFail("Result should be failure")
    }.catch { error in
      if case PhotoCacheError.invalidData = error {
        exp.fulfill()
      } else {
        XCTFail("Error should be invalid data")
      }
    }
    
    waitForExpectations(timeout: 0.1, handler: nil)
    
    mockService.verify(function: "get(route:)", with: "photos/" + id)
  }
  
  func testGet_GivenPhotoNotInCache_ThenCallsService_GivenGoodData_AndSavesToCache() {
    let id = "123"
    let mockService = MockService()
    let mockCache = MockCache()
    let testObject = WWPhotoService(service: mockService, cache: mockCache)
    let exp = expectation(description: "Completion called")
    mockService.promise = Promise<Data>(validImageData())
    mockCache.loadPromise = Promise<UIImage>(PhotoCacheError.invalidID)
    
    testObject.get(id: id).then { image in
      exp.fulfill()
    }.catch { error in
      XCTFail("Result should be success")
    }
    
    waitForExpectations(timeout: 0.1, handler: nil)
    
    _ = waitForPromises(timeout: 1)
    mockCache.verify(function: "savePhoto(for:data:)")
    mockService.verify(function: "get(route:)", with: "photos/" + id)
  }
  
  func testGet_GivenPhotoNotInCache_ThenCallsService_GivenFailure_ThenCallsFailure() {
    let id = "123"
    let mockService = MockService()
    let mockCache = MockCache()
    let testObject = WWPhotoService(service: mockService, cache: mockCache)
    let exp = expectation(description: "Completion called")
    mockService.promise = Promise<Data>(ServiceError.invalidUrl)
    mockCache.loadPromise = Promise<UIImage>(PhotoCacheError.invalidID)

    testObject.get(id: id).then { image in
      XCTFail("Result should be failure")
      }.catch { error in
        if case ServiceError.invalidUrl = error {
          exp.fulfill()
        } else {
          XCTFail("Error should be invalid url")
        }
    }
    
    waitForExpectations(timeout: 0.1, handler: nil)
    
    _ = waitForPromises(timeout: 1)
    mockService.verify(function: "get(route:)", with: "photos/" + id)
  }
  
  func testGet_GivenPhotoInCache_ThenLoadsFromCache() {
    let id = "123"
    let data = UIImage()
    let mockService = MockService()
    let mockCache = MockCache()
    let testObject = WWPhotoService(service: mockService, cache: mockCache)
    let exp = expectation(description: "Completion called")
    mockService.promise = Promise<Data>(validImageData())
    mockCache.loadPromise = Promise<UIImage>(data)
    
    testObject.get(id: id).then { actualData in
      XCTAssertEqual(actualData, data)
      exp.fulfill()
    }.catch { error in
      XCTFail("Result shoud be success")
    }
    
    waitForExpectations(timeout: 0.1, handler: nil)
    
    _ = waitForPromises(timeout: 1)
    XCTAssertEqual(mockService.calls.count, 0)
  }
}

private func validImageData() -> Data {
  return UIImage(named: "test")!.pngData()!
}
