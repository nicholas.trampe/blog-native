//
//  PlaceServiceTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 9/10/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import Promises
@testable import blog

class PlaceServiceTests: XCTestCase {
  func test_Get_GivenServiceCallSucceeds_ThenCachesDataAndCompletes() {
    let places = [Place(_id: "123", name: "names", description: "stuffs", latitude: 123, longitude: 456, date: "dater", photos: [])]
    let encoder = JSONEncoder()
    encoder.dateEncodingStrategy = .formatted(.jsonDateFormatter)
    let data = try! encoder.encode(places)
    let mockService = MockService()
    let mockCache = MockDataCache()
    let testObject = WWPlaceService(service: mockService, cache: mockCache)
    let exp = expectation(description: "Service called")
    
    mockService.promise = Promise<Data>(data)
    mockCache.savePromise = Promise<Data>(data)
    
    testObject.get().then { actualPlaces in
      XCTAssertEqual(actualPlaces, places)
      exp.fulfill()
    }.catch { error in
      XCTFail("Should be success")
    }
    
    waitForExpectations(timeout: 1.0, handler: nil)
    
    XCTAssert(waitForPromises(timeout: 1))
    mockService.verify(function: "get(route:)", with: "places")
    mockCache.verify(function: "saveData(data:)", with: data)
  }
  
  func test_Get_GivenServiceCallFails_AndCacheHasData_ThenCompletesSuccessfully() {
    let places = [Place(_id: "123", name: "names", description: "stuffs", latitude: 123, longitude: 456, date: "dater", photos: [])]
    let encoder = JSONEncoder()
    encoder.dateEncodingStrategy = .formatted(.jsonDateFormatter)
    let data = try! encoder.encode(places)
    let mockService = MockService()
    let mockCache = MockDataCache()
    let testObject = WWPlaceService(service: mockService, cache: mockCache)
    
    mockService.promise = Promise<Data>(ServiceError.invalidData)
    mockCache.loadPromise = Promise<Data>(data)
    
    testObject.get().then { actualPlaces in
      XCTAssertEqual(actualPlaces, places)
    }.catch { error in
      XCTFail("Should be success")
    }
    
    XCTAssert(waitForPromises(timeout: 1))
  }
  
  func test_Get_GivenServiceCallFails_AndCacheHasNoData_ThenCompletesFailurely() {
    let error = ServiceError.invalidData
    let mockService = MockService()
    let mockCache = MockDataCache()
    let testObject = WWPlaceService(service: mockService, cache: mockCache)
    let exp = expectation(description: "Service called")
    
    mockService.promise = Promise<Data>(error)
    mockCache.loadPromise = Promise<Data>(DataCacheError.invalidData)
    
    testObject.get().then { actualData in
      XCTFail("Should be failure")
    }.catch { actualError in
      if case DataCacheError.invalidData = actualError {
        exp.fulfill()
      }
    }
    
    waitForExpectations(timeout: 1.0, handler: nil)
  }
}

