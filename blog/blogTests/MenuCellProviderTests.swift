//
//  MenuCellProviderTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class MenuCellProviderTests: XCTestCase {
  func testNumberOfRows() {
    let table = UITableView()
    let items = [MenuItem(title: "1", icon: .info),
                 MenuItem(title: "2", icon: .info),
                 MenuItem(title: "3", icon: .info),
                 MenuItem(title: "4", icon: .info)]
    let testObject = WWMenuCellProvider()
    testObject.items = items
    
    XCTAssertEqual(testObject.tableView(table, numberOfRowsInSection: 0), items.count)
  }
  
  func testCellForRow() {
    let table = UITableView()
    let path = IndexPath(row: 2, section: 0)
    let items = [MenuItem(title: "1", icon: .info),
                 MenuItem(title: "2", icon: .info),
                 MenuItem(title: "3", icon: .info),
                 MenuItem(title: "4", icon: .info)]
    let testObject = WWMenuCellProvider()
    testObject.items = items
    testObject.registerCells(for: table)
    table.dataSource = testObject
    
    if let cell = testObject.tableView(table, cellForRowAt: path) as? MenuCell {
      XCTAssertEqual(cell.titleLabel.text, items[path.row].title)
      XCTAssertEqual(cell.iconLabel.icon, items[path.row].icon)
    } else {
      XCTFail("Cell not of correct type")
    }
  }
}
