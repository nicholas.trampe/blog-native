//
//  MainPresenterTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class MainPresenterTests: XCTestCase {
  
  class MockView: MainView, Mock {
    var calls: [Call] = []
    var observer: MainViewObserver? = nil
    var indexPathAtLocation: IndexPath? = nil
    var cellFrameAtLocation: CGRect? = nil
    
    func setCellProvider(_ cellProvider: CellProvider) {
      calls.append(Call(name: #function, arguments: [cellProvider]))
    }
    
    func updateData() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func setIcon(_ icon: IconFontIcon) {
      calls.append(Call(name: #function, arguments:[icon]))
    }
    
    func setTitle(with text: String) {
      calls.append(Call(name: #function, arguments:[text]))
    }
    
    func setMessage(with text: String) {
      calls.append(Call(name: #function, arguments:[text]))
    }
    
    func setErrorHidden(hidden: Bool) {
      calls.append(Call(name: #function, arguments:[hidden]))
    }
    
    func showLogo(after delay: TimeInterval) {
      calls.append(Call(name: #function, arguments: [delay]))
    }
    
    func hideLogo(after delay: TimeInterval) {
      calls.append(Call(name: #function, arguments: [delay]))
    }
    
    func indexPath(at location: CGPoint) -> IndexPath? {
      calls.append(Call(name: #function, arguments: [location]))
      return indexPathAtLocation
    }
    
    func cellFrame(at location: CGPoint) -> CGRect? {
      calls.append(Call(name: #function, arguments: [location]))
      return cellFrameAtLocation
    }
  }
  
  class MockSuccessModel: MainModel, Mock {
    var observer: MainModelObserver?
    var calls: [Call] = []
    var posts: [Post] = []
    var lastSyncDate: Date? = nil
    var syncExpirationInterval: TimeInterval = 1
    
    init(posts: [Post]) {
      self.posts = posts
    }
    
    func updatePosts() {
      calls.append(Call(name: #function, arguments: []))
      observer?.didUpdatePosts(posts: posts)
    }
  }
  
  class MockFailureModel: MainModel, Mock {
    var observer: MainModelObserver?
    var calls: [Call] = []
    var posts: [Post] = []
    var lastSyncDate: Date? = nil
    var syncExpirationInterval: TimeInterval = 1
    let error: ServiceError
    
    init(error: ServiceError) {
      self.error = error
    }
    
    func updatePosts() {
      calls.append(Call(name: #function, arguments: []))
      observer?.didFailToUpdatePosts(error: error)
    }
  }
  
  class MockAlert: AlertPresenter, Mock {
    var calls: [Call] = []
    
    func present(title: String, message: String?) {
      var args = [title]
      if let message = message {
        args.append(message)
      }
      calls.append(Call(name: #function, arguments: args))
    }
  }
  
  class MockCellProvider: NSObject, PostCellProvider, Mock {
    var viewModels: [PostCellViewModel] = []
    var calls: [Call] = []
    
    func registerCells(for tableView: UITableView) {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      calls.append(Call(name: #function, arguments: [section]))
      return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      calls.append(Call(name: #function, arguments: [indexPath]))
      return UITableViewCell()
    }
  }
  
  struct MockCellViewModelTransformer: PostCellViewModelTransformer {
    var viewModels: [PostCellViewModel] = []
    
    func viewModels(for posts: [Post]) -> [PostCellViewModel] {
      return viewModels
    }
  }
  
  class MockObserver: MainPresenterObserver, Mock {
    var calls: [Call] = []
    
    func didSelect(_ post: Post, _ frame: CGRect) {
      calls.append(Call(name: #function, arguments: [post, frame]))
    }
    
    func didPressMenuButton() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func didPressPlacesButton() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func didShowPosts() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  func testStart_UpdatesPosts() {
    let mockView = MockView()
    let mockModel = MockSuccessModel(posts: [])
    let mockCellProvider = MockCellProvider()
    let mockViewModelTransformer = MockCellViewModelTransformer(viewModels: [])
    let testObject = WWMainPresenter(view: mockView,
                                     model: mockModel,
                                     cellProvider: mockCellProvider,
                                     cellViewModelTransformer: mockViewModelTransformer)
    
    testObject.start()
    
    mockModel.verify(function: "updatePosts()")
    mockView.verify(function: "showLogo(after:)", with: 0.0)
  }
  
  func testStartSuccess() {
    let posts = [Post(_id: "123", heading: "hello", subheading: "world", cover: "!!", date: Date(), body: [])]
    let viewModels = [WWPostCellViewModel(post: posts[0])]
    let mockView = MockView()
    let mockModel = MockSuccessModel(posts: posts)
    let mockCellProvider = MockCellProvider()
    let mockViewModelTransformer = MockCellViewModelTransformer(viewModels: viewModels)
    let testObject = WWMainPresenter(view: mockView,
                                     model: mockModel,
                                     cellProvider: mockCellProvider,
                                     cellViewModelTransformer: mockViewModelTransformer)
    
    mockView.verify(function: "setCellProvider")
    XCTAssertNil(mockModel.lastSyncDate)
    
    testObject.start()
    
    mockView.verify(function: "updateData()")
    mockView.verify(function: "hideLogo(after:)", with: 2.0)
    XCTAssertNotNil(mockModel.lastSyncDate)
    XCTAssertTrue(mockModel.lastSyncDate!.timeIntervalSinceNow > -1)
    XCTAssertTrue(mockModel.lastSyncDate!.timeIntervalSinceNow < 0)
    XCTAssertEqual(mockCellProvider.viewModels.count, viewModels.count)
  }
  
  func testPostAtLocation_GivenValidLocationAndIndexPath_ReturnsCorrespondingPost() {
    let posts = [
      Post(_id: "123", heading: "hello", subheading: "world", cover: "!!", date: Date(), body: []),
      Post(_id: "456", heading: "hello", subheading: "world", cover: "!!", date: Date(), body: []),
      Post(_id: "789", heading: "hello", subheading: "world", cover: "!!", date: Date(), body: []),
      Post(_id: "101", heading: "hello", subheading: "world", cover: "!!", date: Date(), body: []),
      Post(_id: "112", heading: "hello", subheading: "world", cover: "!!", date: Date(), body: [])
    ]
    let mockView = MockView()
    let mockModel = MockSuccessModel(posts: posts)
    let mockCellProvider = MockCellProvider()
    let mockViewModelTransformer = MockCellViewModelTransformer()
    let testObject = WWMainPresenter(view: mockView,
                                     model: mockModel,
                                     cellProvider: mockCellProvider,
                                     cellViewModelTransformer: mockViewModelTransformer)
    mockView.indexPathAtLocation = IndexPath(row: 3, section: 0)
    
    XCTAssertEqual(testObject.post(at: .zero), posts[3])
  }
  
  func testPostAtLocation_GivenInvalidLocationAndIndexPath_ReturnsNil() {
    let mockView = MockView()
    let mockModel = MockSuccessModel(posts: [])
    let mockCellProvider = MockCellProvider()
    let mockViewModelTransformer = MockCellViewModelTransformer()
    let testObject = WWMainPresenter(view: mockView,
                                     model: mockModel,
                                     cellProvider: mockCellProvider,
                                     cellViewModelTransformer: mockViewModelTransformer)
    mockView.indexPathAtLocation = nil
    
    XCTAssertEqual(testObject.post(at: .zero), nil)
  }
  
  func testPostAtLocation_GivenNegativeIndex_ReturnsNil() {
    let mockView = MockView()
    let mockModel = MockSuccessModel(posts: [])
    let mockCellProvider = MockCellProvider()
    let mockViewModelTransformer = MockCellViewModelTransformer()
    let testObject = WWMainPresenter(view: mockView,
                                     model: mockModel,
                                     cellProvider: mockCellProvider,
                                     cellViewModelTransformer: mockViewModelTransformer)
    mockView.indexPathAtLocation = IndexPath(row: -1, section: 0)
    
    XCTAssertEqual(testObject.post(at: .zero), nil)
  }
  
  func testPostAtLocation_GivenLargerIndex_ReturnsNil() {
    let posts = [
      Post(_id: "123", heading: "hello", subheading: "world", cover: "!!", date: Date(), body: []),
      Post(_id: "456", heading: "hello", subheading: "world", cover: "!!", date: Date(), body: []),
      Post(_id: "789", heading: "hello", subheading: "world", cover: "!!", date: Date(), body: []),
      Post(_id: "101", heading: "hello", subheading: "world", cover: "!!", date: Date(), body: []),
      Post(_id: "112", heading: "hello", subheading: "world", cover: "!!", date: Date(), body: [])
    ]
    let mockView = MockView()
    let mockModel = MockSuccessModel(posts: posts)
    let mockCellProvider = MockCellProvider()
    let mockViewModelTransformer = MockCellViewModelTransformer()
    let testObject = WWMainPresenter(view: mockView,
                                     model: mockModel,
                                     cellProvider: mockCellProvider,
                                     cellViewModelTransformer: mockViewModelTransformer)
    mockView.indexPathAtLocation = IndexPath(row: 5, section: 0)
    
    XCTAssertEqual(testObject.post(at: .zero), nil)
  }
  
  func testFrameAtLocation_ReturnsViewFrame() {
    let mockView = MockView()
    let mockModel = MockSuccessModel(posts: [])
    let mockCellProvider = MockCellProvider()
    let mockViewModelTransformer = MockCellViewModelTransformer()
    let testPoint = CGPoint(x: 1, y: 2)
    let testRect = CGRect(x: 6, y: 7, width: 8, height: 9)
    let testObject = WWMainPresenter(view: mockView,
                                     model: mockModel,
                                     cellProvider: mockCellProvider,
                                     cellViewModelTransformer: mockViewModelTransformer)
    mockView.cellFrameAtLocation = testRect
    
    XCTAssertEqual(testObject.frame(at: testPoint), testRect)
    
    mockView.verify(function: "cellFrame(at:)", with: testPoint)
  }
  
  func testSelection() {
    class MockTable: UITableView {
      let rect: CGRect
      
      init(rect: CGRect) {
        self.rect = rect
        super.init(frame: .zero, style: .plain)
      }
      
      required init?(coder aDecoder: NSCoder) {fatalError("init(coder:) has not been implemented")}
      
      override func rectForRow(at indexPath: IndexPath) -> CGRect {
        return rect
      }
    }
    
    let rect = CGRect(x: 1, y: 1, width: 1, height: 1)
    let posts = [Post(_id: "123", heading: "hello", subheading: "world", cover: "!!", date: Date(), body: [])]
    let mockView = MockView()
    let mockModel = MockSuccessModel(posts: posts)
    let mockCellProvider = MockCellProvider()
    let table = MockTable(rect: rect)
    let mockObserver = MockObserver()
    let mockViewModelTransformer = MockCellViewModelTransformer()
    let testObject = WWMainPresenter(view: mockView,
                                     model: mockModel,
                                     cellProvider: mockCellProvider,
                                     cellViewModelTransformer: mockViewModelTransformer)
    testObject.observer = mockObserver
    
    mockView.observer?.didSelect(table, IndexPath(row: 0, section: 0))
    
    mockObserver.verify(function: "didSelect", with: posts[0], at: 0)
    mockObserver.verify(function: "didSelect", with: rect, at: 1)
  }
  
  func testMenuButton() {
    let mockView = MockView()
    let mockModel = MockSuccessModel(posts: [])
    let mockCellProvider = MockCellProvider()
    let mockObserver = MockObserver()
    let mockViewModelTransformer = MockCellViewModelTransformer()
    let testObject = WWMainPresenter(view: mockView,
                                     model: mockModel,
                                     cellProvider: mockCellProvider,
                                     cellViewModelTransformer: mockViewModelTransformer)
    testObject.observer = mockObserver
    
    mockView.observer?.didPressMenuButton()
    
    mockObserver.verify(function: "didPressMenuButton()")
  }
  
  func testPlacesButton() {
    let mockView = MockView()
    let mockModel = MockSuccessModel(posts: [])
    let mockCellProvider = MockCellProvider()
    let mockObserver = MockObserver()
    let mockViewModelTransformer = MockCellViewModelTransformer()
    let testObject = WWMainPresenter(view: mockView,
                                     model: mockModel,
                                     cellProvider: mockCellProvider,
                                     cellViewModelTransformer: mockViewModelTransformer)
    testObject.observer = mockObserver
    
    mockView.observer?.didPressPlacesButton()
    
    mockObserver.verify(function: "didPressPlacesButton()")
  }
  
  func testFinishHiding() {
    let mockView = MockView()
    let mockModel = MockSuccessModel(posts: [])
    let mockCellProvider = MockCellProvider()
    let mockObserver = MockObserver()
    let mockViewModelTransformer = MockCellViewModelTransformer()
    let testObject = WWMainPresenter(view: mockView,
                                     model: mockModel,
                                     cellProvider: mockCellProvider,
                                     cellViewModelTransformer: mockViewModelTransformer)
    testObject.observer = mockObserver
    
    mockView.observer?.didFinishHidingLogo()
    
    mockObserver.verify(function: "didShowPosts()")
  }
  
  func testUpdatePostsOnEnterForeground() {
    let mockView = MockView()
    let mockModel = MockSuccessModel(posts: [])
    let mockCellProvider = MockCellProvider()
    let mockViewModelTransformer = MockCellViewModelTransformer()
    _ = WWMainPresenter(view: mockView,
                        model: mockModel,
                        cellProvider: mockCellProvider,
                        cellViewModelTransformer: mockViewModelTransformer)
    
    NotificationCenter.default.post(name: UIApplication.willEnterForegroundNotification, object: nil)
    
    mockModel.verify(function: "updatePosts()")
  }
  
  func testStartUrlFailure() {
    verifyStartFailure(error: ServiceError.invalidUrl, message: ServiceError.invalidUrl.errorDescription!)
  }
  
  func testStartDataFailure() {
    verifyStartFailure(error: ServiceError.invalidData, message: ServiceError.invalidData.errorDescription!)
  }
  
  func testStartSessionFailure() {
    verifyStartFailure(error: ServiceError.sessionError(reason: "hello world"), message: "hello world")
  }
  
  private func verifyStartFailure(error: ServiceError, message: String) {
    let mockView = MockView()
    let mockModel = MockFailureModel(error: error)
    let mockCellProvider = MockCellProvider()
    let mockViewModelTransformer = MockCellViewModelTransformer()
    let testObject = WWMainPresenter(view: mockView,
                                     model: mockModel,
                                     cellProvider: mockCellProvider,
                                     cellViewModelTransformer: mockViewModelTransformer)
    
    testObject.start()
    
    mockModel.verify(function: "updatePosts()")
    mockView.verify(function: "updateData()")
    mockView.verify(function: "hideLogo(after:)", with: 0.0)
    XCTAssertEqual(mockCellProvider.viewModels.count, 0)
    mockView.verify(function: "setIcon", with: IconFontIcon.compass)
    mockView.verify(function: "setTitle(with:)", with: "Could Not Update Posts")
    mockView.verify(function: "setMessage(with:)", with: message)
    mockView.verify(function: "setErrorHidden(hidden:)", with: false)
  }

}
