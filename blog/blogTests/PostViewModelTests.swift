//
//  PostViewModelTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/15/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class PostViewModelTests: XCTestCase {
  func testHeadingText() {
    let heading = "heading"
    let post = Post(_id: "", heading: heading, subheading: "", cover: "", date: Date(), body: [])
    let testObject = WWPostViewModel(post: post)
    
    XCTAssertEqual(testObject.headingText, heading)
  }
  
  func testSubheadingText() {
    let subheading = "heading"
    let post = Post(_id: "", heading: "", subheading: subheading, cover: "", date: Date(), body: [])
    let testObject = WWPostViewModel(post: post)
    
    XCTAssertEqual(testObject.subheadingText, subheading)
  }
  
  func testCover() {
    let cover = "cover"
    let post = Post(_id: "", heading: "", subheading: "", cover: cover, date: Date(), body: [])
    let testObject = WWPostViewModel(post: post)
    
    XCTAssertEqual(testObject.cover, cover)
  }
  
  func testComponents() {
    let components = [
      Post.BodyComponent(_id: "123", type: "text", content: "I am some text"),
      Post.BodyComponent(_id: "456", type: "heading", content: "I am a heading"),
      Post.BodyComponent(_id: "789", type: "image", content: "I am an image")
    ]
    let post = Post(_id: "", heading: "", subheading: "", cover: "", date: Date(), body: components)
    let testObject = WWPostViewModel(post: post)
    
    XCTAssertEqual(testObject.components.count, 3)
    XCTAssertEqual(testObject.components[0].type, components[0].type)
    XCTAssertEqual(testObject.components[0].content, components[0].content)
    XCTAssertEqual(testObject.components[1].type, components[1].type)
    XCTAssertEqual(testObject.components[1].content, components[1].content)
    XCTAssertEqual(testObject.components[2].type, components[2].type)
    XCTAssertEqual(testObject.components[2].content, components[2].content)
  }
}
