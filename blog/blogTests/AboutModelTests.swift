//
//  AboutModelTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/25/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class AboutModelTests: XCTestCase {
  func testInitWithTexts() {
    let texts = ["2","3","1"]
    let testObject = WWAboutModel(texts: texts)
    
    XCTAssertEqual(testObject.texts, texts)
  }
  
  func testNext() {
    let texts = ["1","2","3","4","5"]
    let testObject = WWAboutModel(texts: texts)
    
    XCTAssertEqual(testObject.next, "2")
    XCTAssertEqual(testObject.next, "3")
    XCTAssertEqual(testObject.next, "4")
    XCTAssertEqual(testObject.next, "5")
    XCTAssertNil(testObject.next)
  }
  
  func testPrevious() {
    let texts = ["1","2","3","4","5"]
    let testObject = WWAboutModel(texts: texts)
    
    while testObject.next != nil {}
    
    XCTAssertEqual(testObject.previous, "4")
    XCTAssertEqual(testObject.previous, "3")
    XCTAssertEqual(testObject.previous, "2")
    XCTAssertEqual(testObject.previous, "1")
    XCTAssertNil(testObject.previous)
  }
}

