//
//  PushNotificationModelTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 7/7/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import Promises
@testable import blog

class PushNotificationModelTests: XCTestCase {
  let validData = Data(bytes: [88,187,248,112,15,80,230])
  let validDataString = "58BBF8700F50E6"
  
  class MockDeviceService: DeviceService, Mock {
    var calls: [Call] = []
    var promise = Promise<Data>(Data()) 
    
    func register(token: String) -> Promise<Data> {
      calls.append(Call(name: #function, arguments: [token]))
      return promise
    }
  }
  
  class MockObserver: PushNotificationModelObserver, Mock {
    var calls: [Call] = []
    
    func didRegisterDevice() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func didFailToRegisterDevice(error: Error) {
      calls.append(Call(name: #function, arguments: [error]))
    }
    
    func didReceiveNotification(info: [String : Any]) {
      calls.append(Call(name: #function, arguments: [info]))
    }
  }
  
  func testDidRegisterNotification_GivenValidUserInfo_ThenCallsDeviceService() {
    let mockService = MockDeviceService()
    let testObject = WWPushNotificationModel(deviceService: mockService)
    
    NotificationCenter.default.post(name: .didRegisterForRemoteNotificationsWithDeviceToken, object: nil, userInfo: ["deviceToken":validData])
    
    mockService.verify(function: "register(token:)", with: validDataString)
  }
  
  func testDidRegisterNotification_GivenValidUserInfo_AndDeviceServiceSucceeds_ThenNotifiesObserver() {
    let mockService = MockDeviceService()
    let mockObserver = MockObserver()
    let testObject = WWPushNotificationModel(deviceService: mockService)
    
    testObject.observer = mockObserver
    mockService.promise = Promise<Data>(Data())
    
    NotificationCenter.default.post(name: .didRegisterForRemoteNotificationsWithDeviceToken, object: nil, userInfo: ["deviceToken":validData])
    
    XCTAssert(waitForPromises(timeout: 1))
    mockObserver.verify(function: "didRegisterDevice()")
  }
  
  func testDidRegisterNotification_GivenValidUserInfo_AndDeviceServiceFails_ThenNotifiesObserver() {
    let error = ServiceError.invalidData
    let mockService = MockDeviceService()
    let mockObserver = MockObserver()
    let testObject = WWPushNotificationModel(deviceService: mockService)
    
    testObject.observer = mockObserver
    mockService.promise = Promise<Data>(error)
    
    NotificationCenter.default.post(name: .didRegisterForRemoteNotificationsWithDeviceToken, object: nil, userInfo: ["deviceToken":validData])
    
    XCTAssert(waitForPromises(timeout: 1))
    mockObserver.verify(function: "didFailToRegisterDevice(error:)", with: error)
  }
  
  func testDidRegisterNotification_GivenInalidUserInfo_ThenDoesNotCallDeviceService() {
    let mockService = MockDeviceService()
    let testObject = WWPushNotificationModel(deviceService: mockService)
    
    NotificationCenter.default.post(name: .didRegisterForRemoteNotificationsWithDeviceToken, object: nil, userInfo: nil)
    NotificationCenter.default.post(name: .didRegisterForRemoteNotificationsWithDeviceToken, object: nil, userInfo: ["deviceToken":123])
    
    XCTAssert(waitForPromises(timeout: 1))
    XCTAssertEqual(mockService.calls.count, 0)
  }
  
  func testDidFailToRegister_GivenValidUserInfo_ThenNotifiesObserver() {
    let error = ServiceError.invalidData
    let mockObserver = MockObserver()
    let mockService = MockDeviceService()
    let testObject = WWPushNotificationModel(deviceService: mockService)

    testObject.observer = mockObserver
    
    NotificationCenter.default.post(name: .didFailToRegisterForRemoteNotificationsWithError, object: nil, userInfo: ["error":error])
    
    mockObserver.verify(function: "didFailToRegisterDevice(error:)", with: error)
  }
  
  func testDidFailToRegister_GivenInvalidUserInfo_ThenDoesNotNotifieObserver() {
    let mockObserver = MockObserver()
    let mockService = MockDeviceService()
    let testObject = WWPushNotificationModel(deviceService: mockService)
    
    testObject.observer = mockObserver
    
    NotificationCenter.default.post(name: .didFailToRegisterForRemoteNotificationsWithError, object: nil, userInfo: nil)
    NotificationCenter.default.post(name: .didFailToRegisterForRemoteNotificationsWithError, object: nil, userInfo: ["error":123])
    
    mockObserver.verifyNot(function: "didFailToRegisterDevice(error:)")
  }
  
  func testDidReceiveNotification_GivenValidUserInfo_ThenNotifiesObserver() {
    let userInfo = ["aps":["hello":1,"world":2]]
    let mockObserver = MockObserver()
    let mockService = MockDeviceService()
    let testObject = WWPushNotificationModel(deviceService: mockService)
    
    testObject.observer = mockObserver
    
    NotificationCenter.default.post(name: .didReceiveRemoteNotification, object: nil, userInfo: userInfo)
    
    mockObserver.verify(function: "didReceiveNotification(info:)", with: userInfo["aps"])
  }
  
  func testDidReceiveNotification_GivenInvalidUserInfo_ThenDoesNotNotifieObserver() {
    let mockObserver = MockObserver()
    let mockService = MockDeviceService()
    let testObject = WWPushNotificationModel(deviceService: mockService)
    
    testObject.observer = mockObserver
    
    NotificationCenter.default.post(name: .didReceiveRemoteNotification, object: nil, userInfo: nil)
    NotificationCenter.default.post(name: .didFailToRegisterForRemoteNotificationsWithError, object: nil, userInfo: ["aps":123])
    
    mockObserver.verifyNot(function: "didReceiveNotification(info:)")
  }
}

