//
//  SettingsPresenterTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/6/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class SettingsPresenterTests: XCTestCase {
  class MockView: SettingsView, Mock {
    var calls: [Call] = []
    var observer: SettingsViewObserver? = nil
    
  }
  
  class MockModel: SettingsModel, Mock {
    var calls: [Call] = []
    
    func clearCache() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  class MockObserver: SettingsPresenterObserver, Mock {
    var calls: [Call] = []
    
    func didPressCloseButton() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  func test_DidPressCloseButton() {
    let model = MockModel()
    let view = MockView()
    let observer = MockObserver()
    let testObject = WWSettingsPresenter(model: model, view: view)
    testObject.observer = observer
    
    view.observer?.didPressCloseButton()
    
    observer.verify(function: "didPressCloseButton()")
  }
  
  func test_DidPressClearCacheButton() {
    let model = MockModel()
    let view = MockView()
    _ = WWSettingsPresenter(model: model, view: view)
    
    view.observer?.didPressClearCacheButton()
    
    model.verify(function: "clearCache()")
  }
}
