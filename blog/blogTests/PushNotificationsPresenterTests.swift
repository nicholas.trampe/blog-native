//
//  PushNotificationsPresenterTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 7/8/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class PushNotificationsPresenterTests: XCTestCase {
  class MockView: PushNotificationsView, Mock {
    var calls: [Call] = []
    var observer: PushNotificationsViewObserver? = nil
    
    func showSpinner() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func hideSpinner() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func showButtons() {
      calls.append(Call(name: #function, arguments: []))
    }
    
    func hideButtons() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  class MockModel: PushNotificationModel, Mock {
    var calls: [Call] = []
    var observer: PushNotificationModelObserver? = nil
    var isRegistered: Bool = false
    
    func requestAuthorization() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  class MockObserver: PushNotificationsPresenterObserver, Mock {
    var calls: [Call] = []
    
    func didDismiss() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  func testDidRegisterDevice_NotifiesObserver() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockObserver = MockObserver()
    let testObject = WWPushNotificationsPresenter(view: mockView, model: mockModel)
    testObject.observer = mockObserver
    
    mockModel.observer?.didRegisterDevice()
    
    XCTAssertTrue(mockModel.isRegistered)
    mockObserver.verify(function: "didDismiss()")
  }
  
  func testDidFailToRegisterDevice_NotifiesObserver() {
    let error = ServiceError.invalidUrl
    let mockView = MockView()
    let mockModel = MockModel()
    let mockObserver = MockObserver()
    let testObject = WWPushNotificationsPresenter(view: mockView, model: mockModel)
    testObject.observer = mockObserver
    
    mockModel.observer?.didFailToRegisterDevice(error: error)
    
    XCTAssertTrue(mockModel.isRegistered)
    mockObserver.verify(function: "didDismiss()")
  }
  
  func testDidPressYesButton_RegistersDevice() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockObserver = MockObserver()
    let testObject = WWPushNotificationsPresenter(view: mockView, model: mockModel)
    testObject.observer = mockObserver
    
    mockView.observer?.didPressYesButton()
    
    mockView.verify(function: "hideButtons()")
    mockView.verify(function: "showSpinner()")
    mockModel.verify(function: "requestAuthorization()")
  }
  
  func testDidPressNoButton_NotifiesObserver() {
    let mockView = MockView()
    let mockModel = MockModel()
    let mockObserver = MockObserver()
    let testObject = WWPushNotificationsPresenter(view: mockView, model: mockModel)
    testObject.observer = mockObserver
    
    mockView.observer?.didPressNoButton()
    
    XCTAssertTrue(mockModel.isRegistered)
    mockObserver.verify(function: "didDismiss()")
  }
}
