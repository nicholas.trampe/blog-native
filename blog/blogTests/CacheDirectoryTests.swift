//
//  CacheDirectoryTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/6/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import Promises
@testable import blog

class CacheDirectoryTests: XCTestCase {
  private let testObject = WWCacheDirectory()
  
  func test_ClearsCache() {
    let exp = expectation(description: "Data Saved")
    testObject.clear()
    
    XCTAssertEqual(testObject.items.count, 0)
    
    saveDaters().then({ data in
      exp.fulfill()
    })
    
    waitForExpectations(timeout: 1.0, handler: nil)
    
    XCTAssertEqual(testObject.items.count, 10)
    
    testObject.clear()
    
    XCTAssertEqual(testObject.items.count, 0)
  }
  
  func saveDaters() -> Promise<[Data]> {
    var datas: [String:Data] = [:]
    
    for i in 0..<10 {
      let name = "cache \(i)"
      let data = "hello world".data(using: .utf8)!
      datas[name] = data
    }
    
    return all(datas.map { (name, data) -> Promise<Data> in
      let cache = WWDataCache(name: name, directory: self.testObject)
      return cache.saveData(data: data)
    })
  }
}
