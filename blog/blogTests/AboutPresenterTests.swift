//
//  AboutPresenterTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 8/25/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class AboutPresenterTests: XCTestCase {
  class MockModel: AboutModel, Mock {
    var calls: [Call] = []
    var texts: [String] = []
    var next: String? = nil
    var previous: String? = nil
  }
  
  class MockView: AboutView, Mock {
    var calls: [Call] = []
    var observer: AboutViewObserver? = nil
    
    func setText(text: String) {
      calls.append(Call(name: #function, arguments: [text]))
    }
  }
  
  class MockObserver: AboutPresenterObserver, Mock {
    var calls: [Call] = []
    
    func didDismiss() {
      calls.append(Call(name: #function, arguments: []))
    }
  }
  
  func testStart() {
    let mockModel = MockModel()
    let mockView =  MockView()
    let testObject = WWAboutPresenter(view: mockView, model: mockModel)
    mockModel.texts = ["1", "2", "3"]
    
    testObject.start()
    
    mockView.verify(function: "setText(text:)", with: "1")
  }
  
  func testNext_GivenThereIsANext_ThenSetsText() {
    let mockModel = MockModel()
    let mockView =  MockView()
    _ = WWAboutPresenter(view: mockView, model: mockModel)
    mockModel.next = "hello"
    
    mockView.observer?.didPressNextButton()
    
    mockView.verify(function: "setText(text:)", with: "hello")
  }
  
  func testNext_GivenThereIsNotANext_ThenDismisses() {
    let mockModel = MockModel()
    let mockView =  MockView()
    let mockObserver = MockObserver()
    let testObject = WWAboutPresenter(view: mockView, model: mockModel)
    testObject.observer = mockObserver
    mockModel.next = nil
    
    mockView.observer?.didPressNextButton()
    
    mockView.verifyNot(function: "setText(text:)")
    mockObserver.verify(function: "didDismiss()")
  }
  
  func testPrevious_GivenThereIsAPrevious_ThenSetsText() {
    let mockModel = MockModel()
    let mockView =  MockView()
    _ = WWAboutPresenter(view: mockView, model: mockModel)
    mockModel.previous = "hello"
    
    mockView.observer?.didPressPreviousButton()
    
    mockView.verify(function: "setText(text:)", with: "hello")
  }
  
  func testPrevious_GivenThereIsNotAPrevious_ThenDismisses() {
    let mockModel = MockModel()
    let mockView =  MockView()
    let mockObserver = MockObserver()
    let testObject = WWAboutPresenter(view: mockView, model: mockModel)
    testObject.observer = mockObserver
    mockModel.previous = nil
    
    mockView.observer?.didPressPreviousButton()
    
    mockView.verifyNot(function: "setText(text:)")
    mockObserver.verify(function: "didDismiss()")
  }
}


