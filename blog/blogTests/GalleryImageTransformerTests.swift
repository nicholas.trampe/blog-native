//
//  GalleryImageTransformerTests.swift
//  blogTests
//
//  Created by Nicholas Trampe on 7/5/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import blog

class GalleryImageTransformerTests: XCTestCase {
  func test_scaleTransform_GivenAForce_ScalesCorrectly() {
    let force: CGFloat = 0.345324
    let expectedScale: CGFloat = 1.0 + force * 0.15
    let testObject = WWGalleryImageTransformer()
    
    let actualTransform = testObject.scaleTransform(with: force)
    
    XCTAssertEqual(CGAffineTransform(scaleX: expectedScale, y: expectedScale), actualTransform)
  }
  
  func test_popTransform_TransformsCorrectly() {
    let window = UIView(frame: CGRect(x: 0, y: 0, width: 1000, height: 1000))
    let view = UIView(frame: CGRect(x: 100, y: 800, width: 200, height: 200))
    let expectedYTransform: CGFloat = -300 - 900
    let expectedScale: CGFloat = (1000 / 200) * 0.90
    let expectedTransform = CGAffineTransform(a: expectedScale, b: 0.0, c: 0.0, d: expectedScale, tx: 0.0, ty: expectedYTransform)
    let testObject = WWGalleryImageTransformer()
    
    window.addSubview(view)
    
    let actualTransform = testObject.popTransform(toView: window, fromView: view)
    
    XCTAssertEqual(expectedTransform, actualTransform)
  }
  
  func test_slideTransform_GivenAForce_TransformsCorrectly() {
    let distance = CGPoint(x: 100, y: 0)
    let currentTransform = CGAffineTransform(a: 5.0, b: 0.0, c: 5.0, d: 0.0, tx: 0.0, ty: 500)
    let initialTransform = CGAffineTransform.identity
    let expectedScale = sqrt(currentTransform.a*currentTransform.a+currentTransform.c*currentTransform.c)
    let expectedTransform = initialTransform.translatedBy(x: distance.x / expectedScale, y: distance.y / expectedScale)
    let testObject = WWGalleryImageTransformer()
    
    let actualTransform = testObject.slideTransform(distance: distance, currentTransform: currentTransform, initialTransform: initialTransform)
    
    XCTAssertEqual(expectedTransform, actualTransform)
  }
}

