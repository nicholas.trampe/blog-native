//
//  AlertPresenter.swift
//  blog
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol AlertPresenter {
  func present(title: String, message: String?)
}

class WWAlertPresenter: AlertPresenter {
  private let presenter: UIViewController
  
  init(viewController: UIViewController) {
    presenter = viewController
  }
  
  func present(title: String, message: String?) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
    presenter.present(alert, animated: true, completion: nil)
  }
}
