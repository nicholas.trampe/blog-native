//
//  PostPresenter.swift
//  blog
//
//  Created by Nicholas Trampe on 8/14/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PostPresenterObserver: class {
  func didPressCloseButton()
}

protocol PostPresenter {
  var observer: PostPresenterObserver? { get set }
  
  func peek()
  func pop()
}

class WWPostPresenter: PostPresenter {
  weak var observer: PostPresenterObserver? = nil
  private var view: PostView
  private var cellProvider: PostDetailsCellProvider
  
  public init(view: PostView,
              cellProvider: PostDetailsCellProvider) {
    self.view = view
    self.cellProvider = cellProvider
    
    self.view.observer = self
    self.view.setCellProvider(self.cellProvider)
    self.view.setTopOffset(UIApplication.shared.statusBarFrame.size.height)
  }
  
  func peek() {
    view.setCloseButtonHidden(true, animated: false)
    view.setTopOffset(0)
  }
  
  func pop() {
    view.setCloseButtonHidden(false, animated: false)
    self.view.setTopOffset(UIApplication.shared.statusBarFrame.size.height)
  }
}

extension WWPostPresenter: PostViewObserver {
  func didPressCloseButton() {
    view.setScrollPosition(view.headerHeight + 200, animated: false)
    view.setScrollPosition(0, animated: true)
    view.setCloseButtonHidden(true, animated: true)
    observer?.didPressCloseButton()
  }
  
  func didUpdateScroll(with position: CGFloat) {
    let navHeight = UIApplication.shared.statusBarFrame.size.height
    let offset: CGFloat = position
    let threshold: CGFloat = view.headerHeight - (view.closeButtonFrame.origin.y + view.closeButtonFrame.size.height / 2.0) + navHeight
    let variance: CGFloat = view.closeButtonFrame.size.height / 2.0
    let min = threshold - variance
    let max = threshold + variance
    let progress: CGFloat = fmin(fmax((offset - min) / (max - min), 0.0), 1.0)
    let backgroundComponent = 1.0 - progress
    let foregroundComponent = progress
    
    view.setCloseButtonBackgroundColor(UIColor(red: backgroundComponent, green: backgroundComponent, blue: backgroundComponent, alpha: 0.8), for: .normal)
    view.setCloseButtonBackgroundColor(UIColor(red: backgroundComponent, green: backgroundComponent, blue: backgroundComponent, alpha: 0.5), for: .highlighted)
    view.setCloseButtonForegroundColor(UIColor(red: foregroundComponent, green: foregroundComponent, blue: foregroundComponent, alpha: 1.0), for: .normal)
    view.setCloseButtonForegroundColor(UIColor(red: foregroundComponent, green: foregroundComponent, blue: foregroundComponent, alpha: 0.6), for: .highlighted)

  }
}
