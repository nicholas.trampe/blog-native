//
//  GalleryImageTransformer.swift
//  blog
//
//  Created by Nicholas Trampe on 7/5/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol GalleryImageTransformer {
  func scaleTransform(with force: CGFloat) -> CGAffineTransform
  func popTransform(toView: UIView, fromView: UIView) -> CGAffineTransform
  func slideTransform(distance: CGPoint, currentTransform: CGAffineTransform, initialTransform: CGAffineTransform) -> CGAffineTransform
}


class WWGalleryImageTransformer: GalleryImageTransformer {
  func scaleTransform(with force: CGFloat) -> CGAffineTransform {
    return CGAffineTransform(scaleX: 1.0 + force * 0.15, y: 1.0 + force * 0.15)
  }
  
  func popTransform(toView: UIView, fromView: UIView) -> CGAffineTransform {
    let convertedAbsoluteCenter = toView.convert(toView.center, to: fromView)
    let scale = (toView.frame.size.height / fromView.frame.size.height) * 0.90
    var t = fromView.transform
    t = t.translatedBy(x: 0, y: convertedAbsoluteCenter.y - fromView.center.y)
    t = t.scaledBy(x: scale, y: scale)
    
    return t
  }
  
  func slideTransform(distance: CGPoint, currentTransform: CGAffineTransform, initialTransform: CGAffineTransform) -> CGAffineTransform {
    let scale = sqrt(currentTransform.a*currentTransform.a+currentTransform.c*currentTransform.c)
    return initialTransform.translatedBy(x: distance.x / scale, y: distance.y / scale)
  }
}
