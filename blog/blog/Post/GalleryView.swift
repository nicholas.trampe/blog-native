//
//  GalleryView.swift
//  blog
//
//  Created by Nicholas Trampe on 6/21/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol GalleryView {
  var observer: GalleryViewObserver? { get set }
  var frame: CGRect { get }
  var center: CGPoint { get }
  var visibleImage: UIImageView? { get }
  
  func set(image: UIImage, at index: Int)
  func removeAll()
  func scrollTo(page: UInt)
  func setPageControlCurrent(page: Int)
  func setPageControlTotal(page: Int)
  func setPageControlHidden(hidden: Bool)
  func setSpinnerHidden(hidden: Bool)
  func updateVisibleImageTransform(_ transform: CGAffineTransform, animated: Bool)
  func bringVisibleImageToFront()
}

protocol GalleryViewObserver: class {
  func didUpdateScroll(to offset: CGFloat)
  func willBeginDragging()
  func didEndDragging()
  func didForceTouchImage(with force: CGFloat, traveled distance: CGPoint, at state: UIGestureRecognizer.State)
}

class WWGalleryView: UIView, GalleryView {
  weak var observer: GalleryViewObserver? = nil
  var visibleImage: UIImageView? {
    get {
      let container = CGRect(x: scrollView.contentOffset.x, y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height)
      return imageViews.first { imageView -> Bool in
        var frame = imageView.frame
        frame.origin.y = container.origin.y
        return frame.intersects(container)
      }
    }
  }
  
  private let scrollView = UIScrollView(frame: .zero)
  private let spinner = UIActivityIndicatorView(frame: .zero)
  private let pageControl = UIPageControl(frame: .zero)
  private var imageViews: [UIImageView] = []
  
  init() {
    super.init(frame: .zero)
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // Public API
  
  func removeAll() {
    imageViews.forEach { $0.removeFromSuperview() }
    imageViews.removeAll()
    scrollView.contentSize = .zero
  }
  
  func set(image: UIImage, at index: Int) {
    let imageView = UIImageView(image: image)
    let offset: CGFloat = 0
    let size: CGFloat = self.frame.size.width - offset * 2
    
    imageView.contentMode = .scaleAspectFill
    imageView.backgroundColor = .clear
    imageView.clipsToBounds = true
    imageView.frame = CGRect(x: CGFloat(index) * frame.size.width + offset, y: 0, width: size, height: size)
    scrollView.addSubview(imageView)
    imageViews.append(imageView)
    
    scrollView.contentSize = CGSize(width: max(scrollView.contentSize.width, CGFloat(index + 1) * frame.size.width), height: frame.size.height)
  }
  
  func scrollTo(page: UInt) {
    UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: [], animations: { 
      self.scrollView.contentOffset = CGPoint(x: CGFloat(page) * self.frame.size.width, y: 0)
    }, completion: { finished in
      self.observer?.didUpdateScroll(to: self.scrollView.contentOffset.x)
    })
  }
  
  func setPageControlCurrent(page: Int) {
    pageControl.currentPage = page
  }
  
  func setPageControlTotal(page: Int) {
    pageControl.numberOfPages = page
  }
  
  func setPageControlHidden(hidden: Bool) {
    pageControl.isHidden = hidden
  }
  
  func setSpinnerHidden(hidden: Bool) {
    if hidden {
      spinner.stopAnimating()
    } else {
      spinner.startAnimating()
    }
  }
  
  func updateVisibleImageTransform(_ transform: CGAffineTransform, animated: Bool) {
    guard let visibleImage = visibleImage else {
      return
    }
    
    UIView.animate(withDuration: animated ? 0.3 : 0.0,
                   delay: 0.0,
                   usingSpringWithDamping: 1.0,
                   initialSpringVelocity: 1.0,
                   options: [],
                   animations: { 
                    visibleImage.transform = transform
    }, completion: nil)
  }
  
  func bringVisibleImageToFront() {
    guard let visibleImage = visibleImage else {
      return
    }
    
    scrollView.bringSubviewToFront(visibleImage)
  }
  
  // Private API
  
  private func configureViews() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.black.withAlphaComponent(0.05)
    
    spinner.translatesAutoresizingMaskIntoConstraints = false
    spinner.style = .whiteLarge
    spinner.color = .black
    spinner.hidesWhenStopped = true
    addSubview(spinner)
    
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    scrollView.backgroundColor = .clear
    scrollView.isPagingEnabled = true
    scrollView.showsHorizontalScrollIndicator = false
    scrollView.clipsToBounds = false
    scrollView.delegate = self
    addSubview(scrollView)
    
    pageControl.translatesAutoresizingMaskIntoConstraints = false
    pageControl.currentPageIndicatorTintColor = .white
    pageControl.pageIndicatorTintColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.5)
    pageControl.hidesForSinglePage = true
    addSubview(pageControl)
    
    let tap = ForceTouchGestureRecognizer(target: self, action: #selector(handleTouch(_:)))
    tap.threshold = 0.1
    tap.cancelsTouchesInView = false
    addGestureRecognizer(tap)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      spinner.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      spinner.centerYAnchor.constraint(equalTo: self.centerYAnchor),
      
      scrollView.leftAnchor.constraint(equalTo: self.leftAnchor),
      scrollView.rightAnchor.constraint(equalTo: self.rightAnchor),
      scrollView.topAnchor.constraint(equalTo: self.topAnchor),
      scrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
      
      pageControl.bottomAnchor.constraint(equalTo: self.bottomAnchor),
      pageControl.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      ])
  }
  
  @objc func handleTouch(_ sender: ForceTouchGestureRecognizer) {
    observer?.didForceTouchImage(with: sender.force, traveled: sender.distance, at: sender.state)
  }
}

extension WWGalleryView: UIScrollViewDelegate {
  func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
    observer?.didUpdateScroll(to: scrollView.contentOffset.x)
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    observer?.didUpdateScroll(to: scrollView.contentOffset.x)
  }
  
  func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
    observer?.willBeginDragging()
  }
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    observer?.didEndDragging()
  }
}

