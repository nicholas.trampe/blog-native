//
//  BackButton.swift
//  blog
//
//  Created by Nicholas Trampe on 6/24/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

class BackButton: UIControl {
  private var backgroundColorForState: [UIControl.State:UIColor] = [.normal:.black,.selected:.white,.highlighted:.white,.disabled:.lightGray]
  private var foregroundColorForState: [UIControl.State:UIColor] = [.normal:.white,.selected:.black,.highlighted:.black,.disabled:.darkGray]
  
  init() {
    super.init(frame: .zero)
    translatesAutoresizingMaskIntoConstraints = false
    
    backgroundColor = .clear
    accessibilityTraits = UIAccessibilityTraits.button
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override var isSelected: Bool {
    didSet {
      setNeedsDisplay()
    }
  }
  
  override var isEnabled: Bool {
    didSet {
      setNeedsDisplay()
    }
  }
  
  override var isHighlighted: Bool {
    didSet {
      setNeedsDisplay()
    }
  }
  
  func setBackgroundColor(_ color: UIColor?, for state: UIControl.State) {
    backgroundColorForState[state] = color
    setNeedsDisplay()
  }
  
  func setForegroundColor(_ color: UIColor?, for state: UIControl.State) {
    foregroundColorForState[state] = color
    setNeedsDisplay()
  }
  
  override func draw(_ rect: CGRect) {
    guard let background = backgroundColorForState[state],
      let foreground = foregroundColorForState[state] else {return}
    
    //// Circle
    let ovalPath = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 40, height: 40))
    background.setFill()
    ovalPath.fill()
    
    
    //// Arrow
    let bezierPath = UIBezierPath()
    bezierPath.move(to: CGPoint(x: 24.24, y: 27.43))
    bezierPath.addLine(to: CGPoint(x: 16.79, y: 19.98))
    bezierPath.addLine(to: CGPoint(x: 24.04, y: 12.78))
    bezierPath.addCurve(to: CGPoint(x: 24.79, y: 11.29), controlPoint1: CGPoint(x: 24.5, y: 12.44), controlPoint2: CGPoint(x: 24.79, y: 11.9))
    bezierPath.addCurve(to: CGPoint(x: 22.92, y: 9.42), controlPoint1: CGPoint(x: 24.79, y: 10.25), controlPoint2: CGPoint(x: 23.95, y: 9.42))
    bezierPath.addCurve(to: CGPoint(x: 21.81, y: 9.78), controlPoint1: CGPoint(x: 22.5, y: 9.42), controlPoint2: CGPoint(x: 22.12, y: 9.55))
    bezierPath.addCurve(to: CGPoint(x: 21.77, y: 9.79), controlPoint1: CGPoint(x: 21.79, y: 9.78), controlPoint2: CGPoint(x: 21.77, y: 9.79))
    bezierPath.addLine(to: CGPoint(x: 21.61, y: 9.95))
    bezierPath.addCurve(to: CGPoint(x: 21.71, y: 9.99), controlPoint1: CGPoint(x: 21.6, y: 9.96), controlPoint2: CGPoint(x: 21.72, y: 9.97))
    bezierPath.addLine(to: CGPoint(x: 12, y: 19.92))
    bezierPath.addLine(to: CGPoint(x: 12, y: 20.03))
    bezierPath.addLine(to: CGPoint(x: 21.83, y: 30))
    bezierPath.addLine(to: CGPoint(x: 21.84, y: 30))
    bezierPath.addCurve(to: CGPoint(x: 23.07, y: 30.54), controlPoint1: CGPoint(x: 22.16, y: 31), controlPoint2: CGPoint(x: 22.6, y: 30.54))
    bezierPath.addCurve(to: CGPoint(x: 24.87, y: 28.77), controlPoint1: CGPoint(x: 24.06, y: 30.54), controlPoint2: CGPoint(x: 24.87, y: 29.77))
    bezierPath.addCurve(to: CGPoint(x: 24.24, y: 27.43), controlPoint1: CGPoint(x: 24.87, y: 28.22), controlPoint2: CGPoint(x: 24.63, y: 27.76))
    bezierPath.close()
    foreground.setFill()
    bezierPath.fill()
  }
  
  override var intrinsicContentSize: CGSize {
    return CGSize(width: 40, height: 40)
  }
}

extension UIControl.State: Hashable {
  static let all: [UIControl.State] = [.normal, .selected, .disabled, .highlighted]
  public var hashValue: Int {
    return Int(rawValue)
  }
}
