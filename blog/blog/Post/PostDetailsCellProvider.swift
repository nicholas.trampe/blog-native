//
//  PostDetailsCellProvider.swift
//  blog
//
//  Created by Nicholas Trampe on 8/14/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PostDetailsCellProvider: CellProvider {
  var viewModel: PostViewModel { get }
  var photoService: PhotoService { get }
}

class WWPostDetailsCellProvider: NSObject, PostDetailsCellProvider {
  let viewModel: PostViewModel
  let photoService: PhotoService
  
  init(viewModel: PostViewModel, photoService: PhotoService) {
    self.viewModel = viewModel
    self.photoService = photoService
  }
  
  func registerCells(for tableView: UITableView) {
    tableView.register(PostCell.self, forCellReuseIdentifier: PostCell.defaultCellIdentifier)
    tableView.register(TextComponentCell.self, forCellReuseIdentifier: TextComponentCell.defaultCellIdentifier)
    tableView.register(ImageComponentCell.self, forCellReuseIdentifier: ImageComponentCell.defaultCellIdentifier)
    tableView.register(VideoComponentCell.self, forCellReuseIdentifier: VideoComponentCell.defaultCellIdentifier)
    tableView.register(MapComponentCell.self, forCellReuseIdentifier: MapComponentCell.defaultCellIdentifier)
    tableView.register(SpacerComponentCell.self, forCellReuseIdentifier: SpacerComponentCell.defaultCellIdentifier)
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 300
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 3
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return section == 0 ? 1 : viewModel.components.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.section == 0 {
      let cell = tableView.dequeueReusableCell(withIdentifier: PostCell.defaultCellIdentifier) as! PostCell
      
      cell.heading.headingLabel.text = viewModel.headingText
      cell.heading.subheadingLabel.text = viewModel.subheadingText
      
      photoService.get(id: viewModel.cover).then({ image in 
        cell.heading.backgroundImage.image = image
      }).catch({ error in 
        cell.heading.backgroundImage.image = nil
      })
      
      return cell
    } else if indexPath.section == 1 {
      let component = viewModel.components[indexPath.row]
      
      switch component.type {
      case "text", "heading":
        let cell = tableView.dequeueReusableCell(withIdentifier: TextComponentCell.defaultCellIdentifier) as! TextComponentCell
        
        cell.label.text = component.content
        
        if component.type == "text" {
          cell.accessibilityLabel = "Post Details Text Cell"
          cell.label.font = UIFont.openSansLight(of: FontAttributes.body.size)
        } else if component.type == "heading" {
          cell.accessibilityLabel = "Post Details Heading Cell"
          cell.label.font = UIFont.openSansBold(of: FontAttributes.heading1.size)
        }
        
        return cell
      case "image":
        let cell = tableView.dequeueReusableCell(withIdentifier: ImageComponentCell.defaultCellIdentifier) as! ImageComponentCell
        let model = WWGalleryModel(photoService: photoService, photoIdsComponent: component.content)
        
        cell.setModel(model)
        cell.accessibilityLabel = "Post Details Image Cell"
        
        return cell
      case "youtube":
        let cell = tableView.dequeueReusableCell(withIdentifier: VideoComponentCell.defaultCellIdentifier) as! VideoComponentCell
        
        cell.player.load(component.content)
        cell.accessibilityLabel = "Post Details Video Cell"
        
        return cell
        
      case "location":
        let cell = tableView.dequeueReusableCell(withIdentifier: MapComponentCell.defaultCellIdentifier) as! MapComponentCell
        
        let comps: [String] = component.content.components(separatedBy: ",")
        if comps.count == 2 {
          if  let latitude = Double(comps[0]),
              let longitude = Double(comps[1]) {
            let marker = Marker(id: "1", title: nil, description: nil, date: nil, latitude: latitude, longitude: longitude)
            cell.map.addMarker(marker: marker)
            cell.map.moveCamera(latitude: latitude, longitude: longitude, zoom: 6, animated: false)
          }
        }
        
        cell.accessibilityLabel = "Post Details Location Cell"

        return cell
      default:
        return UITableViewCell()
      }
    } else {
      return tableView.dequeueReusableCell(withIdentifier: SpacerComponentCell.defaultCellIdentifier)!
    }
  }
}

class TextComponentCell: UITableViewCell {
  let label = UILabel()
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configureViews() {
    label.translatesAutoresizingMaskIntoConstraints = false
    label.numberOfLines = 0
    label.font = UIFont.openSansLight(of: FontAttributes.body.size)
    label.textColor = .foregroundColor
    contentView.addSubview(label)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      label.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20),
      label.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20),
      label.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
      label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
      ])
  }
}

class ImageComponentCell: UITableViewCell {
  private let gallery = WWGalleryView()
  private var presenter: GalleryPresenter? = nil
  private let haptics = WWHapticGenerator()
  private let imageTransformer = WWGalleryImageTransformer()
    
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    
    presenter?.reset()
    presenter = nil
  }
  
  func setModel(_ model: GalleryModel) {
    presenter = WWGalleryPresenter(view: gallery,
                                   model: model,
                                   hapticGenerator: haptics,
                                   imageTransformer: imageTransformer)
    presenter?.observer = self
    presenter?.start()
  }
  
  private func configureViews() {
    clipsToBounds = false
    
    gallery.translatesAutoresizingMaskIntoConstraints = false
    gallery.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
    gallery.clipsToBounds = true
    contentView.addSubview(gallery)
  }
  
  private func constrainViews() {
    let width = min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height) * (UIDevice.isPad ? 0.5 : 1)
    
    NSLayoutConstraint.activate([
      gallery.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
      gallery.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
      gallery.widthAnchor.constraint(equalToConstant: width),
      gallery.heightAnchor.constraint(equalTo: gallery.widthAnchor),
      gallery.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      ])
  }
}

extension ImageComponentCell: GalleryPresenterObserver {
  func didPeek() {
    layer.zPosition = 10
    gallery.clipsToBounds = false
  }
  
  func didUnPeek() {
    layer.zPosition = 1
    gallery.clipsToBounds = true
  }
}

class VideoComponentCell: UITableViewCell {
  let player = YouTubePlayer()
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configureViews() {
    player.translatesAutoresizingMaskIntoConstraints = false
    player.layer.borderColor = UIColor.oceanBlue.cgColor
    player.layer.borderWidth = 2.0
    contentView.addSubview(player)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      player.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20),
      player.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20),
      player.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
      player.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
      player.heightAnchor.constraint(equalToConstant: 200),
      ])
  }
}

class MapComponentCell: UITableViewCell {
  let map = WWMapView()
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configureViews() {
    map.translatesAutoresizingMaskIntoConstraints = false
    map.layer.borderColor = UIColor.borderPurple.cgColor
    map.layer.borderWidth = 2.0
    map.isGestureInteractionEnabled = false
    contentView.addSubview(map)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      map.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 50),
      map.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -50),
      map.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
      map.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
      map.heightAnchor.constraint(equalTo: map.widthAnchor),
      ])
  }
}

class SpacerComponentCell: UITableViewCell {
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      self.heightAnchor.constraint(equalToConstant: 1),
      ])
  }
}
