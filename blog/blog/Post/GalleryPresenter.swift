//
//  GalleryPresenter.swift
//  blog
//
//  Created by Nicholas Trampe on 7/5/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol GalleryPresenter {
  var observer: GalleryPresenterObserver? { get set }
  func start()
  func reset()
}

protocol GalleryPresenterObserver: class {
  func didPeek()
  func didUnPeek()
}

class WWGalleryPresenter: GalleryPresenter {
  weak var observer: GalleryPresenterObserver? = nil
  var isAutomaticallyScrolling: Bool {
    get {
      if let timer = timer {
        return timer.isValid
      }
      
      return false
    }
  }
  
  private var view: GalleryView
  private var model: GalleryModel
  private var haptics: HapticGenerator
  private var transformer: GalleryImageTransformer
  private var timer: Timer? = nil
  private var isPeeking = false
  private var isPopping = false
  private let threshold: CGFloat = 0.8
  private var initialTransform: CGAffineTransform = .identity
  
  init(view: GalleryView,
       model: GalleryModel,
       hapticGenerator: HapticGenerator,
       imageTransformer: GalleryImageTransformer) {
    self.view = view
    self.model = model
    self.haptics = hapticGenerator
    self.transformer = imageTransformer
    
    self.view.observer = self
    self.model.observer = self
    
    self.view.setPageControlTotal(page: self.model.photoIds.count)
    self.view.setPageControlCurrent(page: 0)
    self.view.setPageControlHidden(hidden: self.model.photoIds.count == 1)
    self.view.setSpinnerHidden(hidden: false)
  }
  
  deinit {
    stopAutoScroll()
  }
  
  // Public API
  
  func start() {
    model.retrieveImages()
    startAutoScroll()
  }
  
  func reset() {
    view.removeAll()
    stopAutoScroll()
  }
  
  // Private API
  
  private func startAutoScroll() {
    weak var weakSelf = self
    timer = .scheduledTimer(withTimeInterval: model.interval, repeats: true, block: { t in
      guard let currentPage = weakSelf?.model.currentPage,
        let numberOfPhotos = weakSelf?.model.photoIds.count else {
        return
      }
      let nextPage = currentPage + 1 < numberOfPhotos ? currentPage + 1 : 0
      weakSelf?.view.scrollTo(page: nextPage)
    })
  }
  
  private func stopAutoScroll() {
    timer?.invalidate()
    timer = nil
  }
}

extension WWGalleryPresenter: GalleryViewObserver {
  func didUpdateScroll(to offset: CGFloat) {
    let numberOfPhotos = model.photoIds.count
    
    if numberOfPhotos > 0 {
      let totalWidth: CGFloat = view.frame.size.width * CGFloat(numberOfPhotos)
      let page: UInt = UInt((offset / totalWidth) * CGFloat(numberOfPhotos))
      model.currentPage = page
      view.setPageControlCurrent(page: Int(page))
    }
  }
  
  func willBeginDragging() {
    stopAutoScroll()
  }
  
  func didEndDragging() {
    startAutoScroll()
  }
  
  func didForceTouchImage(with force: CGFloat, traveled distance: CGPoint, at state: UIGestureRecognizer.State) {
    switch state {
    case .began:
      haptics.prepare()
      stopAutoScroll()
      isPeeking = true
      view.updateVisibleImageTransform(transformer.scaleTransform(with: force), animated: false)
      view.bringVisibleImageToFront()
      
      observer?.didPeek()
    case .changed:
      if !isPeeking {
        observer?.didPeek()
        view.bringVisibleImageToFront()
        isPeeking = true
      }
      
      if isPopping {
        guard let image = view.visibleImage else { return }
        
        let transform = transformer.slideTransform(distance: distance,
                                                   currentTransform: image.transform,
                                                   initialTransform: initialTransform)
        view.updateVisibleImageTransform(transform, animated: false)
      } else {
        if force < threshold { 
          view.updateVisibleImageTransform(transformer.scaleTransform(with: force), animated: false)
        } else {
          guard let window = UIApplication.shared.keyWindow, let image = view.visibleImage else { return }
          
          initialTransform = transformer.popTransform(toView: window, fromView: image)
          view.updateVisibleImageTransform(initialTransform, animated: true)
          view.setPageControlHidden(hidden: true)
          haptics.generate()
          isPopping = true
        }
      }
    case .ended:
      if isPeeking {
        observer?.didUnPeek()
        view.setPageControlHidden(hidden: model.photoIds.count == 1)
        startAutoScroll()
      }
      
      isPeeking = false
      isPopping = false
      
      view.updateVisibleImageTransform(.identity, animated: true)
    default:
      break
    }
  }
}

extension WWGalleryPresenter: GalleryModelObserver {
  func didRetrieve(image: UIImage, at index: Int) {
    view.set(image: image, at: index)
  }
  
  func didFailToRetrieveImage(at index: Int, with error: Error) {
    
  }
}
