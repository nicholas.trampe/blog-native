//
//  GalleryModel.swift
//  blog
//
//  Created by Nicholas Trampe on 7/3/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol GalleryModel {
  var observer: GalleryModelObserver? { get set }
  var photoIds: [String] { get }
  var currentPage: UInt { get set }
  var interval: TimeInterval { get set }
  
  func retrieveImages()
}

protocol GalleryModelObserver: class {
  func didRetrieve(image: UIImage, at index: Int)
  func didFailToRetrieveImage(at index: Int, with error: Error)
}

class WWGalleryModel: GalleryModel {
  weak var observer: GalleryModelObserver? = nil
  public var photoIds: [String] = []
  public var currentPage: UInt = 0
  public var interval: TimeInterval = 5.0
  private var photoService: PhotoService
  
  public init(photoService: PhotoService, photoIdsComponent: String) {
    self.photoService = photoService
    let components = photoIdsComponent.split(separator: ",")
    photoIds = components.map { return String($0) }
  }
  
  func retrieveImages() {
    for (index, id) in photoIds.enumerated() {
      photoService.get(id: String(id)).then({ image in 
        self.observer?.didRetrieve(image: image, at: index)
      }).catch({ error in 
        self.observer?.didFailToRetrieveImage(at: index, with: error)
      })
    }
  }
}
