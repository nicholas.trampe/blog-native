//
//  PostDismissTransitioning.swift
//  blog
//
//  Created by Nicholas Trampe on 8/17/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class PostDismissTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
  var finalFrame: CGRect = .zero
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 1.0
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard
      let fromVC = transitionContext.viewController(forKey: .from),
      let toVC = transitionContext.viewController(forKey: .to)
      else {
        return
    }
    
    let maskDuration = 0.4
    let fadeDuration = 0.15
    let containerView = transitionContext.containerView
    let topOffset: CGFloat = UIApplication.shared.statusBarFrame.size.height
    
    let blackView = UIView(frame: finalFrame)
    blackView.backgroundColor = .black
    containerView.insertSubview(blackView, at: 0)
    
    let originPath = CGMutablePath()
    originPath.addRect(fromVC.view.frame)
    
    let originMask = CAShapeLayer()
    originMask.path = originPath
    originMask.fillRule = CAShapeLayerFillRule.evenOdd
    
    let finalRect = CGRect(x: 0, y: topOffset, width: finalFrame.size.width, height: finalFrame.size.height)
    let finalPath = CGMutablePath()
    finalPath.addRect(finalRect)
    
    toVC.view.frame = containerView.frame
    containerView.addSubview(toVC.view)
    containerView.sendSubviewToBack(toVC.view)

    fromVC.view.layer.mask = originMask
    
    let anim = CABasicAnimation(keyPath: "path")
    anim.fromValue = originPath
    anim.toValue = finalPath
    anim.duration = maskDuration
    anim.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    
    originMask.add(anim, forKey: nil)
    
    CATransaction.begin()
    CATransaction.setDisableActions(true)
    originMask.path = finalPath
    CATransaction.commit()
    
    UIView.animate(withDuration: transitionDuration(using: transitionContext),
                   delay: 0,
                   usingSpringWithDamping: 0.6,
                   initialSpringVelocity: 1.0,
                   options: [],
                   animations: { 
                    fromVC.view.frame = CGRect(x: self.finalFrame.origin.x, y: self.finalFrame.origin.y-topOffset, width: fromVC.view.frame.size.width, height: fromVC.view.frame.size.height)
    }) { completed in
      blackView.removeFromSuperview()
      UIView.animate(withDuration: fadeDuration,
                     delay: 0,
                     options: [.curveEaseInOut],
                     animations: {
                      fromVC.view.alpha = 0
      }) { _ in
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
      }
    }
  }
}
