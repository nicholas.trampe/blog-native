//
//  YouTubePlayer.swift
//  blog
//
//  Created by Nicholas Trampe on 8/15/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit
import WebKit

class YouTubePlayer: WKWebView {
  var id: String? = nil
  
  init() {
    let webConfiguration = WKWebViewConfiguration()
    super.init(frame: .zero, configuration: webConfiguration)
    scrollView.isScrollEnabled = false
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func load(_ videoID: String) {
    id = videoID
    loadHTMLString("<iframe width='100%' height='100%' src='https://www.youtube.com/embed/\(videoID)' frameborder='0' allowfullscreen></iframe>", baseURL: nil)
  }
}
