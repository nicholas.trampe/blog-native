//
//  PostViewModel.swift
//  blog
//
//  Created by Nicholas Trampe on 8/15/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol PostComponentViewModel {
  var type: String { get }
  var content: String { get }
}

protocol PostViewModel {
  var headingText: String { get }
  var subheadingText: String { get }
  var cover: String { get }
  var components: [PostComponentViewModel] { get }
}

struct WWPostComponentViewModel: PostComponentViewModel {
  let type: String
  let content: String
}

struct WWPostViewModel: PostViewModel {
  let headingText: String
  let subheadingText: String
  let cover: String
  let components: [PostComponentViewModel]
  
  init(post: Post) {
    headingText = post.heading
    subheadingText = post.subheading
    cover = post.cover
    
    components = post.body.map({ (comp) -> WWPostComponentViewModel in
      return WWPostComponentViewModel(type: comp.type, content: comp.content)
    })
  }
}
