//
//  PostView.swift
//  blog
//
//  Created by Nicholas Trampe on 8/14/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PostViewObserver: class {
  func didPressCloseButton()
  func didUpdateScroll(with position: CGFloat)
}

protocol PostView {
  var observer: PostViewObserver? { get set }
  var headerHeight: CGFloat { get }
  var closeButtonFrame: CGRect { get }
  
  func setCellProvider(_ cellProvider: CellProvider)
  func updateData()
  
  func setScrollPosition(_ position: CGFloat, animated: Bool)
  func setCloseButtonHidden(_ hidden: Bool, animated: Bool)
  func setCloseButtonBackgroundColor(_ color: UIColor, for state: UIControl.State)
  func setCloseButtonForegroundColor(_ color: UIColor, for state: UIControl.State)
  func setTopOffset(_ offset: CGFloat)
}

class WWPostView: UIView, PostView {
  weak var observer: PostViewObserver? = nil
  private let tableView = UITableView()
  private let closeButton = BackButton()
  private lazy var topConstraint: NSLayoutConstraint = tableView.topAnchor.constraint(equalTo: self.topAnchor)

  var headerHeight: CGFloat {
    get {
      guard let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) else {
        return 0
      }
      
      return cell.frame.size.height
    }
  }
  
  var closeButtonFrame: CGRect {
    get {
      return closeButton.frame
    }
  }
  
  init() {
    super.init(frame: .zero)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // Public API
  
  func setCellProvider(_ cellProvider: CellProvider) {
    cellProvider.registerCells(for: tableView)
    tableView.dataSource = cellProvider
  }
  
  public func updateData() {
    tableView.reloadData()
  }
  
  func setScrollPosition(_ position: CGFloat, animated: Bool) {
    tableView.scrollRectToVisible(CGRect(x: 0, y: position, width: 1, height: 1), animated: animated)
  }
  
  func setCloseButtonHidden(_ hidden: Bool, animated: Bool) {
    UIView.animate(withDuration: animated ? 0.3 : 0.0) { 
      self.closeButton.alpha = hidden ? 0.0 : 1.0
    }
  }
  
  func setCloseButtonBackgroundColor(_ color: UIColor, for state: UIControl.State) {
    closeButton.setBackgroundColor(color, for: state)
  }
  
  func setCloseButtonForegroundColor(_ color: UIColor, for state: UIControl.State) {
    closeButton.setForegroundColor(color, for: state)
  }
  
  func setTopOffset(_ offset: CGFloat) {
    topConstraint.constant = offset
    setNeedsUpdateConstraints()
    updateConstraintsIfNeeded()
  }
  
  // Private API
  
  private func configureViews() {
    self.backgroundColor = .backgroundColor
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.accessibilityLabel = "Post Table"
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 300
    tableView.allowsSelection = false
    tableView.separatorStyle = .none
    tableView.showsVerticalScrollIndicator = false
    tableView.delegate = self
    addSubview(tableView)
    
    closeButton.accessibilityLabel = "Close Post Button"
    closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
    addSubview(closeButton)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
        tableView.leftAnchor.constraint(equalTo: self.leftAnchor),
        tableView.rightAnchor.constraint(equalTo: self.rightAnchor),
        topConstraint,
        tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        
        closeButton.leftAnchor.constraint(equalTo: tableView.leftAnchor, constant: 20),
        closeButton.topAnchor.constraint(equalTo: tableView.topAnchor, constant: 20),
      ])
  }
  
  @objc private func closeButtonPressed() {
    observer?.didPressCloseButton()
  }
}

extension WWPostView: UITableViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    observer?.didUpdateScroll(with: scrollView.contentOffset.y)
  }
}
