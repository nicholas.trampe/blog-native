//
//  PostDismissInteractor.swift
//  blog
//
//  Created by Nicholas Trampe on 8/16/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class PostDismissInteractor: UIPercentDrivenInteractiveTransition, Interactor {
  
  var hasStarted: Bool = false
  
  func interact(with gesture: UIPanGestureRecognizer,
                in view: UIView,
                presentationAction: () -> (),
                finishAction: () -> (),
                cancelAction: () -> ()) {
    let translation = -Float(gesture.translation(in: view).y)
    let height = Float(view.frame.size.height)
    let progress = CGFloat(fmaxf(translation, 0) / height)
    let velocity = Float(gesture.velocity(in: view).y) 
    
    switch gesture.state {
    case .began:
      hasStarted = true
      presentationAction()
    case .changed:
      update(progress)
    case .cancelled:
      cancelAction()
      cancel()
    case .ended:
      hasStarted = false
      if progress > 0.3 || velocity < -1000 {
        finishAction()
        finish()
      } else {
        cancelAction()
        cancel()
      }
    default:
      break
    }
  }
}
