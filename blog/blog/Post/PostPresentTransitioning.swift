//
//  PostPresentTransitioning.swift
//  blog
//
//  Created by Nicholas Trampe on 8/17/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class PostPresentTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
  var originFrame: CGRect = .zero
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 1.0
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard
      let fromVC = transitionContext.viewController(forKey: .from),
      let toVC = transitionContext.viewController(forKey: .to)
      else {
        return
    }
    
    let containerView = transitionContext.containerView
    
    UIGraphicsBeginImageContextWithOptions(fromVC.view.bounds.size, false, 0)
    fromVC.view.drawHierarchy(in:fromVC.view.bounds, afterScreenUpdates: false)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    let snapshot = UIImageView(image: image)
    
    let topOffset: CGFloat = UIApplication.shared.statusBarFrame.size.height
    
    let originPath = CGMutablePath()
    originPath.addRect(toVC.view.bounds)
    originPath.addRect(CGRect(x: 0, y: topOffset, width: originFrame.size.width, height: originFrame.size.height))
    
    let originMask = CAShapeLayer()
    originMask.path = originPath
    originMask.fillRule = CAShapeLayerFillRule.evenOdd
    
    let finalPath = CGMutablePath()
    finalPath.addRect(toVC.view.bounds)
    finalPath.addRect(containerView.frame)
    
    snapshot.frame = fromVC.view.frame
    containerView.addSubview(snapshot)
    
    toVC.view.frame = CGRect(x: originFrame.origin.x, y: originFrame.origin.y-topOffset, width: originFrame.size.width, height: originFrame.size.height)
    toVC.view.layer.mask = originMask
    containerView.addSubview(toVC.view)
    
    let anim = CABasicAnimation(keyPath: "path")
    anim.fromValue = originPath
    anim.toValue = finalPath
    anim.duration = transitionDuration(using: transitionContext) / 4.0
    anim.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    
    originMask.add(anim, forKey: nil)
    
    CATransaction.begin()
    CATransaction.setDisableActions(true)
    originMask.path = finalPath
    CATransaction.commit()
    
    UIView.animate(withDuration: transitionDuration(using: transitionContext),
                   delay: 0,
                   usingSpringWithDamping: 0.5,
                   initialSpringVelocity: 1.0,
                   options: [],
                   animations: { 
      toVC.view.frame = containerView.frame
    }) { completed in
      snapshot.removeFromSuperview()
      toVC.view.layer.mask = nil
      transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
    }
  }
}
