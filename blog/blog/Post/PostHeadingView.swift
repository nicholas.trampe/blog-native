//
//  PostHeadingView.swift
//  blog
//
//  Created by Nicholas Trampe on 8/14/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class PostHeadingView: UIView {
  let headingLabel = UILabel()
  let subheadingLabel = UILabel()
  let dateLabel = UILabel()
  let backgroundImage = UIImageView()
  private let blackView = UIView()
  
  init() {
    super.init(frame: .zero)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  
  // Private API
  
  private func configureViews() {
    self.clipsToBounds = true
    
    backgroundImage.translatesAutoresizingMaskIntoConstraints = false
    backgroundImage.backgroundColor = .lightGray
    backgroundImage.contentMode = .scaleAspectFill
    backgroundImage.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
    backgroundImage.clipsToBounds = true
    addSubview(backgroundImage)
    
    blackView.translatesAutoresizingMaskIntoConstraints = false
    blackView.backgroundColor = .black
    blackView.alpha = 0.3
    addSubview(blackView)
    
    headingLabel.translatesAutoresizingMaskIntoConstraints = false
    headingLabel.numberOfLines = 0
    headingLabel.font = UIFont.openSansExtraBold(of: FontAttributes.heading1.size)
    headingLabel.textColor = .white
    headingLabel.shadowColor = .black
    headingLabel.shadowOffset = CGSize(width: 1, height: 1)
    addSubview(headingLabel)
    
    subheadingLabel.translatesAutoresizingMaskIntoConstraints = false
    subheadingLabel.numberOfLines = 0
    subheadingLabel.font = UIFont.openSansSemiBold(of: FontAttributes.heading4.size)
    subheadingLabel.textColor = .white
    subheadingLabel.shadowColor = .black
    subheadingLabel.shadowOffset = CGSize(width: 1, height: 1)
    addSubview(subheadingLabel)
    
    dateLabel.translatesAutoresizingMaskIntoConstraints = false
    dateLabel.font = UIFont.openSansSemiBold(of: FontAttributes.heading6.size)
    dateLabel.textColor = .white
    dateLabel.shadowColor = .black
    dateLabel.shadowOffset = CGSize(width: 1, height: 1)
    addSubview(dateLabel)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      backgroundImage.topAnchor.constraint(equalTo: self.topAnchor, constant: -100),
      backgroundImage.leftAnchor.constraint(equalTo: self.leftAnchor),
      backgroundImage.rightAnchor.constraint(equalTo: self.rightAnchor),
      backgroundImage.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 100),
      
      blackView.topAnchor.constraint(equalTo: self.topAnchor),
      blackView.leftAnchor.constraint(equalTo: self.leftAnchor),
      blackView.rightAnchor.constraint(equalTo: self.rightAnchor),
      blackView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
      
      headingLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 100),
      headingLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
      headingLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20),
      
      subheadingLabel.topAnchor.constraint(equalTo: headingLabel.bottomAnchor, constant: 10),
      subheadingLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
      subheadingLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20),
      subheadingLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20),
      
      dateLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
      dateLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20),
      ])
  }
}
