//
//  PostViewController.swift
//  blog
//
//  Created by Nicholas Trampe on 8/14/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PostViewControllerObserver: class {
  func didPressCloseButton()
  func didUpdateSizeOrRotation()
}

class PostViewController: UIViewController {
  weak var observer: PostViewControllerObserver? = nil
  private let postView = WWPostView()
  private var presenter: PostPresenter
  
  init(post: Post, photoService: PhotoService) {
    let viewModel: PostViewModel = WWPostViewModel(post: post)
    let cellProvider: PostDetailsCellProvider = WWPostDetailsCellProvider(viewModel: viewModel, photoService: photoService)
    presenter = WWPostPresenter(view: postView, cellProvider: cellProvider)
    super.init(nibName: nil, bundle: nil)
    presenter.observer = self

    modalPresentationStyle = .fullScreen
  }
    
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func loadView() {
    view = postView
  }
  
  func updateForPeek() {
    presenter.peek()
  }
  
  func updateForPop() {
    presenter.pop()
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    
    self.observer?.didUpdateSizeOrRotation()
  }
}

extension PostViewController: PostPresenterObserver {
  func didPressCloseButton() {
    observer?.didPressCloseButton()
  }
}
