//
//  PostCellViewModel.swift
//  blog
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol PostCellViewModel {
  var headingText: String { get }
  var subheadingText: String { get }
  var cover: String { get }
  var dateText: String { get }
}

struct WWPostCellViewModel: PostCellViewModel {
  let headingText: String
  let subheadingText: String
  let cover: String
  let dateText: String
  
  init(post: Post) {
    headingText = post.heading
    subheadingText = post.subheading
    cover = post.cover
    
    let calendar = Calendar.current
    let year = calendar.component(.year, from: post.date)
    let currentYear = calendar.component(.year, from: Date())
    
    if year == currentYear {
      dateText = DateFormatter.monthDayFormatter.string(from: post.date)
    } else {
      dateText = DateFormatter.monthDayYearFormatter.string(from: post.date)
    }
  }
}
