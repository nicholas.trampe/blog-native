//
//  PostCell.swift
//  blog
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {
  let heading = PostHeadingView()
  private let separatorView = UIView()
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    heading.backgroundImage.image = nil
  }
  
  override func setHighlighted(_ highlighted: Bool, animated: Bool) {
    UIView.animate(withDuration: 0.1) { 
      if highlighted {
        self.transform = CGAffineTransform(scaleX: 0.99, y: 0.99)
      } else {
        self.transform = .identity
      }
    }
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    
  }
  
  // Private API
  
  private func configureViews() {
    self.clipsToBounds = true
    
    heading.translatesAutoresizingMaskIntoConstraints = false
    contentView.addSubview(heading)
    
    separatorView.translatesAutoresizingMaskIntoConstraints = false
    separatorView.backgroundColor = .white
    contentView.addSubview(separatorView)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      heading.topAnchor.constraint(equalTo: contentView.topAnchor),
      heading.leftAnchor.constraint(equalTo: contentView.leftAnchor),
      heading.rightAnchor.constraint(equalTo: contentView.rightAnchor),
      heading.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
      
      separatorView.heightAnchor.constraint(equalToConstant: 1),
      separatorView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
      separatorView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
      separatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
      ])
  }
}
