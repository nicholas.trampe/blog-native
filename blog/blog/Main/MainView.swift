//
//  MainView.swift
//  blog
//
//  Created by Nicholas Trampe on 8/12/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol MainViewObserver: class {
  func didSelect(_ tableView: UITableView, _ indexPath: IndexPath)
  func didPressMenuButton()
  func didPressPlacesButton()
  func didFinishShowingLogo()
  func didFinishHidingLogo()
}

protocol MainView {
  var observer: MainViewObserver? { get set }
  
  func setCellProvider(_ cellProvider: CellProvider)
  func setIcon(_ icon: IconFontIcon)
  func setTitle(with text: String)
  func setMessage(with text: String)
  func setErrorHidden(hidden: Bool)
  func updateData()
  
  func showLogo(after delay: TimeInterval)
  func hideLogo(after delay: TimeInterval)
  
  func indexPath(at location: CGPoint) -> IndexPath?
  func cellFrame(at location: CGPoint) -> CGRect?
}

class WWMainView: UIView, MainView {
  public weak var observer: MainViewObserver? = nil
  private let tableView = UITableView(frame: .zero, style: .plain)
  private let errorView = ErrorView()
  private let navbar = MainNavBar()
  private let logo = Logo()
  
  init() {
    super.init(frame: .zero)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // Public API
  
  func setCellProvider(_ cellProvider: CellProvider) {
    cellProvider.registerCells(for: tableView)
    tableView.dataSource = cellProvider
  }
  
  func setIcon(_ icon: IconFontIcon) {
    errorView.setIcon(icon: icon)
  }
  
  func setTitle(with text: String) {
    errorView.setTitle(with: text)
  }
  
  func setMessage(with text: String) {
    errorView.setMessage(with: text)
  }
  
  func setErrorHidden(hidden: Bool) {
    errorView.isHidden = hidden
  }
  
  public func updateData() {
    tableView.reloadData()
  }
  
  func showLogo(after delay: TimeInterval) {
    tableView.isHidden = true
    navbar.transform = CGAffineTransform(translationX: 0, y: -100)
    logo.animate()
    logo.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
    logo.alpha = 0
    UIView.animate(withDuration: 0.5,
                   delay: delay,
                   usingSpringWithDamping: 0.5,
                   initialSpringVelocity: 0.5,
                   options: [],
                   animations: {
                    self.logo.alpha = 1
                    self.logo.transform = .identity
    }) { _ in 
      self.observer?.didFinishShowingLogo()
    }
  }
  
  func hideLogo(after delay: TimeInterval) {
    for cell in tableView.visibleCells {
      cell.transform = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height)
    }
    
    tableView.isHidden = false
    tableView.isUserInteractionEnabled = false
    
    UIView.animate(withDuration: 0.5,
                   delay: delay,
                   usingSpringWithDamping: 0.8,
                   initialSpringVelocity: 0.1,
                   options: [],
                   animations: {
      self.navbar.transform = .identity
    }) { _ in
      self.tableView.isUserInteractionEnabled = true
    }
    
    var cellDelay: TimeInterval = delay
    for cell in tableView.visibleCells {
      UIView.animate(withDuration: 0.5,
                     delay: cellDelay,
                     usingSpringWithDamping: 0.8,
                     initialSpringVelocity: 0.1,
                     options: [],
                     animations: {
        cell.transform = .identity
      }) { _ in }
      cellDelay += 0.1
    }
    
    UIView.animate(withDuration: 0.3,
                   delay: delay,
                   usingSpringWithDamping: 0.8,
                   initialSpringVelocity: 0.1,
                   options: [],
                   animations: {
                    self.logo.alpha = 0
                    self.logo.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
    }) { _ in 
      self.observer?.didFinishHidingLogo()
    }
  }
  
  func indexPath(at location: CGPoint) -> IndexPath? {
    return tableView.indexPathForRow(at: convert(location, to: tableView))
  }
  
  func cellFrame(at location: CGPoint) -> CGRect? {
    guard let indexPath = indexPath(at: location),
          let frame = tableView.cellForRow(at: indexPath)?.frame else {
      return nil
    }
    
    return tableView.convert(frame, to: nil)
  }
  
  // Private API
  
  private func configureViews() {
    self.backgroundColor = .backgroundColor
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.accessibilityLabel = "Main Table"
    tableView.backgroundColor = .backgroundColor
    tableView.delegate = self
    tableView.separatorStyle = .none
    tableView.isHidden = true
    addSubview(tableView)
    
    errorView.isHidden = true
    addSubview(errorView)
    
    navbar.menuAction = {
      self.observer?.didPressMenuButton()
    }
    navbar.placesAction = {
      self.observer?.didPressPlacesButton()
    }
    navbar.transform = CGAffineTransform(translationX: 0, y: -100)
    addSubview(navbar)
    
    logo.alpha = 0
    addSubview(logo)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
        navbar.topAnchor.constraint(equalTo: self.topAnchor),
        navbar.leftAnchor.constraint(equalTo: self.leftAnchor),
        navbar.rightAnchor.constraint(equalTo: self.rightAnchor),
      
        errorView.leftAnchor.constraint(equalTo: self.leftAnchor),
        errorView.rightAnchor.constraint(equalTo: self.rightAnchor),
        errorView.topAnchor.constraint(equalTo: navbar.bottomAnchor),
        errorView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
      
        tableView.leftAnchor.constraint(equalTo: self.leftAnchor),
        tableView.rightAnchor.constraint(equalTo: self.rightAnchor),
        tableView.topAnchor.constraint(equalTo: navbar.bottomAnchor),
        tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        
        logo.centerXAnchor.constraint(equalTo: self.centerXAnchor),
        logo.centerYAnchor.constraint(equalTo: self.centerYAnchor),
      ])
  }
}

extension WWMainView: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    observer?.didSelect(tableView, indexPath)
  }
}

private class MainNavBar: UIView {
  let menuButton = UIButton.iconButton(of: 24, with: .menu)
  let placesButton = UIButton.iconButton(of: 24, with: .earth)
  let label = UILabel()
  let separator = UIView()
  var menuAction: () -> () = {}
  var placesAction: () -> () = {}
  private var shouldUpdateConstraints = true
  
  init() {
    super.init(frame: .zero)
    configureViews()
  }
  
  override func updateConstraints() {
    if shouldUpdateConstraints {
      constrainViews()
      shouldUpdateConstraints = false
    }
    super.updateConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configureViews() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = .backgroundColor
    
    menuButton.accessibilityLabel = "Menu Button"
    menuButton.setTitleColor(.foregroundColor, for: .normal)
    menuButton.setTitleColor(.lightGray, for: .highlighted)
    menuButton.addTarget(self, action: #selector(menuButtonPressed), for: .touchUpInside)
    addSubview(menuButton)
    
    placesButton.accessibilityLabel = "Places Button"
    placesButton.setTitleColor(.foregroundColor, for: .normal)
    placesButton.setTitleColor(.lightGray, for: .highlighted)
    placesButton.addTarget(self, action: #selector(placesButtonPressed), for: .touchUpInside)
    addSubview(placesButton)
    
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = .openSansBold(of: 18)
    label.textColor = .foregroundColor
    label.textAlignment = .center
    label.text = "Worldwide Walkabouts"
    addSubview(label)
    
    separator.translatesAutoresizingMaskIntoConstraints = false
    separator.backgroundColor = .foregroundColor
    addSubview(separator)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      label.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      label.topAnchor.constraint(equalTo: self.topAnchor, constant: 10 + UIApplication.shared.statusBarFrame.size.height),
      label.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
      
      menuButton.centerYAnchor.constraint(equalTo: label.centerYAnchor),
      menuButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
      
      placesButton.centerYAnchor.constraint(equalTo: label.centerYAnchor),
      placesButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10),
      
      separator.leftAnchor.constraint(equalTo: self.leftAnchor),
      separator.rightAnchor.constraint(equalTo: self.rightAnchor),
      separator.bottomAnchor.constraint(equalTo: self.bottomAnchor),
      separator.heightAnchor.constraint(equalToConstant: 0.5)
      ])
  }
  
  @objc private func menuButtonPressed() {
    menuAction()
  }
  
  @objc private func placesButtonPressed() {
    placesAction()
  }
}

private class ErrorView: UIView {
  private let iconLabel = UILabel.iconLabel(of: FontAttributes.iconExtraLarge.size, with: .compass)
  private let titleLabel = UILabel()
  private let messageLabel = UILabel()
  
  init() {
    super.init(frame: .zero)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // Public API
  
  func setIcon(icon: IconFontIcon?) {
    iconLabel.icon = icon
  }
  
  func setTitle(with text: String?) {
    titleLabel.text = text
  }
  
  func setMessage(with text: String?) {
    messageLabel.text = text
  }
  
  // Private API
  
  private func configureViews() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = .clear
    
    iconLabel.textColor = .lightGray
    addSubview(iconLabel)
    
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.font = .openSansSemiBold(of: 22)
    titleLabel.textColor = .lightGray
    titleLabel.textAlignment = .center
    titleLabel.numberOfLines = 0
    addSubview(titleLabel)
    
    messageLabel.translatesAutoresizingMaskIntoConstraints = false
    messageLabel.font = .openSans(of: 14)
    messageLabel.textColor = .lightGray
    messageLabel.textAlignment = .center
    messageLabel.numberOfLines = 0
    addSubview(messageLabel)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      iconLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      iconLabel.bottomAnchor.constraint(equalTo: titleLabel.topAnchor, constant: -30),
      
      titleLabel.leftAnchor.constraint(lessThanOrEqualTo: self.leftAnchor, constant: 40),
      titleLabel.rightAnchor.constraint(lessThanOrEqualTo: self.rightAnchor, constant: -40),
      titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
      
      messageLabel.leftAnchor.constraint(lessThanOrEqualTo: self.leftAnchor, constant: 40),
      messageLabel.rightAnchor.constraint(lessThanOrEqualTo: self.rightAnchor, constant: -40),
      messageLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
      ])
  }
}
