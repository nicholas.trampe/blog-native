//
//  PostCellViewModelTransformer.swift
//  blog
//
//  Created by Nicholas Trampe on 8/15/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol PostCellViewModelTransformer {
  func viewModels(for posts: [Post]) -> [PostCellViewModel]
}

struct WWPostCellViewModelTransformer: PostCellViewModelTransformer {
  func viewModels(for posts: [Post]) -> [PostCellViewModel] {
    return posts.map { (post) -> PostCellViewModel in
      return WWPostCellViewModel(post: post)
    }
  }
}
