//
//  MainPresenter.swift
//  blog
//
//  Created by Nicholas Trampe on 8/12/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol MainPresenter {
  var observer: MainPresenterObserver? { get set }
  func start()
  
  func post(at location: CGPoint) -> Post?
  func frame(at location: CGPoint) -> CGRect?
}

protocol MainPresenterObserver: class {
  func didSelect(_ post: Post, _ frame: CGRect)
  func didPressMenuButton()
  func didPressPlacesButton()
  func didShowPosts()
}

class WWMainPresenter: MainPresenter {
  public weak var observer: MainPresenterObserver? = nil
  private var view: MainView
  private var model: MainModel
  private let cellProvider: PostCellProvider
  private let cellViewModelTransformer: PostCellViewModelTransformer
  
  public init(view: MainView,
              model: MainModel,
              cellProvider: PostCellProvider,
              cellViewModelTransformer: PostCellViewModelTransformer) {
    self.view = view
    self.model = model
    self.cellProvider = cellProvider
    self.cellViewModelTransformer = cellViewModelTransformer
    
    self.model.observer = self
    self.view.observer = self
    
    self.view.setCellProvider(cellProvider)
    
    registerForNotifications()
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  // Public API
  
  public func start() {
    view.showLogo(after: 0.0)
    model.updatePosts()
  }
  
  func post(at location: CGPoint) -> Post? {
    guard let index = view.indexPath(at: location)?.row,
          index >= 0 && index < model.posts.count else {
      return nil
    }
    
    return model.posts[index]
  }
  
  func frame(at location: CGPoint) -> CGRect? {
    return view.cellFrame(at: location)
  }
  
  // Private API
  
  private func registerForNotifications() {
    NotificationCenter.default.addObserver(self, selector: #selector(didEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
  }
  
  @objc private func didEnterForeground() {
    model.updatePosts()
  } 
}

extension WWMainPresenter: MainModelObserver {
  func didUpdatePosts(posts: [Post]) {
    cellProvider.viewModels = cellViewModelTransformer.viewModels(for: posts)
    model.lastSyncDate = Date()
    view.setErrorHidden(hidden: true)
    view.updateData()
    view.hideLogo(after: 2.0)
  }
  
  func didFailToUpdatePosts(error: Error) {
    cellProvider.viewModels = []
    view.updateData()
    view.hideLogo(after: 0.0)
    view.setIcon(.compass)
    view.setTitle(with: "Could Not Update Posts")
    view.setMessage(with: error.localizedDescription)
    view.setErrorHidden(hidden: false)
  }
}

extension WWMainPresenter: MainViewObserver {
  func didSelect(_ tableView: UITableView, _ indexPath: IndexPath) {
    let post = model.posts[indexPath.row]
    let rect = tableView.convert(tableView.rectForRow(at: indexPath), to: nil)
    observer?.didSelect(post, rect)
  }
  
  func didPressMenuButton() {
    observer?.didPressMenuButton()
  }
  
  func didPressPlacesButton() {
    observer?.didPressPlacesButton()
  }
  
  func didFinishHidingLogo() {
    observer?.didShowPosts()
  }
  
  func didFinishShowingLogo() {
    
  }
}
