//
//  MainViewController.swift
//  blog
//
//  Created by Nicholas Trampe on 8/12/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol MainViewControllerObserver: class {
  func didPressMenuButton()
  func didPressPlacesButton()
  func didShowPosts()
  func didSelect(_ post: Post, _ frame: CGRect)
  func didPeek(_ post: Post, _ frame: CGRect) -> UIViewController
  func didPop()
}

class MainViewController: UIViewController {
  public weak var observer: MainViewControllerObserver? = nil
  private let mainView = WWMainView()
  private var presenter: MainPresenter
  private var firstLaunch = true
  
  init(mainModel: MainModel,
       cellProvider: PostCellProvider,
       cellViewModelTransformer: PostCellViewModelTransformer) {
    presenter = WWMainPresenter(view: mainView,
                                model: mainModel,
                                cellProvider: cellProvider,
                                cellViewModelTransformer: cellViewModelTransformer)
    super.init(nibName: nil, bundle: nil)
    presenter.observer = self
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func loadView() {
    view = mainView
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    if firstLaunch {
      presenter.start()
      firstLaunch = false
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    registerForPreviewing(with: self, sourceView: view)
  }
  
  override var shouldAutorotate: Bool {
    get {
      if let vc = presentedViewController {
        return vc.shouldAutorotate
      }
      
      return true
    }
  }
}

extension MainViewController: UIViewControllerPreviewingDelegate {
  func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
    guard let post = presenter.post(at: location),
          let frame = presenter.frame(at: location) else {
      return nil
    }
    
    previewingContext.sourceRect = frame
    
    return observer?.didPeek(post, frame)
  }
  
  func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
    observer?.didPop()
  }
}

extension MainViewController: MainPresenterObserver {
  func didSelect(_ post: Post, _ frame: CGRect) {
    observer?.didSelect(post, frame)
  }
  
  func didPressMenuButton() {
    observer?.didPressMenuButton()
  }
  
  func didPressPlacesButton() {
    observer?.didPressPlacesButton()
  }
  
  func didShowPosts() {
    observer?.didShowPosts()
  }
}
