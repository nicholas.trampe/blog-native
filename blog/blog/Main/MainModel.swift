//
//  MainModel.swift
//  blog
//
//  Created by Nicholas Trampe on 8/12/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol MainModelObserver: class {
  func didUpdatePosts(posts: [Post])
  func didFailToUpdatePosts(error: Error)
}

protocol MainModel {
  func updatePosts()
  var observer: MainModelObserver? { get set }
  var posts: [Post] { get }
  var lastSyncDate: Date? { get set }
  var syncExpirationInterval: TimeInterval { get set }
}

class WWMainModel: MainModel {
  private let postService: PostService
  public weak var observer: MainModelObserver? = nil
  public var posts: [Post] = []
  public var lastSyncDate: Date? = nil
  public var syncExpirationInterval: TimeInterval = 1 * 60 * 60
  
  init(postService: PostService) {
    self.postService = postService
  }
  
  func updatePosts() {
    
    if let syncDate = lastSyncDate {
      let seconds = -syncDate.timeIntervalSinceNow
      if seconds <= syncExpirationInterval {
        return
      }
    }
    
    postService.get().then({ posts in
      self.posts = posts
      self.observer?.didUpdatePosts(posts: posts)
    }).catch({ error in 
      self.observer?.didFailToUpdatePosts(error: error)
    })
  }
}
