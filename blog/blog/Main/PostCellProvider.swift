//
//  PostCellProvider.swift
//  blog
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PostCellProvider: CellProvider {
  var viewModels: [PostCellViewModel] { get set }
}

class WWPostCellProvider: NSObject, PostCellProvider {
  var viewModels: [PostCellViewModel] = []
  private let photoService: PhotoService 
  
  init(photoService: PhotoService) {
    self.photoService = photoService
  }
  
  func registerCells(for tableView: UITableView) {
    tableView.register(PostCell.self, forCellReuseIdentifier: PostCell.defaultCellIdentifier)
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 300
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModels.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: PostCell.defaultCellIdentifier) as! PostCell
    let model = viewModels[indexPath.row]
    
    cell.accessibilityLabel = "Post Cell \(indexPath.row)"
    cell.heading.headingLabel.text = model.headingText
    cell.heading.subheadingLabel.text = model.subheadingText
    cell.heading.dateLabel.text = model.dateText
    
    photoService.get(id: model.cover).then({ image in
      cell.heading.backgroundImage.image = image
    }).catch({ error in 
      cell.heading.backgroundImage.image = nil
    })
    
    return cell
  }
}
