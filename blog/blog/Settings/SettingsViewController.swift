//
//  SettingsViewController.swift
//  blog
//
//  Created by Nicholas Trampe on 8/6/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol SettingsViewControllerObserver: class {
  func didClose()
}

class SettingsViewController: UIViewController {
  weak var observer: SettingsViewControllerObserver? = nil
  private let model: SettingsModel
  private let settingsView = WWSettingsView()
  private var presenter: SettingsPresenter
  
  init(cachesDirectory: CacheDirectory) {
    model = WWSettingsModel(cachesDirectory: cachesDirectory)
    presenter = WWSettingsPresenter(model: model, view: settingsView)
    super.init(nibName: nil, bundle: nil)
    
    presenter.observer = self
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func loadView() {
    view = settingsView
  }
}

extension SettingsViewController: SettingsPresenterObserver {
  func didPressCloseButton() {
    observer?.didClose()
  }
}
