//
//  SettingsPresenter.swift
//  blog
//
//  Created by Nicholas Trampe on 8/6/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol SettingsPresenterObserver: class {
  func didPressCloseButton()
}

protocol SettingsPresenter {
  var observer: SettingsPresenterObserver? { get set }
}

class WWSettingsPresenter: SettingsPresenter {
  weak var observer: SettingsPresenterObserver? = nil
  
  private var model: SettingsModel
  private var view: SettingsView
  
  init(model: SettingsModel, view: SettingsView) {
    self.model = model
    self.view = view
    
    self.view.observer = self
  }
}

extension WWSettingsPresenter: SettingsViewObserver {
  func didPressCloseButton() {
    observer?.didPressCloseButton()
  }
  
  func didPressClearCacheButton() {
    model.clearCache()
  }
}
