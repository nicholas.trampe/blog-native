//
//  SettingsModel.swift
//  blog
//
//  Created by Nicholas Trampe on 8/6/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol SettingsModel {
  func clearCache()
}

class WWSettingsModel: SettingsModel {
  private let cachesDirectory: CacheDirectory
  
  init(cachesDirectory: CacheDirectory) {
    self.cachesDirectory = cachesDirectory
  }
  
  func clearCache() {
    cachesDirectory.clear()
  }
}
