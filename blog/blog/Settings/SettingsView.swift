//
//  SettingsView.swift
//  blog
//
//  Created by Nicholas Trampe on 8/6/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol SettingsViewObserver: class {
  func didPressCloseButton()
  func didPressClearCacheButton()
}

protocol SettingsView {
  var observer: SettingsViewObserver? { get set }
}

class WWSettingsView: UIView, SettingsView {
  weak var observer: SettingsViewObserver? = nil
  private let closeButton = UIButton.iconButton(of: FontAttributes.iconSmall.size, with: .cancel)
  private let tableView = UITableView(frame: .zero)
  
  init() {
    super.init(frame: .zero)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: Private API
  
  @objc private func closeButtonPressed() {
    observer?.didPressCloseButton()
  }
  
  func configureViews() {
    backgroundColor = .backgroundColor
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.backgroundColor = .backgroundColor
    tableView.separatorColor = UIColor.foregroundColor.withAlphaComponent(0.5)
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 50
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "SettingsCell")
    tableView.dataSource = self
    tableView.delegate = self
    addSubview(tableView)
    
    closeButton.translatesAutoresizingMaskIntoConstraints = false
    closeButton.setTitleColor(.foregroundColor, for: .normal)
    closeButton.setTitleColor(.lightGray, for: .highlighted)
    closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
    addSubview(closeButton)
  }
  
  func constrainViews() {
    NSLayoutConstraint.activate([
      closeButton.topAnchor.constraint(equalTo: self.topAnchor, constant: UIApplication.shared.statusBarFrame.size.height + 10),
      closeButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20),
      
      tableView.topAnchor.constraint(equalTo: closeButton.bottomAnchor, constant: 10),
      tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
      tableView.leftAnchor.constraint(equalTo: self.leftAnchor),
      tableView.rightAnchor.constraint(equalTo: self.rightAnchor),
      ])
  }
}

extension WWSettingsView: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell", for: indexPath)
    cell.textLabel?.text = "Clear Cache"
    return cell
  }
}

extension WWSettingsView: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    observer?.didPressClearCacheButton()
    tableView.deselectRow(at: indexPath, animated: true)
  }
}
