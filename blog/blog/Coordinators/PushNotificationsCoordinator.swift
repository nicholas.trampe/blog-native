//
//  PushNotificationsCoordinator.swift
//  blog
//
//  Created by Nicholas Trampe on 7/8/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PushNotificationsCoordinatorObserver: class {
  func didDismissPushNotificationsCoordinator(_ coordinator: Coordinator)
}

class PushNotificationsCoordinator: Coordinator {
  private let pushNotificationsViewController: PushNotificationsViewController
  private let transitioning = PushNotificationsTransitioning()
  
  weak var observer: PushNotificationsCoordinatorObserver? = nil
  var childCoordinators: [Coordinator] = []
  var rootViewController: UIViewController {
    get {
      return pushNotificationsViewController
    }
  }
  
  init(deviceService: DeviceService) {
    pushNotificationsViewController = PushNotificationsViewController(deviceService: deviceService)
    pushNotificationsViewController.observer = self
    pushNotificationsViewController.transitioningDelegate = transitioning
  }
}

extension PushNotificationsCoordinator: PushNotificationsViewControllerObserver {
  func didDismiss() {
    pushNotificationsViewController.dismiss(animated: true) { 
      self.observer?.didDismissPushNotificationsCoordinator(self)
    }
  }
}

