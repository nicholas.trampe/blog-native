//
//  MainCoordinator.swift
//  blog
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class MainCoordinator: Coordinator {
  private var menuCoordinator: MenuCoordinator? = nil 
  private var postPopCoordinator: PostCoordinator? = nil
  private let menuTransitioning = MenuTransitioning()
  private let mainViewController: MainViewController
  private let photoService: PhotoService
  private let contactService: ContactService
  private let placeService: PlaceService
  private let deviceService: DeviceService
  private let cachesDirectory: CacheDirectory
  
  var childCoordinators: [Coordinator] = []
  var rootViewController: UIViewController {
    get {
      return mainViewController
    }
  }
  
  init(postService: PostService,
       photoService: PhotoService,
       contactService: ContactService,
       placeService: PlaceService,
       deviceService: DeviceService,
       cachesDirectory: CacheDirectory) {
    self.photoService = photoService
    self.contactService = contactService
    self.placeService = placeService
    self.deviceService = deviceService
    self.cachesDirectory = cachesDirectory
    
    let mainModel: MainModel = WWMainModel(postService: postService)
    let cellProvider: PostCellProvider = WWPostCellProvider(photoService: photoService)
    let cellViewModelTransformer: PostCellViewModelTransformer = WWPostCellViewModelTransformer()
    mainViewController = MainViewController(mainModel: mainModel,
                                            cellProvider: cellProvider,
                                            cellViewModelTransformer: cellViewModelTransformer)
    
    mainViewController.observer = self
    
    menuTransitioning.setPresentingViewController(mainViewController)
    menuTransitioning.delegate = self
  }
  
  private func showMenu() {
    let coordinator = MenuCoordinator(contactService: contactService, cachesDirectory: cachesDirectory)
    coordinator.rootViewController.transitioningDelegate = menuTransitioning
    menuTransitioning.setPresentedViewController(coordinator.rootViewController)
    rootViewController.present(coordinator.rootViewController, animated: true, completion: nil)
    menuCoordinator = coordinator
  }
  
  private func hideMenu() {
    rootViewController.dismiss(animated: true)
  }
  
  private func showPushNotificationDialog() {
    let notificationModel: PushNotificationModel = WWPushNotificationModel(deviceService: deviceService)
    
    if !notificationModel.isRegistered {
      let pushNotificationsCoordinator = PushNotificationsCoordinator(deviceService: self.deviceService)
      pushNotificationsCoordinator.observer = self
      presentCoordinator(pushNotificationsCoordinator)
    }
  }
}


extension MainCoordinator: MainViewControllerObserver {
  func didSelect(_ post: Post, _ frame: CGRect) {
    let postCoordinator = PostCoordinator(post: post, photoService: photoService, presentationRect: frame)
    postCoordinator.observer = self
    presentCoordinator(postCoordinator)
  }
  
  func didPeek(_ post: Post, _ frame: CGRect) -> UIViewController {
    postPopCoordinator = PostCoordinator(post: post, photoService: photoService, presentationRect: frame)
    postPopCoordinator?.observer = self
    postPopCoordinator?.updateForPeek()
    return postPopCoordinator!.rootViewController
  }
  
  func didPop() {
    postPopCoordinator?.updateForPop()
    presentCoordinator(postPopCoordinator!)
    postPopCoordinator = nil
  }
  
  func didPressMenuButton() {
    showMenu()
  }
  
  func didPressPlacesButton() {
    let placesCoordinator = PlacesCoordinator(placeService: placeService, photoService: photoService)
    placesCoordinator.observer = self
    presentCoordinator(placesCoordinator)
  }
  
  func didShowPosts() {
    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
      self.showPushNotificationDialog()
    })
  }
}

extension MainCoordinator: MenuTransitioningDelegate {
  func present() {
    showMenu()
  }
    
  func dismiss() {
    hideMenu()
  }
  
  func didDismiss() {
    menuCoordinator = nil
  }
}

extension MainCoordinator: PostCoordinatorObserver {
  func didDismissPostCoordinator(_ coordinator: Coordinator) {
    removeChildCoordinator(coordinator)
  }
}

extension MainCoordinator: PlacesCoordinatorObserver {
  func didDismissPlacesCoordinator(_ coordinator: Coordinator) {
    removeChildCoordinator(coordinator)
  }
}

extension MainCoordinator: PushNotificationsCoordinatorObserver {
  func didDismissPushNotificationsCoordinator(_ coordinator: Coordinator) {
    removeChildCoordinator(coordinator)
  }
}
