//
//  Coordinator.swift
//  blog
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol Coordinator: class {
  var rootViewController: UIViewController { get } 
  var childCoordinators: [Coordinator] { get set }
}

extension Coordinator {
  
  func addChildCoordinator(_ childCoordinator: Coordinator) {
    self.childCoordinators.append(childCoordinator)
  }
  
  func removeChildCoordinator(_ childCoordinator: Coordinator) {
    self.childCoordinators = self.childCoordinators.filter { $0 !== childCoordinator }
  }
  
  func presentCoordinator(_ childCoordinator: Coordinator) {
    rootViewController.present(childCoordinator.rootViewController, animated: true) {
      self.addChildCoordinator(childCoordinator)
    }
  }
  
  func dismissCoordinator(_ childCoordinator: Coordinator) {
    rootViewController.dismiss(animated: true) { 
      self.removeChildCoordinator(childCoordinator)
    }
  }
}

