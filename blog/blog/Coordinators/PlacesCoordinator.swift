//
//  PlacesCoordinator.swift
//  blog
//
//  Created by Nicholas Trampe on 9/11/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PlacesCoordinatorObserver: class {
  func didDismissPlacesCoordinator(_ coordinator: Coordinator)
}

class PlacesCoordinator: Coordinator {
  private let placesViewController: PlacesViewController
  private let transitioning = PlacesTransitioning()
  
  weak var observer: PlacesCoordinatorObserver? = nil
  var childCoordinators: [Coordinator] = []
  var rootViewController: UIViewController {
    get {
      return placesViewController
    }
  }
  
  init(placeService: PlaceService, photoService: PhotoService) {
    placesViewController = PlacesViewController(placeService: placeService, photoService: photoService)
    placesViewController.observer = self
    placesViewController.transitioningDelegate = transitioning
  }
}

extension PlacesCoordinator: PlacesViewControllerObserver {
  func didPressCloseButton() {
    placesViewController.dismiss(animated: true) { 
      self.observer?.didDismissPlacesCoordinator(self)
    }
  }
}
