//
//  AboutCoordinator.swift
//  blog
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol AboutCoordinatorObserver: class {
  func didDismissAboutCoordinator(_ coordinator: Coordinator)
}

class AboutCoordinator: Coordinator {
  private let aboutViewController = AboutViewController()
  private let transitioning = MenuItemTransitioning()
  
  weak var observer: AboutCoordinatorObserver? = nil
  var childCoordinators: [Coordinator] = []
  var rootViewController: UIViewController {
    get {
      return aboutViewController
    }
  }
  
  init() {
    aboutViewController.observer = self
    aboutViewController.transitioningDelegate = transitioning
  }
}

extension AboutCoordinator: AboutViewControllerObserver {
  func didDismiss() {
    observer?.didDismissAboutCoordinator(self)
  }
}
