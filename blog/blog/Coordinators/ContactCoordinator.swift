//
//  ContactCoordinator.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol ContactCoordinatorObserver: class {
  func didDismissContactCoordinator(_ coordinator: Coordinator)
}

class ContactCoordinator: Coordinator {
  private let contactViewController: ContactViewController
  private let transitioning = MenuItemTransitioning()
  
  weak var observer: ContactCoordinatorObserver? = nil
  var childCoordinators: [Coordinator] = []
  var rootViewController: UIViewController {
    get {
      return contactViewController
    }
  }
  
  init(contactService: ContactService) {
    contactViewController = ContactViewController(contactService: contactService)
    contactViewController.observer = self
    contactViewController.transitioningDelegate = transitioning
  }
}

extension ContactCoordinator: ContactViewControllerObserver {
  func didDismiss() {
    observer?.didDismissContactCoordinator(self)
  }
  
  func didSend() {
    observer?.didDismissContactCoordinator(self)
  }
}
