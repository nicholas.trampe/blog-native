//
//  PostCoordinator.swift
//  blog
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PostCoordinatorObserver: class {
  func didDismissPostCoordinator(_ coordinator: Coordinator)
}

class PostCoordinator: Coordinator {
  weak var observer: PostCoordinatorObserver? = nil
  private let postViewController: PostViewController
  private let postTransitioning: PostTransitioning
  var childCoordinators: [Coordinator] = []
  var rootViewController: UIViewController {
    get {
      return postViewController
    }
  }
  
  init(post: Post, photoService: PhotoService, presentationRect: CGRect) {
    postViewController = PostViewController(post: post, photoService: photoService)
    postTransitioning = PostTransitioning(originFrame: presentationRect,
                                          viewController: postViewController)
    postTransitioning.delegate = self
    postViewController.transitioningDelegate = postTransitioning
    postViewController.observer = self
  }
  
  func updateForPeek() {
    postViewController.updateForPeek()
  }
  
  func updateForPop() {
    postViewController.updateForPop()
  }
}

extension PostCoordinator: PostTransitioningDelegate {
  func dismiss() {
    rootViewController.dismiss(animated: true)
  }
  
  func didDismiss() {
    self.observer?.didDismissPostCoordinator(self)
  }
}

extension PostCoordinator: PostViewControllerObserver {
  func didPressCloseButton() {
    rootViewController.dismiss(animated: true) {
      self.observer?.didDismissPostCoordinator(self)
    }
  }
  
  func didUpdateSizeOrRotation() {
    postViewController.transitioningDelegate = nil
  }
}
