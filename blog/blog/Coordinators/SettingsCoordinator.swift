//
//  SettingsCoordinator.swift
//  blog
//
//  Created by Nicholas Trampe on 8/6/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol SettingsCoordinatorObserver: class {
  func didDismissSettingsCoordinator(_ coordinator: Coordinator)
}

class SettingsCoordinator: Coordinator {
  weak var observer: SettingsCoordinatorObserver? = nil
  private let settingsViewController: SettingsViewController
  private let transitioning = MenuItemTransitioning()
  
  var childCoordinators: [Coordinator] = []
  var rootViewController: UIViewController {
    get {
      return settingsViewController
    }
  }
  
  init(cachesDirectory: CacheDirectory) {
    settingsViewController = SettingsViewController(cachesDirectory: cachesDirectory)
    settingsViewController.observer = self
    settingsViewController.transitioningDelegate = transitioning
  }
}

extension SettingsCoordinator: SettingsViewControllerObserver {
  func didClose() {
    observer?.didDismissSettingsCoordinator(self)
  }
}
