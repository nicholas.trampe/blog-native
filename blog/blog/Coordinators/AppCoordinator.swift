//
//  AppCoordinator.swift
//  blog
//
//  Created by Nicholas Trampe on 8/17/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit
import GoogleMaps

class AppCoordinator: Coordinator {
  var childCoordinators: [Coordinator] = []
  var rootViewController: UIViewController {
    get {
      return window.rootViewController!
    }
  }
  
  private lazy var window: UIWindow = UIWindow(frame: UIScreen.main.bounds)
  private let service: Service = WWService(baseURL: Configuration.baseURL)
  private let cachesDirectory = WWCacheDirectory()
  private lazy var postCache: DataCache = {
    return WWDataCache(name: "posts", directory: cachesDirectory)
  }()
  private lazy var placesCache: DataCache = {
    return WWDataCache(name: "places", directory: cachesDirectory)
  }()
  private lazy var photoCache: PhotoCache = {
    return WWPhotoCache(directory: cachesDirectory)
  }()
  private lazy var contactService: ContactService = {
    return WWContactService(service: service)
  }()
  private lazy var postService: PostService = {
    return WWPostService(service: service, cache: postCache)
  }()
  private lazy var photoService: PhotoService = {
    return WWPhotoService(service: service, cache: photoCache)
  }()
  private lazy var placeService: PlaceService = {
    return WWPlaceService(service: service, cache: placesCache)
  }()
  private lazy var deviceService: DeviceService = {
    return WWDeviceService(service: service)
  }()
  
  private lazy var mainCoordinator: MainCoordinator = {
    return MainCoordinator(postService: postService,
                           photoService: photoService,
                           contactService: contactService,
                           placeService: placeService,
                           deviceService: deviceService,
                           cachesDirectory: cachesDirectory)
  }()
  
  init() {
    addChildCoordinator(mainCoordinator)
  }
  
  public func start() {
    GMSServices.provideAPIKey(Configuration.mapsKey)
    
    self.window.rootViewController = mainCoordinator.rootViewController
    self.window.makeKeyAndVisible()
  }
}
