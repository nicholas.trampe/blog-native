//
//  WebCoordinator.swift
//  blog
//
//  Created by Nicholas Trampe on 7/9/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol WebCoordinatorObserver: class {
  func didDismissWebCoordinator(_ coordinator: Coordinator)
}

class WebCoordinator: Coordinator {
  private let webViewController = WebViewController()
  private let transitioning = MenuItemTransitioning()
  
  weak var observer: WebCoordinatorObserver? = nil
  var childCoordinators: [Coordinator] = []
  var rootViewController: UIViewController {
    get {
      return webViewController
    }
  }
  
  init() {
    webViewController.observer = self
    webViewController.transitioningDelegate = transitioning
  }
  
  func load(url: URL, title: String) {
    webViewController.load(url: url, title: title)
  }
}

extension WebCoordinator: WebViewControllerObserver {
  func didPressCloseButton() {
    observer?.didDismissWebCoordinator(self)
  }
}
