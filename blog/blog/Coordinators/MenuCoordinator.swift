//
//  MenuCoordinator.swift
//  blog
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class MenuCoordinator: Coordinator {
  private let menuViewController: MenuViewController
  private let contactService: ContactService
  private let cachesDirectory: CacheDirectory
  
  var childCoordinators: [Coordinator] = []
  var rootViewController: UIViewController {
    get {
      return menuViewController
    }
  }
  
  init(contactService: ContactService, cachesDirectory: CacheDirectory) {
    menuViewController = MenuViewController()
    self.contactService = contactService
    self.cachesDirectory = cachesDirectory
    
    menuViewController.observer = self
  }
  
  func showAbout() {
    let aboutCoordinator = AboutCoordinator()
    aboutCoordinator.observer = self
    presentCoordinator(aboutCoordinator)
  }
  
  func showContact() {
    let contactCoordinator = ContactCoordinator(contactService: contactService)
    contactCoordinator.observer = self
    presentCoordinator(contactCoordinator)
  }
  
  func showSettings() {
    let settingsCoordinator = SettingsCoordinator(cachesDirectory: cachesDirectory)
    settingsCoordinator.observer = self
    presentCoordinator(settingsCoordinator)
  }
  
  func show(urlString: String, title: String) {
    if let url = URL(string: urlString) {
      let webCoordinator = WebCoordinator()
      webCoordinator.load(url: url, title: title)
      webCoordinator.observer = self
      presentCoordinator(webCoordinator)
    }
  }
}

extension MenuCoordinator: MenuViewControllerObserver {
  func didSelectItem(at index: Int) {
    switch index {
    case 0:
      showAbout()
    case 1:
      showContact()
    case 2:
      show(urlString: "https://www.instagram.com/nicholas.trampe", title: "Instagram")
    case 3:
      show(urlString: "https://www.facebook.com/nicholas.trampe", title: "Facebook")
    case 4:
      show(urlString: "https://www.couchsurfing.com/people/nicholas-trampe", title: "Couchsurfing")
    case 5:
      show(urlString: "https://gitlab.com/nicholas.trampe", title: "GitLab")
    case 6:
      showSettings()
    default:
      break
    }
  }
}

extension MenuCoordinator: AboutCoordinatorObserver {
  func didDismissAboutCoordinator(_ coordinator: Coordinator) {
    dismissCoordinator(coordinator)
  }
}

extension MenuCoordinator: ContactCoordinatorObserver {
  func didDismissContactCoordinator(_ coordinator: Coordinator) {
    dismissCoordinator(coordinator)
  }
}

extension MenuCoordinator: WebCoordinatorObserver {
  func didDismissWebCoordinator(_ coordinator: Coordinator) {
    dismissCoordinator(coordinator)
  }
}

extension MenuCoordinator: SettingsCoordinatorObserver {
  func didDismissSettingsCoordinator(_ coordinator: Coordinator) {
    dismissCoordinator(coordinator)
  }
}
