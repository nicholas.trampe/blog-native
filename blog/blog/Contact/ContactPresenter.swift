//
//  ContactPresenter.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol ContactPresenterObserver: class {
  func didClose()
  func didSend()
}

protocol ContactPresenter {
  var observer: ContactPresenterObserver? { get set }
  
  func start()
}

class WWContactPresenter: ContactPresenter {
  weak var observer: ContactPresenterObserver? = nil
  private var view: ContactView
  private var model: ContactModel
  
  init(view: ContactView, model: ContactModel) {
    self.view = view
    self.model = model
    
    self.view.observer = self
    self.model.observer = self
  }
  
  func start() {
    model.startEditingFields()
  }
}

extension WWContactPresenter: ContactViewObserver {  
  func didPressLeaveButton() {
    view.unfocus()
    observer?.didClose()
  }
  
  func didPressEnterButton(_ text: String) {
    model.submitFieldValue(text)
  }
  
  func didUpdateField() {
    view.focus()
  }
}

extension WWContactPresenter: ContactModelObserver {  
  func didMoveToNextField(field: ContactField) {
    view.unfocus()
    view.setHeading(field.heading)
    view.setError(nil)
    view.setKeyboardType(field.keyboardType)
    view.setCapitalizationType(field.capitalizationType)
    view.setReturnType(field.returnKeyType)
  }
  
  func didFailToSubmitField(field: ContactField) {
    view.setError(field.error)
  }
  
  func didFinishFields() {
    view.unfocus()
    view.setSpinning(true)
    model.send()
  }
  
  func didSendContact() {
    observer?.didSend()
  }
  
  func didFailToSendContact() {
    view.setSpinning(false)
  }
  
  func didUpdateKeyboardHeight(height: Float) {
    view.setBottomOffset(CGFloat(height))
  }
}
