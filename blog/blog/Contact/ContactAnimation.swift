//
//  ContactAnimation.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

enum ContactAnimationDirection {
  case up
  case down
  case left
  case right
}

class ContactAnimation {
  static func hide(with duration: TimeInterval, view: UIView, direction: ContactAnimationDirection, _ completion: (() -> ())? = nil) {
    var delay: TimeInterval = 0
    
    CATransaction.begin()
    CATransaction.setCompletionBlock(completion)
    
    for child in view.subviews {
      UIView.animate(withDuration: duration / 2.0, delay: delay, options: [], animations: {
        switch direction {
          case .up:
            child.transform = CGAffineTransform(translationX: 0, y: 20)
          case .down:
            child.transform = CGAffineTransform(translationX: 0, y: -20)
          case .left:
            child.transform = CGAffineTransform(translationX: 20, y: 0)
          case .right:
            child.transform = CGAffineTransform(translationX: -20, y: 0)
        }
      }, completion: { (finished) in
        UIView.animate(withDuration: duration / 2.0, animations: { 
          switch direction {
          case .up:
            child.transform = CGAffineTransform(translationX: 0, y: -view.frame.size.height - 100)
          case .down:
            child.transform = CGAffineTransform(translationX: 0, y: view.frame.size.height + 100)
          case .left:
            child.transform = CGAffineTransform(translationX: -view.frame.size.width - 100, y: 0)
          case .right:
            child.transform = CGAffineTransform(translationX: view.frame.size.width + 100, y: 0)
          }
        }, completion: { (finished) in
          
        })
      })
      
      delay += 0.1
    }
    
    CATransaction.commit()
  }
  
  static func show(with duration: TimeInterval, view: UIView, direction: ContactAnimationDirection, _ completion: (() -> ())? = nil) {
    var delay: TimeInterval = 0
    
    CATransaction.begin()
    CATransaction.setCompletionBlock(completion)
    
    for child in view.subviews {
      switch direction {
      case .up:
        child.transform = CGAffineTransform(translationX: 0, y: view.frame.size.height + 100)
      case .down:
        child.transform = CGAffineTransform(translationX: 0, y: -view.frame.size.height - 100)
      case .left:
        child.transform = CGAffineTransform(translationX: view.frame.size.width + 100, y: 0)
      case .right:
        child.transform = CGAffineTransform(translationX: -view.frame.size.width - 100, y: 0)
      }
    }
    for child in view.subviews {
      UIView.animate(withDuration: duration / 2.0, delay: delay, options: [], animations: { 
        switch direction {
        case .up:
          child.transform = CGAffineTransform(translationX: 0, y: -20)
        case .down:
          child.transform = CGAffineTransform(translationX: 0, y: 20)
        case .left:
          child.transform = CGAffineTransform(translationX: -20, y: 0)
        case .right:
          child.transform = CGAffineTransform(translationX: 20, y: 0)
        }
      }, completion: { (finished) in
        UIView.animate(withDuration: duration / 2.0, animations: { 
          child.transform = .identity
        }, completion: { (finished) in
          
        })
      })
      
      delay += 0.1
    }
    
    CATransaction.commit()
  }
}
