//
//  ContactView.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol ContactViewObserver: class {
  func didPressLeaveButton()
  func didPressEnterButton(_ text: String)
  func didUpdateField()
}

protocol ContactView {
  var observer: ContactViewObserver? { get set }
  
  func setSpinning(_ isSpinning: Bool)
  func setHeading(_ text: String)
  func setError(_ text: String?)
  func setKeyboardType(_ type: UIKeyboardType)
  func setCapitalizationType(_ type: UITextAutocapitalizationType)
  func setReturnType(_ type: UIReturnKeyType)
  
  func setBottomOffset(_ offset: CGFloat)
  
  func focus()
  func unfocus()
}

class WWContactView: UIView, ContactView {
  weak var observer: ContactViewObserver? = nil
  private let form = UIView()
  private let headingLabel = UILabel()
  private let errorLabel = UILabel()
  private let textView = ContactTextView()
  private let cancelButton = UIButton()
  private let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
  private lazy var bottomConstraint: NSLayoutConstraint = {
    return form.bottomAnchor.constraint(equalTo: self.bottomAnchor)
  }()
  
  init() {
    super.init(frame: .zero)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setSpinning(_ isSpinning: Bool) {
    if isSpinning {
      UIView.animate(withDuration: 0.3, animations: { 
        self.form.alpha = 0
      }) { (finished) in
        self.activityIndicator.alpha = 0
        self.activityIndicator.startAnimating()
        UIView.animate(withDuration: 0.5, delay: 0.2, options: [], animations: { 
          self.activityIndicator.alpha = 1
        }) { (finished) in
          
        }
      }
    } else {
      UIView.animate(withDuration: 0.3, animations: { 
        self.activityIndicator.alpha = 0
      }) { (finished) in
        self.activityIndicator.stopAnimating()
        UIView.animate(withDuration: 0.5, delay: 0.2, options: [], animations: { 
          self.form.alpha = 1
        }) { (finished) in
          
        }
      }
    }
  }
  
  func setHeading(_ text: String) {
    UIView.animate(withDuration: 0.5, animations: { 
      self.form.alpha = 0
    }) { (finished) in
      self.headingLabel.text = text
      self.textView.text = ""
      UIView.animate(withDuration: 0.5, delay: 0.2, options: [], animations: { 
        self.form.alpha = 1
      }) { (finished) in
        self.observer?.didUpdateField()
      }
    }
  }
  
  func setError(_ text: String?) {
    errorLabel.text = text
    
    if text != nil {
      UIView.animate(withDuration: 0.1, animations: { 
        self.errorLabel.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
      }, completion: { (finished) in
        UIView.animate(withDuration: 0.1, animations: { 
          self.errorLabel.transform = .identity
        })
      })
    }
  }
  
  func setKeyboardType(_ type: UIKeyboardType) {
    textView.keyboardType = type
  }
  
  func setCapitalizationType(_ type: UITextAutocapitalizationType) {
    textView.autocapitalizationType = type
  }
  
  func setReturnType(_ type: UIReturnKeyType) {
    textView.returnKeyType = type
  }
  
  func setBottomOffset(_ offset: CGFloat) {
    self.layoutIfNeeded()
    
    bottomConstraint.constant = -offset
    
    UIView.animate(withDuration: 0.3) { 
      self.layoutIfNeeded()
    }
  }
  
  func focus() {
    textView.becomeFirstResponder()
  }
  
  func unfocus() {
    textView.resignFirstResponder()
  }
  
  private func configureViews() {
    backgroundColor = .backgroundColor
    
    form.translatesAutoresizingMaskIntoConstraints = false
    form.alpha = 0
    addSubview(form)
    
    headingLabel.translatesAutoresizingMaskIntoConstraints = false
    headingLabel.numberOfLines = 0
    headingLabel.textAlignment = .center
    headingLabel.font = .openSansLight(of: 24)
    headingLabel.textColor = .foregroundColor
    form.addSubview(headingLabel)
    
    errorLabel.translatesAutoresizingMaskIntoConstraints = false
    errorLabel.numberOfLines = 0
    errorLabel.textAlignment = .center
    errorLabel.font = .openSans(of: 16)
    errorLabel.textColor = .red
    form.addSubview(errorLabel)
    
    textView.onNextPressed = {
      self.observer?.didPressEnterButton(self.textView.text)
    }
    form.addSubview(textView)
    
    cancelButton.translatesAutoresizingMaskIntoConstraints = false
    cancelButton.titleLabel?.font = .openSans(of: 14)
    cancelButton.setTitle("Actually, I don't want to talk to you!", for: .normal)
    cancelButton.setTitleColor(.foregroundColor, for: .normal)
    cancelButton.setTitleColor(.lightGray, for: .highlighted)
    cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
    form.addSubview(cancelButton)
    
    activityIndicator.translatesAutoresizingMaskIntoConstraints = false
    activityIndicator.color = .foregroundColor
    activityIndicator.isHidden = true
    addSubview(activityIndicator)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      form.leftAnchor.constraint(equalTo: self.leftAnchor),
      form.rightAnchor.constraint(equalTo: self.rightAnchor),
      form.topAnchor.constraint(equalTo: self.topAnchor),
      bottomConstraint,
      
      cancelButton.bottomAnchor.constraint(equalTo: form.bottomAnchor, constant: -10),
      cancelButton.centerXAnchor.constraint(equalTo: form.centerXAnchor),
      
      headingLabel.leftAnchor.constraint(equalTo: form.leftAnchor, constant: 20),
      headingLabel.rightAnchor.constraint(equalTo: form.rightAnchor, constant: -20),
      headingLabel.topAnchor.constraint(equalTo: form.topAnchor, constant: 40),
      
      errorLabel.leftAnchor.constraint(equalTo: form.leftAnchor, constant: 20),
      errorLabel.rightAnchor.constraint(equalTo: form.rightAnchor, constant: -20),
      errorLabel.topAnchor.constraint(equalTo: headingLabel.bottomAnchor, constant: 10),
      
      textView.leftAnchor.constraint(equalTo: form.leftAnchor, constant: 20),
      textView.rightAnchor.constraint(equalTo: form.rightAnchor, constant: -20),
      textView.topAnchor.constraint(equalTo: errorLabel.bottomAnchor, constant: 20),
      textView.bottomAnchor.constraint(equalTo: cancelButton.topAnchor, constant: -20),
      
      activityIndicator.centerXAnchor.constraint(equalTo: form.centerXAnchor),
      activityIndicator.centerYAnchor.constraint(equalTo: form.centerYAnchor),
      ])
  }
  
  @objc private func cancelButtonPressed() {
    observer?.didPressLeaveButton()
  }
}

private class ContactTextView: UITextView {
  var onTextChange: (String) -> () = {str in}
  var onNextPressed: () -> () = {}
  
  init() {
    super.init(frame: .zero, textContainer: nil)
    
    self.translatesAutoresizingMaskIntoConstraints = false
    self.font = .openSansLight(of: 22)
    self.autocorrectionType = .no
    self.returnKeyType = .next
    self.backgroundColor = .clear
    self.textColor = .foregroundColor
    self.delegate = self
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension ContactTextView: UITextViewDelegate {  
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    if text == "\n" {
      onNextPressed()
      return false
    }
    return true
  }
}
