//
//  ContactViewController.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol ContactViewControllerObserver: class {
  func didDismiss()
  func didSend()
}

class ContactViewController: UIViewController {
  private let contactView = WWContactView()
  private let contactModel: ContactModel
  private var presenter: ContactPresenter
  weak var observer: ContactViewControllerObserver? = nil
  
  init(contactService: ContactService) {
    contactModel = WWContactModel(contactService: contactService)
    presenter = WWContactPresenter(view: contactView, model: contactModel)
    super.init(nibName: nil, bundle: nil)
    presenter.observer = self
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func loadView() {
    view = contactView
  }
  
  override func viewDidAppear(_ animated: Bool) {
    presenter.start()
  }
}

extension ContactViewController: ContactPresenterObserver {
  func didClose() {
    observer?.didDismiss()
  }
  
  func didSend() {
    observer?.didSend()
  }
}
