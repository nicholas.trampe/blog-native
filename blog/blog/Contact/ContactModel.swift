//
//  ContactModel.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol ContactModelObserver: class {
  func didMoveToNextField(field: ContactField)
  func didFailToSubmitField(field: ContactField)
  func didFinishFields()
  func didSendContact()
  func didFailToSendContact()
  func didUpdateKeyboardHeight(height: Float)
}

protocol ContactModel {
  var observer: ContactModelObserver? { get set }
  
  func startEditingFields()
  func submitFieldValue(_ text: String)
  func send()
}

protocol ContactField {
  var value: String { get set }
  var heading: String { get }
  var error: String { get }
  var keyboardType: UIKeyboardType { get }
  var capitalizationType: UITextAutocapitalizationType { get }
  var returnKeyType: UIReturnKeyType { get }
  
  var isValid: Bool { get }
}

private struct WWContactTextField: ContactField {
  var value: String = ""
  let heading: String
  let error: String
  let keyboardType: UIKeyboardType
  let capitalizationType: UITextAutocapitalizationType
  let returnKeyType: UIReturnKeyType
  
  var isValid: Bool {
    get {
      return value != ""
    }
  }
}

private struct WWContactEmailField: ContactField {
  var value: String = ""
  let heading: String
  let error: String
  let keyboardType: UIKeyboardType
  let capitalizationType: UITextAutocapitalizationType
  let returnKeyType: UIReturnKeyType
  
  var isValid: Bool {
    get {
      let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
      
      if let _ = value.range(of: emailRegEx, options: .regularExpression) {
        return true
      }
      
      return false
    }
  }
}

class WWContactModel: ContactModel {
  weak var observer: ContactModelObserver? = nil
  private let contactService: ContactService
  private var index = 0
  private var fields: [ContactField] = [
    WWContactTextField(value: "",
                       heading: "I'm glad you would like to talk!\nWhat's your name?",
                       error: "Please enter your name",
                       keyboardType: .default,
                       capitalizationType: .words,
                       returnKeyType: .next),
    WWContactEmailField(value: "",
                        heading: "How can I reach you?",
                        error: "Please enter a valid email",
                        keyboardType: .emailAddress,
                        capitalizationType: .none,
                        returnKeyType: .next),
    WWContactTextField(value: "",
                       heading: "What would you like to talk about?",
                       error: "Please enter a message",
                       keyboardType: .default,
                       capitalizationType: .sentences,
                       returnKeyType: .send)
  ]
  
  init(contactService: ContactService) {
    self.contactService = contactService
    
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  func startEditingFields() {
    index = 0
    observer?.didMoveToNextField(field: fields[0])
  }
  
  func submitFieldValue(_ text: String) {
    fields[index].value = text
    if fields[index].isValid {
      if index < fields.count-1 {
        index += 1
        observer?.didMoveToNextField(field: fields[index])
      } else {
        observer?.didFinishFields()
      }
    } else {
      observer?.didFailToSubmitField(field: fields[index])
    }
  }
  
  func send() {
    contactService.contact(name: fields[0].value, email: fields[1].value, message: fields[2].value).then({ data in 
      self.observer?.didSendContact()
    }).catch({ error in 
      self.observer?.didFailToSendContact()
    })
  }
  
  @objc private func keyboardWillHide(_ notification: Notification) {
    observer?.didUpdateKeyboardHeight(height: 0)
  }
  
  @objc private func keyboardWillShow(_ notification: Notification) {
    if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
      let keyboardRectangle = keyboardFrame.cgRectValue
      let keyboardHeight = keyboardRectangle.height
      observer?.didUpdateKeyboardHeight(height: Float(keyboardHeight))
    }
  }
}
