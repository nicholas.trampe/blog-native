//
//  AboutViewController.swift
//  blog
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

private let aboutTexts = [
  "Hi, I'm Nick! 👋🏻",
  "I'm a software engineer, photographer, and adventurer!",
  "As a software engineer, I program in Swift, Obj-C, C++, Javascript, React, React Native, and Node.",
  "As a photographer, I immurse myself in nature, make time-lapses, and use long shutter speeds.",
  "As an adventurer, I strive to meet people that change my perspective about the world, experience cultures completely different from my own, and travel to places I've never been.",
  "Whether it's a new program, photo, or trip, I am always looking forward to the next adventure.",
  "I hope you enjoy my blog!",
]

protocol AboutViewControllerObserver: class {
  func didDismiss()
}

class AboutViewController: UIViewController {
  private let aboutView = WWAboutView()
  private let aboutModel = WWAboutModel(texts: aboutTexts)
  private lazy var presenter: AboutPresenter = {
    let presenter = WWAboutPresenter(view: aboutView, model: aboutModel)
    presenter.observer = self
    return presenter
  }()
  weak var observer: AboutViewControllerObserver? = nil
  
  override func loadView() {
    view = aboutView
  }
  
  override func viewDidAppear(_ animated: Bool) {
    presenter.start()
  }
}

extension AboutViewController: AboutPresenterObserver {
  func didDismiss() {
    observer?.didDismiss()
  }
}
