//
//  AboutView.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol AboutViewObserver: class {
  func didPressPreviousButton()
  func didPressNextButton()
}

protocol AboutView {
  var observer: AboutViewObserver? { get set }
  
  func setText(text: String)
}

class WWAboutView: UIView, AboutView {
  weak var observer: AboutViewObserver? = nil
  let label = UILabel()
  let previousButton = UIButton.iconButton(of: 32, with: .arrowLeft)
  let nextButton = UIButton.iconButton(of: 32, with: .arrowRight)
  
  init() {
    super.init(frame: .zero)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setText(text: String) {
    UIView.animate(withDuration: 0.5, animations: { 
      self.label.alpha = 0
    }) { (finished) in
      self.label.text = text
      UIView.animate(withDuration: 0.5, delay: 0.2, options: [], animations: { 
        self.label.alpha = 1
      }) { (finished) in
        
      }
    }
  }
  
  private func configureViews() {
    backgroundColor = .backgroundColor
    
    previousButton.setTitleColor(.foregroundColor, for: .normal)
    previousButton.setTitleColor(.lightGray, for: .highlighted)
    previousButton.addTarget(self, action: #selector(previousButtonPressed), for: .touchUpInside)
    addSubview(previousButton)
    
    nextButton.setTitleColor(.foregroundColor, for: .normal)
    nextButton.setTitleColor(.lightGray, for: .highlighted)
    nextButton.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
    addSubview(nextButton)
    
    label.translatesAutoresizingMaskIntoConstraints = false
    label.numberOfLines = 0
    label.textAlignment = .center
    label.font = .openSansLight(of: 24)
    label.textColor = .foregroundColor
    label.alpha = 0
    addSubview(label)
    
    let left = UISwipeGestureRecognizer(target: self, action: #selector(nextButtonPressed))
    left.direction = .left
    addGestureRecognizer(left)
    
    let right = UISwipeGestureRecognizer(target: self, action: #selector(previousButtonPressed))
    right.direction = .right
    addGestureRecognizer(right)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      previousButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
      previousButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20),
      
      nextButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20),
      nextButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20),
      
      label.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
      label.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20),
      label.topAnchor.constraint(equalTo: self.topAnchor, constant: 20),
      label.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20),
      ])
  }
  
  @objc private func previousButtonPressed() {
    observer?.didPressPreviousButton()
  }
  
  @objc private func nextButtonPressed() {
    observer?.didPressNextButton()
  }
}
