//
//  AboutModel.swift
//  blog
//
//  Created by Nicholas Trampe on 8/25/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol AboutModel {
  var texts: [String] { get }
  var previous: String? { get }
  var next: String? { get }
}

class WWAboutModel: AboutModel {
  private var index = 0
  let texts: [String]
  
  init(texts: [String]) {
    self.texts = texts
  }
  
  var previous: String? {
    get {
      if index > 0 {
        index -= 1
        return texts[index]
      } else {
        return nil
      }
    }
  }
  
  var next: String? {
    get {
      if index < texts.count-1 {
        index += 1
        return texts[index]
      } else {
        return nil
      }
    }
  }
}
