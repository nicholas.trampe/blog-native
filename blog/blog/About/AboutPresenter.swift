//
//  AboutPresenter.swift
//  blog
//
//  Created by Nicholas Trampe on 8/25/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol AboutPresenterObserver: class {
 func didDismiss()
}

protocol AboutPresenter {
  var observer: AboutPresenterObserver? { get set }
  func start()
}

class WWAboutPresenter: AboutPresenter {
  weak var observer: AboutPresenterObserver? = nil
  private var view: AboutView
  private let model: AboutModel
  
  init(view: AboutView, model: AboutModel) {
    self.view = view
    self.model = model
    
    self.view.observer = self
  }
  
  func start() {
    if model.texts.count > 0 {
      view.setText(text: model.texts[0])
    }
  }
}


extension WWAboutPresenter: AboutViewObserver {
  func didPressPreviousButton() {
    if let text = model.previous {
      view.setText(text: text)
    } else {
      observer?.didDismiss()
    }
  }
  
  func didPressNextButton() {
    if let text = model.next {
      view.setText(text: text)
    } else {
      observer?.didDismiss()
    }
  }
}
