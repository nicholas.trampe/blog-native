//
//  MenuItemPresentTransitioning.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

enum MenuItemTransitionViewTag: Int {
  case whiteness = 99
}

class MenuItemPresentTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.6
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard
      let fromVC = transitionContext.viewController(forKey: .from) as? MenuViewController,
      let toVC = transitionContext.viewController(forKey: .to)
      else {
        return
    }
    
    let containerView = transitionContext.containerView
    let duration = transitionDuration(using: transitionContext)
    let whiteness = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: containerView.frame.size.height))
    whiteness.tag = MenuItemTransitionViewTag.whiteness.rawValue
    whiteness.backgroundColor = .backgroundColor
    
    containerView.addSubview(whiteness)
    
    fromVC.view.frame = containerView.frame
    if UIDevice.versionUnder("13") {
      containerView.addSubview(fromVC.view)
    }
    
    toVC.view.frame = containerView.frame
    containerView.addSubview(toVC.view)
    
    toVC.view.alpha = 0
    
    fromVC.hide(with: duration / 2.0, nil)
    
    UIView.animate(withDuration: duration / 2.0,
                   delay: 0.1,
                   options: [.curveEaseInOut],
                   animations: {
                    whiteness.frame = containerView.frame
    }) { _ in
      UIView.animate(withDuration: duration / 2.0,
                     delay: 0.1,
                     options: [.curveEaseInOut],
                     animations: {
                      toVC.view.alpha = 1
      }) { _ in
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
      }
    }
  }
}
