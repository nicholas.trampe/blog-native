//
//  MenuItemDismissTransitioning.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class MenuItemDismissTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.6
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    let containerView = transitionContext.containerView
    guard
      let fromVC = transitionContext.viewController(forKey: .from),
      let toVC = transitionContext.viewController(forKey: .to) as? MenuViewController,
      let whiteness = containerView.viewWithTag(MenuItemTransitionViewTag.whiteness.rawValue)
      else {
        return
    }
    
    let duration = transitionDuration(using: transitionContext)
    
    toVC.view.frame = containerView.frame
    toVC.view.alpha = 0
    if UIDevice.versionUnder("13") {
      containerView.addSubview(toVC.view)
    }
    
    UIView.animate(withDuration: duration / 2.0,
                   delay: 0,
                   options: [.curveEaseInOut],
                   animations: {
                    fromVC.view.alpha = 0
    }) { _ in
      toVC.view.alpha = 1
      toVC.show(with: duration / 2.0, nil)
      UIView.animate(withDuration: duration / 2.0,
                     delay: 0,
                     options: [.curveEaseInOut],
                     animations: {
                      whiteness.frame = CGRect(x: 0, y: 0, width: 300, height: containerView.frame.size.height)
                      fromVC.view.alpha = 0
      }) { _ in
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        whiteness.removeFromSuperview()
      }
    }
  }
}
