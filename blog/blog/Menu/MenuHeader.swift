//
//  MenuHeader.swift
//  blog
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class MenuHeader: UIView {
  let image = UIImageView(image: UIImage(named: "header"))
  let label = UILabel()

  init() {
    super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 250))
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configureViews() {
    image.translatesAutoresizingMaskIntoConstraints = false
    image.contentMode = .scaleAspectFill
    addSubview(image)
    
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Worldwide\nWalkabouts"
    label.textColor = .foregroundColor
    label.numberOfLines = 2
    label.textAlignment = .center
    label.font = .openSansBold(of: 24)
    addSubview(label)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
        image.widthAnchor.constraint(equalToConstant: 75),
        image.heightAnchor.constraint(equalToConstant: 75),
        image.centerXAnchor.constraint(equalTo: self.centerXAnchor),
        image.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -30),
        
        label.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 20),
        label.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      ])
  }
}
