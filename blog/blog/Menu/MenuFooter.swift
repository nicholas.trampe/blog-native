//
//  MenuFooter.swift
//  blog
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class MenuFooter: UIView {
  let label = UILabel()
  
  init() {
    super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 100))
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configureViews() {
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "\u{00A9} 2017 Worldwide Walkabouts"
    label.textColor = .lightGray
    label.textAlignment = .center
    label.font = .openSansLight(of: 12)
    addSubview(label)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      label.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      label.centerYAnchor.constraint(equalTo: self.centerYAnchor),
      ])
  }
}
