//
//  MenuPresenter.swift
//  blog
//
//  Created by Nicholas Trampe on 8/18/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol MenuPresenterObserver: class {
  func didSelectItem(at index: Int)
}

protocol MenuPresenter {
  var observer: MenuPresenterObserver? { get set }
  func start()
}

class WWMenuPresenter: MenuPresenter {
  weak var observer: MenuPresenterObserver? = nil
  var view: MenuView
  var model: MenuModel
  let cellProvider: MenuCellProvider
  
  init(view: MenuView,
       model: MenuModel,
       cellProvider: MenuCellProvider) {
    self.view = view
    self.model = model
    self.cellProvider = cellProvider
    
    self.view.observer = self
    self.model.observer = self
  }
  
  func start() {
    self.cellProvider.items = self.model.items
    self.view.setCellProvider(cellProvider)
  }
}

extension WWMenuPresenter: MenuViewObserver {
  func didSelectItem(at indexPath: IndexPath) {
    model.selectItem(at: indexPath.row)
  }
}

extension WWMenuPresenter: MenuModelObserver {
  func didSelectItem(at index: Int) {
    observer?.didSelectItem(at: index)
  }
}
