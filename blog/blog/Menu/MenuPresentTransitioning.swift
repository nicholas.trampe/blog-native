//
//  MenuPresentTransitioning.swift
//  blog
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

enum MenuTransitionViewTag: Int {
  case blackness = 99
}

class MenuPresentTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.3
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard
      let fromVC = transitionContext.viewController(forKey: .from),
      let toVC = transitionContext.viewController(forKey: .to)
      else {
        return
    }
    
    let containerView = transitionContext.containerView
    
    let blackness = UIView()
    blackness.tag = MenuTransitionViewTag.blackness.rawValue
    blackness.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    
    blackness.frame = fromVC.view.frame
    containerView.addSubview(blackness)
    
    toVC.view.frame = containerView.frame
    containerView.addSubview(toVC.view)
    
    blackness.alpha = 0
    toVC.view.transform = CGAffineTransform(translationX: -400, y: 0)
    
    UIView.animate(withDuration: transitionDuration(using: transitionContext),
                   delay: 0,
                   options: [.curveEaseInOut],
                   animations: {
                    blackness.alpha = 1
                    toVC.view.transform = .identity
    }) { _ in
      transitionContext.completeTransition(true)
    }
  }
}
