//
//  MenuCellProvider.swift
//  blog
//
//  Created by Nicholas Trampe on 8/18/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit


protocol MenuCellProvider: CellProvider {
  var items: [MenuItem] { get set }
}

class WWMenuCellProvider: NSObject, MenuCellProvider {
  var items: [MenuItem] = []
  
  func registerCells(for tableView: UITableView) {
    tableView.register(MenuCell.self, forCellReuseIdentifier: MenuCell.defaultCellIdentifier)
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: MenuCell.defaultCellIdentifier, for: indexPath) as! MenuCell
    
    cell.titleLabel.text = items[indexPath.row].title
    cell.iconLabel.icon = items[indexPath.row].icon
    
    return cell
  }
}

