//
//  WebView.swift
//  blog
//
//  Created by Nicholas Trampe on 7/9/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol WebViewObserver: class {
  func didPressCloseButton()
}

protocol WebView {
  var observer: WebViewObserver? { get set }
  
  func setUrlRequest(_ urlRequest: URLRequest)
  func setTitle(_ title: String)
}

class WWWebView: UIView, WebView {
  weak var observer: WebViewObserver? = nil
  private let web = UIWebView()
  private let closeButton = UIButton.iconButton(of: FontAttributes.iconSmall.size, with: .cancel)
  private let backButton = UIButton.iconButton(of: FontAttributes.iconSmall.size, with: .arrowLeft)
  private let titleLabel = UILabel(frame: .zero)
  private let spinner = UIActivityIndicatorView(frame: .zero)
  
  init() {
    super.init(frame: .zero)
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: Public API
  
  func setUrlRequest(_ urlRequest: URLRequest) {
    web.loadRequest(urlRequest)
  }
  
  func setTitle(_ title: String) {
    titleLabel.text = title
  }
  
  // MARK: Private API
  
  @objc private func closeButtonPressed() {
    observer?.didPressCloseButton()
  }
  
  @objc private func backButtonPressed() {
    if web.canGoBack {
      web.goBack()
    } else {
      observer?.didPressCloseButton()
    }
  }
  
  private func configureViews() {
    
    backgroundColor = .clear
    
    spinner.translatesAutoresizingMaskIntoConstraints = false
    spinner.style = .whiteLarge
    spinner.color = .black
    spinner.startAnimating()
    addSubview(spinner)
    
    web.translatesAutoresizingMaskIntoConstraints = false
    web.backgroundColor = .clear
    web.isOpaque = false
    addSubview(web)
    
    closeButton.translatesAutoresizingMaskIntoConstraints = false
    closeButton.setTitleColor(.foregroundColor, for: .normal)
    closeButton.setTitleColor(.lightGray, for: .highlighted)
    closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
    addSubview(closeButton)
    
    backButton.translatesAutoresizingMaskIntoConstraints = false
    backButton.setTitleColor(.foregroundColor, for: .normal)
    backButton.setTitleColor(.lightGray, for: .highlighted)
    backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
    addSubview(backButton)
    
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.font = UIFont.openSansSemiBold(of: 18)
    titleLabel.textColor = .black
    addSubview(titleLabel)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      closeButton.topAnchor.constraint(equalTo: self.topAnchor, constant: UIApplication.shared.statusBarFrame.size.height + 10),
      closeButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20),
      
      backButton.topAnchor.constraint(equalTo: self.topAnchor, constant: UIApplication.shared.statusBarFrame.size.height + 10),
      backButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
      
      titleLabel.centerYAnchor.constraint(equalTo: backButton.centerYAnchor),
      titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      
      spinner.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      spinner.centerYAnchor.constraint(equalTo: self.centerYAnchor),
      
      web.topAnchor.constraint(equalTo: closeButton.bottomAnchor, constant: 10),
      web.bottomAnchor.constraint(equalTo: self.bottomAnchor),
      web.leftAnchor.constraint(equalTo: self.leftAnchor),
      web.rightAnchor.constraint(equalTo: self.rightAnchor),
      ])
  }
}
