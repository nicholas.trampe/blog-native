//
//  WebViewController.swift
//  blog
//
//  Created by Nicholas Trampe on 7/9/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

//TODO: Add a presenter and tests if this gets more complicated

import UIKit

protocol WebViewControllerObserver: class {
  func didPressCloseButton()
}

class WebViewController: UIViewController {
  weak var observer: WebViewControllerObserver? = nil
  private let webView = WWWebView()
  
  init() {
    super.init(nibName: nil, bundle: nil)
    
    webView.observer = self
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func loadView() {
    view = webView
  }
  
  func load(url: URL, title: String) {
    let urlRequest = URLRequest(url: url)
    webView.setUrlRequest(urlRequest)
    webView.setTitle(title)
  }
}

extension WebViewController: WebViewObserver {
  func didPressCloseButton() {
    observer?.didPressCloseButton()
  }
}
