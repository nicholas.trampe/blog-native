//
//  MenuDismissTransitioning.swift
//  blog
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class MenuDismissTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.3
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard
      let fromVC = transitionContext.viewController(forKey: .from),
      let blackness = transitionContext.containerView.viewWithTag(MenuTransitionViewTag.blackness.rawValue)
      else {
        return
    }
    
    UIView.animate(withDuration: transitionDuration(using: transitionContext),
                   delay: 0,
                   options: [.curveEaseInOut],
                   animations: {
                    blackness.alpha = 0
                    fromVC.view.transform = CGAffineTransform(translationX: -400, y: 0)
    }) { _ in
      transitionContext.completeTransition(true)
    }
  }
}
