//
//  MenuModel.swift
//  blog
//
//  Created by Nicholas Trampe on 8/18/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol MenuModelObserver: class {
  func didSelectItem(at index: Int)
}

struct MenuItem {
  let title: String
  let icon: IconFontIcon
}

extension MenuItem: Equatable {
  static func ==(lhs: MenuItem, rhs: MenuItem) -> Bool {
    return lhs.title == rhs.title && lhs.icon == rhs.icon
  }
}

protocol MenuModel {
  var observer: MenuModelObserver? { get set }
  var items: [MenuItem] { get }
  
  func selectItem(at index: Int)
}

class WWMenuModel: MenuModel {
  weak var observer: MenuModelObserver? = nil
  let items: [MenuItem] = [MenuItem(title: "About", icon: .info),
                           MenuItem(title: "Contact", icon: .mail),
                           MenuItem(title: "Instagram", icon: .instagram),
                           MenuItem(title: "Facebook", icon: .facebook),
                           MenuItem(title: "Couchsurfing", icon: .earth),
                           MenuItem(title: "GitLab", icon: .git),
                           MenuItem(title: "Settings", icon: .cog)]
  
  func selectItem(at index: Int) {
    observer?.didSelectItem(at: index)
  }
}
