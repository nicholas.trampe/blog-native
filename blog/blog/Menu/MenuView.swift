//
//  MenuView.swift
//  blog
//
//  Created by Nicholas Trampe on 8/18/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol MenuViewObserver: class {
  func didSelectItem(at indexPath: IndexPath)
}

protocol MenuView {
  var observer: MenuViewObserver? { get set }
  
  func setCellProvider(_ cellProvider: CellProvider)
}

class WWMenuView: UIView, MenuView, Animatable {
  weak var observer: MenuViewObserver? = nil
  private let tableView = UITableView()
  
  init() {
    super.init(frame: .zero)
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: Public API
  
  func setCellProvider(_ cellProvider: CellProvider) {
    cellProvider.registerCells(for: tableView)
    tableView.dataSource = cellProvider
  }
  
  func show(with duration: TimeInterval, _ completion: (() -> ())?) {
    let bounceDuration = duration * 0.25
    let runDuration = duration * 0.75
    
    UIView.animate(withDuration: runDuration, delay: 0, options: [], animations: { 
      self.tableView.tableHeaderView?.transform = CGAffineTransform(translationX: 0, y: 20)
      self.tableView.tableFooterView?.transform = CGAffineTransform(translationX: 0, y: -20)
      
      for cell in self.tableView.visibleCells {
        cell.transform = CGAffineTransform(translationX: 0, y: -20)
      }
    }) { (finished) in
      
    }
    
    UIView.animate(withDuration: bounceDuration, delay: runDuration, options: [], animations: { 
      self.tableView.tableHeaderView?.transform = .identity
      self.tableView.tableFooterView?.transform = .identity
      
      for cell in self.tableView.visibleCells {
        cell.transform = .identity
      }
    }) { (finished) in
      completion?()
    }
  }
  
  func hide(with duration: TimeInterval, _ completion: (() -> ())?) {    
    let bounceDuration = duration * 0.25
    let runDuration = duration * 0.75
    
    UIView.animate(withDuration: bounceDuration, delay: 0, options: [], animations: { 
      self.tableView.tableHeaderView?.transform = CGAffineTransform(translationX: 0, y: 20)
      self.tableView.tableFooterView?.transform = CGAffineTransform(translationX: 0, y: -20)
      
      for cell in self.tableView.visibleCells {
        cell.transform = CGAffineTransform(translationX: 0, y: -20)
      }
    }) { (finished) in
      
    }
    
    UIView.animate(withDuration: runDuration, delay: bounceDuration, options: [], animations: { 
      self.tableView.tableHeaderView?.transform = CGAffineTransform(translationX: 0, y: -300)
      self.tableView.tableFooterView?.transform = CGAffineTransform(translationX: 0, y: 500)
      
      for cell in self.tableView.visibleCells {
        cell.transform = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.size.height)
      }
    }) { (finished) in
      completion?()
    }
  }
  
  // MARK: Private API
  
  private func configureViews() {
    
    backgroundColor = .clear
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.backgroundColor = .backgroundColor
    tableView.separatorColor = UIColor.foregroundColor.withAlphaComponent(0.5)
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 50
    tableView.tableHeaderView = MenuHeader()
    tableView.tableFooterView = MenuFooter()
    tableView.layer.shadowOpacity = 0.7
    tableView.delegate = self
    addSubview(tableView)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      tableView.topAnchor.constraint(equalTo: self.topAnchor),
      tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
      tableView.leftAnchor.constraint(equalTo: self.leftAnchor),
      tableView.widthAnchor.constraint(equalToConstant: 300),
      ])
  }
}

extension WWMenuView: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    observer?.didSelectItem(at: indexPath)
  }
}
