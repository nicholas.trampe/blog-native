//
//  MenuInteractor.swift
//  blog
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class MenuPresentInteractor: UIPercentDrivenInteractiveTransition, Interactor {
  
  var hasStarted: Bool = false
  
  func interact(with gesture: UIPanGestureRecognizer,
                in view: UIView,
                presentationAction: () -> (),
                finishAction: () -> (),
                cancelAction: () -> ()) {
    let translation = Float(gesture.translation(in: view).x)
    let velocity = Float(gesture.velocity(in: view).x)
    let progress = CGFloat(0.25 + fmaxf(translation, 0) / 300)
    
    switch gesture.state {
    case .began:
      hasStarted = true
      presentationAction()
    case .changed:
      update(progress)
    case .cancelled:
      cancelAction()
      cancel()
    case .ended:
      hasStarted = false
      if progress > 0.4 || velocity > 1000 {
        finishAction()
        finish()
      } else {
        cancelAction()
        cancel()
      }
    default:
      break
    }
  }
}

class MenuDismissInteractor: UIPercentDrivenInteractiveTransition, Interactor {
  
  var hasStarted: Bool = false
  
  func interact(with gesture: UIPanGestureRecognizer,
                in view: UIView,
                presentationAction: () -> (),
                finishAction: () -> (),
                cancelAction: () -> ()) {
    let translation = Float(-gesture.translation(in: view).x)
    let velocity = Float(gesture.velocity(in: view).x)
    let progress = CGFloat(fmaxf(translation, 0) / 400)
    
    switch gesture.state {
    case .began:
      hasStarted = true
      presentationAction()
    case .changed:
      update(progress)
    case .cancelled:
      cancelAction()
      cancel()
    case .ended:
      hasStarted = false
      if progress > 0.4 || velocity < -1000 {
        finishAction()
        finish()
      } else {
        cancelAction()
        cancel()
      }
    default:
      break
    }
  }
}
