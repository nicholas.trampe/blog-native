//
//  MenuViewController.swift
//  blog
//
//  Created by Nicholas Trampe on 8/18/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol MenuViewControllerObserver: class {
  func didSelectItem(at index: Int)
}

class MenuViewController: UIViewController, Animatable {
  weak var observer: MenuViewControllerObserver? = nil
  private let menuView = WWMenuView()
  private let menuModel = WWMenuModel()
  private let cellProvider = WWMenuCellProvider()
  private lazy var presenter: MenuPresenter = {
    let presenter = WWMenuPresenter(view: menuView, model: menuModel, cellProvider: cellProvider)
    presenter.observer = self
    return presenter
  }()
  
  init() {
    super.init(nibName: nil, bundle: nil)
    
    modalPresentationStyle = .overFullScreen
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func loadView() {
    view = menuView
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    presenter.start()
  }
  
  func show(with duration: TimeInterval, _ completion: (() -> ())?) {
    menuView.show(with: duration, completion)
  }
  
  func hide(with duration: TimeInterval, _ completion: (() -> ())?) {
    menuView.hide(with: duration, completion)
  }
  
  override var shouldAutorotate: Bool {
    get {
      return false
    }
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    get {
      return .all
    }
  }
}

extension MenuViewController: MenuPresenterObserver {
  func didSelectItem(at index: Int) {
    observer?.didSelectItem(at: index)
  }
}
