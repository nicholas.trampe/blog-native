//
//  MenuCell.swift
//  blog
//
//  Created by Nicholas Trampe on 8/18/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
  
  let titleLabel = UILabel()
  let iconLabel = UILabel.iconLabel(of: 28, with: .facebook)
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: Private API
  
  private func configureViews() {
    backgroundColor = .clear
    contentView.backgroundColor = .clear
    
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.font = .openSans(of: 16)
    titleLabel.textColor = .foregroundColor
    contentView.addSubview(titleLabel)
    
    iconLabel.textColor = .foregroundColor
    contentView.addSubview(iconLabel)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
        titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
        titleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20),
        titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20),
        
        iconLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20),
        iconLabel.rightAnchor.constraint(equalTo: titleLabel.leftAnchor, constant: -10),
        iconLabel.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor)
      ])
  }
}
