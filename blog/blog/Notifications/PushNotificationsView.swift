//
//  PushNotificationsView.swift
//  blog
//
//  Created by Nicholas Trampe on 7/8/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PushNotificationsView {
  var observer: PushNotificationsViewObserver? { get set }
  
  func showSpinner()
  func hideSpinner()
  func showButtons()
  func hideButtons()
}

protocol PushNotificationsViewObserver: class {
  func didPressYesButton()
  func didPressNoButton()
}

class WWPushNotificationsView: UIView, PushNotificationsView {
  weak var observer: PushNotificationsViewObserver? = nil
  
  private let container = UIView(frame: .zero)
  private let iconLabel = UILabel.iconLabel(of: FontAttributes.iconLarge.size, with: .bullhorn)
  private let label = UILabel(frame: .zero)
  private let yesButton = UIButton(frame: .zero)
  private let noButton = UIButton(frame: .zero)
  private let spinner = UIActivityIndicatorView(frame: .zero)
  
  init() {
    super.init(frame: .zero)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  // Public API
  
  func showSpinner() {
    UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseInOut], animations: { 
      self.spinner.alpha = 1.0
    }, completion: nil)
  }
  
  func hideSpinner() {
    UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseInOut], animations: { 
      self.spinner.alpha = 0.0
    }, completion: nil)
  }
  
  func showButtons() {
    UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseInOut], animations: {
      self.yesButton.alpha = 1.0
      self.noButton.alpha = 1.0
    }, completion: nil)
  }
  
  func hideButtons() {
    UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseInOut], animations: { 
      self.yesButton.alpha = 0.0
      self.noButton.alpha = 0.0
    }, completion: nil)
  }
  
  // Private API
  
  private func configureViews() {
    backgroundColor = .clear
    
    container.translatesAutoresizingMaskIntoConstraints = false
    container.layer.cornerRadius = 25.0
    container.backgroundColor = .white
    addSubview(container)
    
    iconLabel.translatesAutoresizingMaskIntoConstraints = false
    iconLabel.textColor = .black
    container.addSubview(iconLabel)
    
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Would you like to receive notifications about new posts or places visited?"
    label.font = .openSansSemiBold(of: 20)
    label.textColor = .black
    label.numberOfLines = 0
    label.textAlignment = .center
    container.addSubview(label)
    
    yesButton.translatesAutoresizingMaskIntoConstraints = false
    yesButton.layer.cornerRadius = 15.0
    yesButton.setTitle("Of Course!", for: .normal)
    yesButton.backgroundColor = .oceanBlue
    yesButton.setTitleColor(.white, for: .normal)
    yesButton.setTitleColor(.gray, for: .highlighted)
    yesButton.titleLabel?.font = UIFont.openSans(of: 18)
    yesButton.addTarget(self, action: #selector(yesButtonPressed), for: .touchUpInside)
    container.addSubview(yesButton)
    
    noButton.translatesAutoresizingMaskIntoConstraints = false
    noButton.setTitle("No, Thanks", for: .normal)
    noButton.setTitleColor(.black, for: .normal)
    noButton.setTitleColor(.gray, for: .highlighted)
    noButton.titleLabel?.font = UIFont.openSansLight(of: 14)
    noButton.addTarget(self, action: #selector(noButtonPressed), for: .touchUpInside)
    container.addSubview(noButton)
    
    spinner.translatesAutoresizingMaskIntoConstraints = false
    spinner.style = .whiteLarge
    spinner.color = .black
    spinner.startAnimating()
    spinner.alpha = 0.0
    container.addSubview(spinner)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      container.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
      container.centerYAnchor.constraint(equalTo: self.centerYAnchor),
      container.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
      container.heightAnchor.constraint(lessThanOrEqualTo: self.heightAnchor, multiplier: 0.75),
      
      iconLabel.topAnchor.constraint(equalTo: container.topAnchor, constant: 20),
      iconLabel.centerXAnchor.constraint(equalTo: container.centerXAnchor),
      
      label.topAnchor.constraint(equalTo: iconLabel.bottomAnchor, constant: 20),
      label.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 20),
      label.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -20),
      
      noButton.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 20),
      noButton.centerXAnchor.constraint(equalTo: container.centerXAnchor),
      
      yesButton.topAnchor.constraint(equalTo: noButton.bottomAnchor, constant: 10),
      yesButton.centerXAnchor.constraint(equalTo: container.centerXAnchor),
      yesButton.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -20),
      yesButton.heightAnchor.constraint(equalToConstant: 50),
      yesButton.widthAnchor.constraint(equalToConstant: 140),
      
      spinner.centerXAnchor.constraint(equalTo: container.centerXAnchor),
      spinner.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -30),
      ])
  }
  
  @objc private func yesButtonPressed() {
    observer?.didPressYesButton()
  }
  
  @objc private func noButtonPressed() {
    observer?.didPressNoButton()
  }
}
