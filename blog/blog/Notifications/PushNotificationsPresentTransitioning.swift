//
//  PushNotificationsPresentTransitioning.swift
//  blog
//
//  Created by Nicholas Trampe on 7/8/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

enum PushNotificationsTransitionViewTag: Int {
  case blackness = 99
}

class PushNotificationsPresentTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.6
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard
      let fromVC = transitionContext.viewController(forKey: .from),
      let toVC = transitionContext.viewController(forKey: .to)
      else {
        return
    }
    
    let containerView = transitionContext.containerView
    
    UIGraphicsBeginImageContextWithOptions(fromVC.view.bounds.size, false, 0)
    fromVC.view.drawHierarchy(in:fromVC.view.bounds, afterScreenUpdates: false)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    let snapshot = UIImageView(image: image)
    let blackness = UIView()
    blackness.tag = PushNotificationsTransitionViewTag.blackness.rawValue
    blackness.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    
    snapshot.frame = fromVC.view.frame
    containerView.addSubview(snapshot)
    
    blackness.frame = fromVC.view.frame
    containerView.addSubview(blackness)
    
    toVC.view.frame = containerView.frame
    containerView.addSubview(toVC.view)
    
    blackness.alpha = 0
    toVC.view.transform = CGAffineTransform(translationX: 0, y: toVC.view.frame.size.height)
    
    UIView.animate(withDuration: transitionDuration(using: transitionContext),
                   delay: 0,
                   usingSpringWithDamping: 0.6,
                   initialSpringVelocity: 0.5,
                   options: [],
                   animations: { 
                    blackness.alpha = 1
                    toVC.view.transform = .identity
    }) { completed in
      transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
    }
  }
}
