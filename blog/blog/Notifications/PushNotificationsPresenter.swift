//
//  PushNotificationsPresenter.swift
//  blog
//
//  Created by Nicholas Trampe on 7/8/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol PushNotificationsPresenter {
  var observer: PushNotificationsPresenterObserver? { get set }
}

protocol PushNotificationsPresenterObserver: class {
  func didDismiss()
}

class WWPushNotificationsPresenter: PushNotificationsPresenter {
  weak var observer: PushNotificationsPresenterObserver? = nil
  private var view: PushNotificationsView
  private var model: PushNotificationModel
  
  init(view: PushNotificationsView, model: PushNotificationModel) {
    self.view = view
    self.model = model
    
    self.model.observer = self
    self.view.observer = self
  }
}

extension WWPushNotificationsPresenter: PushNotificationModelObserver {
  func didRegisterDevice() {
    model.isRegistered = true
    observer?.didDismiss()
  }
  
  func didFailToRegisterDevice(error: Error) {
    model.isRegistered = true
    observer?.didDismiss()
  }
  
  func didReceiveNotification(info: [String : Any]) {
    
  }
}

extension WWPushNotificationsPresenter: PushNotificationsViewObserver {
  func didPressYesButton() {
    view.hideButtons()
    view.showSpinner()
    model.requestAuthorization()
    
    // This is a necessary workaround for the fact that you can't detect if the user selects 'Don't Allow'
    // when asked to allow notifications
    // NO tests around this
    weak var weakSelf = self
    DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
      weakSelf?.observer?.didDismiss()
    })
  }
  
  func didPressNoButton() {
    model.isRegistered = true
    observer?.didDismiss()
  }
}
