//
//  PushNotificationModel.swift
//  blog
//
//  Created by Nicholas Trampe on 7/7/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit
import UserNotifications

extension Notification.Name {
  static let didRegisterForRemoteNotificationsWithDeviceToken = Notification.Name("didRegisterForRemoteNotificationsWithDeviceToken")
  static let didFailToRegisterForRemoteNotificationsWithError = Notification.Name("didFailToRegisterForRemoteNotificationsWithError")
  static let didReceiveRemoteNotification = Notification.Name("didReceiveRemoteNotification")
}

protocol PushNotificationModel {
  var observer: PushNotificationModelObserver? { get set }
  var isRegistered: Bool { get set }
  
  func requestAuthorization()
}

protocol PushNotificationModelObserver: class {
  func didRegisterDevice()
  func didFailToRegisterDevice(error: Error)
  func didReceiveNotification(info: [String:Any])
}

class WWPushNotificationModel: PushNotificationModel {
  private let registeredKey = "RegisteredForPushNotifications"
  weak var observer: PushNotificationModelObserver? = nil
  private let deviceService: DeviceService
  
  var isRegistered: Bool {
    get {
      return UserDefaults.standard.bool(forKey: registeredKey)
    }
    
    set {
      UserDefaults.standard.set(newValue, forKey: registeredKey)
    }
  }
  
  init(deviceService: DeviceService) {
    self.deviceService = deviceService
    
    NotificationCenter.default.addObserver(self, selector: #selector(didRegister(notification:)), name: .didRegisterForRemoteNotificationsWithDeviceToken, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(didFailToRegister(notification:)), name: .didFailToRegisterForRemoteNotificationsWithError, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(didReceiveNotification(notification:)), name: .didReceiveRemoteNotification, object: nil)
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  // Public API
  
  func requestAuthorization() {
    UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .alert, .sound], completionHandler: {(granted, error) in
      DispatchQueue.main.async {
        UIApplication.shared.registerForRemoteNotifications()
      }
    })
  }
  
  // Private API
  
  @objc private func didRegister(notification: Notification) {
    guard let userInfo = notification.userInfo,
          let token = userInfo["deviceToken"] as? Data else { return }
    
    let deviceTokenString = token.reduce("", {$0 + String(format: "%02X", $1)})
    deviceService.register(token: deviceTokenString).then({ data in 
      self.observer?.didRegisterDevice()
    }).catch({ error in 
      self.observer?.didFailToRegisterDevice(error: error)
    })
  }
  
  @objc private func didFailToRegister(notification: Notification) {
    guard let userInfo = notification.userInfo,
          let error = userInfo["error"] as? Error else { return }
    observer?.didFailToRegisterDevice(error: error)
  }
  
  @objc private func didReceiveNotification(notification: Notification) {
    guard let userInfo = notification.userInfo,
          let aps = userInfo["aps"] as? [String:Any] else { return }
    print("Received push notification: \(userInfo)")
    observer?.didReceiveNotification(info: aps)
  }
}
