//
//  PushNotificationsDismissTransitioning.swift
//  blog
//
//  Created by Nicholas Trampe on 7/8/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

class PushNotificationsDismissTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.3
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard
      let fromVC = transitionContext.viewController(forKey: .from),
      let blackness = transitionContext.containerView.viewWithTag(PushNotificationsTransitionViewTag.blackness.rawValue)
      else {
        return
    }
    
    UIView.animate(withDuration: transitionDuration(using: transitionContext),
                   delay: 0,
                   options: [.curveEaseInOut],
                   animations: {
                    blackness.alpha = 0
                    fromVC.view.transform = CGAffineTransform(translationX: 0, y: fromVC.view.frame.size.height)
    }) { _ in
      transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
    }
  }
}

