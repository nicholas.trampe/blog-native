//
//  PushNotificationsViewController.swift
//  blog
//
//  Created by Nicholas Trampe on 7/8/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PushNotificationsViewControllerObserver: class {
  func didDismiss()
}

class PushNotificationsViewController: UIViewController {
  weak var observer: PushNotificationsViewControllerObserver? = nil
  private let notificationsView = WWPushNotificationsView()
  private let presenter: WWPushNotificationsPresenter
  
  init(deviceService: DeviceService) {
    let model = WWPushNotificationModel(deviceService: deviceService)
    presenter = WWPushNotificationsPresenter(view: notificationsView, model: model)
    super.init(nibName: nil, bundle: nil)
    
    presenter.observer = self
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func loadView() {
    view = notificationsView
  }
}


extension PushNotificationsViewController: PushNotificationsPresenterObserver {
  func didDismiss() {
    observer?.didDismiss()
  }
}
