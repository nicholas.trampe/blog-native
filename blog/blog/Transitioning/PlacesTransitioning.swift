//
//  PlacesTransitioning.swift
//  blog
//
//  Created by Nicholas Trampe on 9/11/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class PlacesTransitioning: NSObject {
  
}

extension PlacesTransitioning: UIViewControllerTransitioningDelegate {
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return PlacesPresentTransitioning()
  }
  
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return PlacesDismissTransitioning()
  }
}
