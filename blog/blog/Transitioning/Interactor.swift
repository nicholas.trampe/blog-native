//
//  Interactor.swift
//  blog
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol Interactor {
  var hasStarted: Bool { get }
  func interact(with gesture: UIPanGestureRecognizer,
                in view: UIView,
                presentationAction: () -> (),
                finishAction: () -> (),
                cancelAction: () -> ())
}
