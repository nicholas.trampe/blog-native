//
//  PostTransitioning.swift
//  blog
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PostTransitioningDelegate: class {
  func dismiss()
  func didDismiss()
}

class PostTransitioning: NSObject {
  weak var delegate: PostTransitioningDelegate? = nil
  let postPresentTransition = PostPresentTransitioning()
  let postDismissTransition = PostDismissTransitioning()
  let postDismissInteractor = PostDismissInteractor()
  private weak var viewController: UIViewController? = nil
  
  init(originFrame: CGRect, viewController: UIViewController) {
    super.init()
    postPresentTransition.originFrame = originFrame
    postDismissTransition.finalFrame = originFrame
    self.viewController = viewController
  }
}

extension PostTransitioning: UIViewControllerTransitioningDelegate {
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return postPresentTransition
  }
  
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return postDismissTransition
  }
  
  func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    return postDismissInteractor.hasStarted ? postDismissInteractor : nil
  }
}
