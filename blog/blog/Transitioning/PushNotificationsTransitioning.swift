//
//  PushNotificationsTransitioning.swift
//  blog
//
//  Created by Nicholas Trampe on 7/8/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

class PushNotificationsTransitioning: NSObject {
  
}

extension PushNotificationsTransitioning: UIViewControllerTransitioningDelegate {
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return PushNotificationsPresentTransitioning()
  }
  
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return PushNotificationsDismissTransitioning()
  }
}
