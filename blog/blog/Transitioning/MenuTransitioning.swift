//
//  MenuTransitioning.swift
//  blog
//
//  Created by Nicholas Trampe on 8/19/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol MenuTransitioningDelegate: class {
  func present()
  func dismiss()
  func didDismiss()
}

class MenuTransitioning: NSObject {
  weak var delegate: MenuTransitioningDelegate? = nil
  private let menuPresentTransition = MenuPresentTransitioning()
  private let menuDismissTransition = MenuDismissTransitioning()
  private let menuPresentInteractor = MenuPresentInteractor()
  private let menuDismissInteractor = MenuDismissInteractor()
  private weak var presentingViewController: UIViewController? = nil
  private weak var presentedViewController: UIViewController? = nil
  
  func setPresentingViewController(_ viewController: UIViewController) {
    presentingViewController = viewController
    
    let edgeGesture = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(handlePresentPan(_:)))
    edgeGesture.edges = .left
    viewController.view.addGestureRecognizer(edgeGesture)
  }
  
  func setPresentedViewController(_ viewController: UIViewController) {
    presentedViewController = viewController
    
    let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handleDismissPan(_:)))
    viewController.view.addGestureRecognizer(panGesture)
  }
  
  @objc private func handlePresentPan(_ sender: UIScreenEdgePanGestureRecognizer) {
    if let viewController = presentingViewController {
      menuPresentInteractor.interact(with: sender, in: viewController.view, presentationAction: { 
        self.delegate?.present()
      }, finishAction: { 
        
      }, cancelAction: { 
        self.delegate?.didDismiss()
      })
    }
  }
  
  @objc private func handleDismissPan(_ sender: UIPanGestureRecognizer) {
    if let viewController = presentedViewController {
      menuDismissInteractor.interact(with: sender, in: viewController.view, presentationAction: { 
        self.delegate?.dismiss()
      }, finishAction: { 
        self.delegate?.didDismiss()
      }, cancelAction: { 
        
      })
    }
  }
}

extension MenuTransitioning: UIViewControllerTransitioningDelegate {
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return menuPresentTransition
  }
  
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return menuDismissTransition
  }
  
  func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    return menuPresentInteractor.hasStarted ? menuPresentInteractor : nil
  }
  
  func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    return menuDismissInteractor.hasStarted ? menuDismissInteractor : nil
  }
}
