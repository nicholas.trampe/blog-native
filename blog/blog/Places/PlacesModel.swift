//
//  PlacesModel.swift
//  blog
//
//  Created by Nicholas Trampe on 9/10/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

struct Location {
  let latitude: Double
  let longitude: Double
}

protocol PlacesModel {
  var places: [Place] { get }
  var startingLocation: Location { get }
  var observer: PlacesModelObserver? { get set }
  
  func updatePlaces()
  func loadPhotos(for placeID: String)
}

protocol PlacesModelObserver: class {
  func didUpdatePlaces(places: [Place])
  func didFailToUpdateUpdatePlaces(error: Error)
  
  func didLoad(photo: UIImage, for place: Place)
  func didFailToLoadPhotos(for place: Place, error: Error)
}

class WWPlacesModel: PlacesModel {
  private let placeService: PlaceService
  private let photoService: PhotoService
  weak var observer: PlacesModelObserver? = nil
  var places: [Place] = []
  var startingLocation: Location = Location(latitude: 38.6270025, longitude: -90.1994042)
  
  init(placeService: PlaceService, photoService: PhotoService) {
    self.placeService = placeService
    self.photoService = photoService
  }
  
  func updatePlaces() {
    placeService.get().then({ places in 
      self.places = places
      self.observer?.didUpdatePlaces(places: places)
    }).catch({ error in
      self.observer?.didFailToUpdateUpdatePlaces(error: error)
    })
  }
  
  func loadPhotos(for placeID: String) {
    guard let place = places.first(where: { $0._id == placeID }) else {
      return
    }
    
    place.photos?.forEach { photo in
      self.photoService.get(id: photo._id).then({ image in
        self.observer?.didLoad(photo: image, for: place)
      }).catch({ error in
        self.observer?.didFailToLoadPhotos(for: place, error: error)
      })
    }
  }
}
