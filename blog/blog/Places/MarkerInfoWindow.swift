//
//  MarkerInfoWindow.swift
//  blog
//
//  Created by Nicholas Trampe on 6/24/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

class MarkerInfoWindow: UIView {
  private let headingLabel = UILabel(frame: .zero)
  private let dateLabel = UILabel(frame: .zero)
  private let textView = UITextView(frame: .zero)
  
  var text: String? {
    get {
      return textView.text
    }
    
    set {
      textView.text = newValue
      updateFrame()
      setNeedsUpdateConstraints()
    }
  }
  
  var heading: String? {
    get {
      return headingLabel.text
    }
    
    set {
      headingLabel.text = newValue
      updateFrame()
      setNeedsUpdateConstraints()
    }
  }
  
  var date: String? {
    get {
      return dateLabel.text
    }
    
    set {
      dateLabel.text = newValue
      updateFrame()
      setNeedsUpdateConstraints()
    }
  }
  
  init() {
    super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width - 40, height: 240))
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func updateFrame() {
    let width = min(UIScreen.main.bounds.size.width - 40, 350)
    let sizeToFit = CGSize(width: width - 20, height: 0)
    let height =  textView.sizeThatFits(sizeToFit).height +
                  headingLabel.sizeThatFits(sizeToFit).height +
                  dateLabel.sizeThatFits(sizeToFit).height + 20
    self.frame = CGRect(x: 0, y: 0, width: width, height: height)
  }
  
  private func configureViews() {
    backgroundColor = .white
    
    textView.translatesAutoresizingMaskIntoConstraints = false
    textView.isUserInteractionEnabled = true
    textView.isScrollEnabled = true
    textView.isEditable = false
    textView.textColor = .black
    textView.font = .openSans(of: 14)
    addSubview(textView)
    
    headingLabel.translatesAutoresizingMaskIntoConstraints = false
    headingLabel.textColor = .black
    headingLabel.font = .openSansBold(of: 18)
    headingLabel.numberOfLines = 2
    headingLabel.textAlignment = .center
    addSubview(headingLabel)
    
    dateLabel.translatesAutoresizingMaskIntoConstraints = false
    dateLabel.textColor = .black
    dateLabel.font = .openSansSemiBold(of: 14)
    dateLabel.numberOfLines = 1
    dateLabel.textAlignment = .center
    addSubview(dateLabel)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      headingLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
      headingLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
      headingLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10),
      
      dateLabel.topAnchor.constraint(equalTo: headingLabel.bottomAnchor, constant: 0),
      dateLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
      dateLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10),
      
      textView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 5),
      textView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5),
      textView.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 0),
      textView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5),
      ])
  }
}
