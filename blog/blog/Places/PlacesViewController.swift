//
//  PlacesViewController.swift
//  blog
//
//  Created by Nicholas Trampe on 9/10/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PlacesViewControllerObserver: class {
  func didPressCloseButton()
}

class PlacesViewController: UIViewController {
  weak var observer: PlacesViewControllerObserver? = nil
  private let placesView = WWPlacesView()
  private let presenter: WWPlacesPresenter
  
  init(placeService: PlaceService, photoService: PhotoService) {
    let model = WWPlacesModel(placeService: placeService, photoService: photoService)
    presenter = WWPlacesPresenter(view: placesView, model: model)
    super.init(nibName: nil, bundle: nil)
    
    presenter.observer = self

    modalPresentationStyle = .fullScreen
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func loadView() {
    view = placesView
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    presenter.start()
  }

  override var preferredStatusBarStyle: UIStatusBarStyle {
    if #available(iOS 13.0, *) {
      return .darkContent
    }
    
    return .default
  }
}


extension PlacesViewController: PlacesPresenterObserver {
  func didPressCloseButton() {
    observer?.didPressCloseButton()
  }
}
