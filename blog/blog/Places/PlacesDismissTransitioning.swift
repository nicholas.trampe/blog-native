//
//  PlacesDismissTransitioning.swift
//  blog
//
//  Created by Nicholas Trampe on 9/11/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class PlacesDismissTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.5
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard
      let fromVC = transitionContext.viewController(forKey: .from),
      let toVC = transitionContext.viewController(forKey: .to)
      else {
        return
    }
    
    let topOffset = 12 + UIApplication.shared.statusBarFrame.size.height
    let containerView = transitionContext.containerView
    let screenSize = UIScreen.main.bounds.size
    let originRect = CGRect(x: -screenSize.height / 2.0, y: -screenSize.height / 2.0, width: screenSize.height * 2.0, height: screenSize.height * 2.0)
    let finalRect = CGRect(x: screenSize.width - 30, y: topOffset, width: 1, height: 1)
    
    let originPath = CGMutablePath()
    originPath.addEllipse(in: originRect)
    
    let originMask = CAShapeLayer()
    originMask.path = originPath
    originMask.fillRule = CAShapeLayerFillRule.evenOdd
    
    let finalPath = CGMutablePath()
    finalPath.addEllipse(in: finalRect)
    
    fromVC.view.layer.mask = originMask
    
    toVC.view.frame = containerView.frame
    containerView.insertSubview(toVC.view, at: 0)
    
    let anim = CABasicAnimation(keyPath: "path")
    anim.fromValue = originPath
    anim.toValue = finalPath
    anim.duration = transitionDuration(using: transitionContext)
    anim.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    
    originMask.add(anim, forKey: nil)
    
    CATransaction.begin()
    CATransaction.setDisableActions(true)
    originMask.path = finalPath
    CATransaction.commit()
    
    DispatchQueue.main.asyncAfter(deadline: .now() + transitionDuration(using: transitionContext)) { 
      transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
      fromVC.view.layer.mask = nil
    }
  }
}
