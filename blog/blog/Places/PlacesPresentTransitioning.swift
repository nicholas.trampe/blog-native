//
//  PlacesPresentTransitioning.swift
//  blog
//
//  Created by Nicholas Trampe on 9/11/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class PlacesPresentTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.5
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard
      let fromVC = transitionContext.viewController(forKey: .from),
      let toVC = transitionContext.viewController(forKey: .to)
      else {
        return
    }
    
    let topOffset = 12 + UIApplication.shared.statusBarFrame.size.height
    let containerView = transitionContext.containerView
    let screenSize = UIScreen.main.bounds.size
    let originRect = CGRect(x: screenSize.width - 30, y: topOffset, width: 1, height: 1)
    let finalRect = CGRect(x: -screenSize.height / 2.0, y: -screenSize.height / 2.0, width: screenSize.height * 2.0, height: screenSize.height * 2.0)
    
    UIGraphicsBeginImageContextWithOptions(fromVC.view.bounds.size, false, 0)
    fromVC.view.drawHierarchy(in:fromVC.view.bounds, afterScreenUpdates: false)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    let snapshot = UIImageView(image: image)
    
    let originPath = CGMutablePath()
    originPath.addEllipse(in: originRect)
    
    let originMask = CAShapeLayer()
    originMask.path = originPath
    originMask.fillRule = CAShapeLayerFillRule.evenOdd
    
    let finalPath = CGMutablePath()
    finalPath.addEllipse(in: finalRect)
    
    snapshot.frame = fromVC.view.frame
    containerView.addSubview(snapshot)
    
    toVC.view.frame = fromVC.view.frame
    toVC.view.layer.mask = originMask
    containerView.addSubview(toVC.view)
    
    let anim = CABasicAnimation(keyPath: "path")
    anim.fromValue = originPath
    anim.toValue = finalPath
    anim.duration = transitionDuration(using: transitionContext)
    anim.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    
    originMask.add(anim, forKey: nil)
    
    CATransaction.begin()
    CATransaction.setDisableActions(true)
    originMask.path = finalPath
    CATransaction.commit()
        
    DispatchQueue.main.asyncAfter(deadline: .now() + transitionDuration(using: transitionContext)) { 
      transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
      snapshot.removeFromSuperview()
      toVC.view.layer.mask = nil
    }
  }
}
