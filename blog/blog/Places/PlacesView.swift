//
//  PlacesView.swift
//  blog
//
//  Created by Nicholas Trampe on 9/10/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PlacesViewObserver: class {
  func didPressCloseButton()
  func didTap(marker: Marker)
  func didCloseMarker()
  
  func didCloseContentView()
}

protocol PlacesView: MapView {
  var observer: PlacesViewObserver? { get set }
  
  func showContentView()
  func updateContentView(heading: String?, subheading: String?, text: String?)
  func insert(image: UIImage)
  func removeAllImages()
  func hideContentView()
}

class WWPlacesView: UIView, PlacesView {
  var isGestureInteractionEnabled: Bool = true
  
  var zoom: Float {
    get {
      return map.zoom
    }
  }
  
  var latitude: Double {
    get {
      return map.latitude
    }
  }
  
  var longitude: Double {
    get {
      return map.longitude
    }
  }
  
  weak var observer: PlacesViewObserver? = nil
  private let closeButton = UIButton.iconButton(of: 24, with: .earth)
  private let contentView = PlaceContentView()
  private let map = WWMapView()
  
  init() {
    super.init(frame: .zero)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func addMarker(marker: Marker) {
    map.addMarker(marker: marker)
  }
  
  func moveCamera(latitude: Double, longitude: Double, zoom: Float, animated: Bool) {
    map.moveCamera(latitude: latitude, longitude: longitude, zoom: zoom, animated: animated)
  }
  
  func removeFocus() {
    map.removeFocus()
  }
  
  func showContentView() {
    contentView.peek()
  }
  
  func hideContentView() {
    contentView.hide()
  }
  
  func updateContentView(heading: String?, subheading: String?, text: String?) {
    contentView.heading = heading
    contentView.subheading = subheading
    contentView.text = text
  }
  
  func insert(image: UIImage) {
    contentView.insert(image: image)
  }
  
  func removeAllImages() {
    contentView.removeAll()
  }
  
  @objc private func closeButtonPressed() {
    observer?.didPressCloseButton()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    contentView.expandedHeightTopPadding = closeButton.center.y + closeButton.frame.size.height / 2.0 + 10
  }
  
  private func configureViews() {
    map.translatesAutoresizingMaskIntoConstraints = false
    map.observer = self
    addSubview(map)
    
    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.accessibilityLabel = "Places Content View"
    contentView.observer = self
    contentView.peekPadding = 30
    addSlideUpView(contentView)
    
    closeButton.accessibilityLabel = "Close Places Button"
    closeButton.setTitleColor(.black, for: .normal)
    closeButton.setTitleColor(.lightGray, for: .highlighted)
    closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
    addSubview(closeButton)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      map.leftAnchor.constraint(equalTo: self.leftAnchor),
      map.rightAnchor.constraint(equalTo: self.rightAnchor),
      map.topAnchor.constraint(equalTo: self.topAnchor),
      map.bottomAnchor.constraint(equalTo: self.bottomAnchor),
      
      closeButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 5 + UIApplication.shared.statusBarFrame.size.height),
      closeButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10),
      ])
  }
}

extension WWPlacesView: MapViewObserver {
  func didTap(marker: Marker) {
    observer?.didTap(marker: marker)
  }
  
  func didCloseMarker() {
    observer?.didCloseMarker()
  }
}

extension WWPlacesView: SlideUpContentViewObserver {
  func didPeek() {
    
  }
  
  func didExpand() {
    
  }
  
  func didSit() {
    
  }
  
  func didHide() {
    observer?.didCloseContentView()
  }
}
