//
//  PlacesPresenter.swift
//  blog
//
//  Created by Nicholas Trampe on 9/10/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PlacesPresenterObserver: class {
  func didPressCloseButton()
}

protocol PlacesPresenter {
  var observer: PlacesPresenterObserver? { get set }
  
  func start()
}

class WWPlacesPresenter: PlacesPresenter {
  weak var observer: PlacesPresenterObserver? = nil
  private var view: PlacesView
  private var model: PlacesModel
  
  init(view: PlacesView, model: PlacesModel) {
    self.view = view
    self.model = model
    
    self.model.observer = self
    self.view.observer = self
    
    self.view.moveCamera(latitude: self.model.startingLocation.latitude,
                         longitude: self.model.startingLocation.longitude,
                         zoom: 3,
                         animated: false)
  }
  
  func start() {
    model.updatePlaces()
  }
}

extension WWPlacesPresenter: PlacesModelObserver {
  func didUpdatePlaces(places: [Place]) {
    for place in places {
      let marker = Marker(id: place._id, title: place.name, description: place.description, date: place.date, latitude: place.latitude, longitude: place.longitude)
      view.addMarker(marker: marker)
    }
  }
  
  func didFailToUpdateUpdatePlaces(error: Error) {
    
  }
  
  func didLoad(photo: UIImage, for place: Place) {
    view.insert(image: photo)
  }
  
  func didFailToLoadPhotos(for place: Place, error: Error) {
    
  }
}

extension WWPlacesPresenter: PlacesViewObserver {
  func didPressCloseButton() {
    observer?.didPressCloseButton()
  }
  
  func didTap(marker: Marker) {
    view.moveCamera(latitude: marker.latitude,
                    longitude: marker.longitude,
                    zoom: 5,
                    animated: true)
    view.removeAllImages()
    view.updateContentView(heading: marker.title, subheading: marker.date, text: marker.description)
    view.showContentView()
    model.loadPhotos(for: marker.id)
  }
  
  func didCloseMarker() {
    view.hideContentView()
    view.moveCamera(latitude: view.latitude,
                    longitude: view.longitude,
                    zoom: 3,
                    animated: true)
  }
  
  func didCloseContentView() {
    view.removeFocus()
    view.moveCamera(latitude: view.latitude,
                    longitude: view.longitude,
                    zoom: 3,
                    animated: true)
  }
}
