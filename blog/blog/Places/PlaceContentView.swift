//
//  PlaceContentView.swift
//  blog
//
//  Created by Nicholas Trampe on 8/1/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

class PlaceContentView: SlideUpContentView, PhotosCollection {
  private let contentPadding: CGFloat = 15
  
  private let headingLabel = UILabel(frame: .zero)
  private let subheadingLabel = UILabel(frame: .zero)
  private let textView = UILabel(frame: .zero)
  private var photosHeight: NSLayoutConstraint!
  private let photosCollection = WWPhotosCollection()
  
  var text: String? {
    get {
      return textView.text
    }
    
    set {
      textView.text = newValue
      setNeedsUpdateConstraints()
    }
  }
  
  var heading: String? {
    get {
      return headingLabel.text
    }
    
    set {
      headingLabel.text = newValue
      setNeedsUpdateConstraints()
    }
  }
  
  var subheading: String? {
    get {
      return subheadingLabel.text
    }
    
    set {
      subheadingLabel.text = newValue
      setNeedsUpdateConstraints()
    }
  }
  
  override init() {
    super.init()
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func insert(image: UIImage) {
    photosCollection.insert(image: image)
    
    photosHeight.constant = photosCollection.height
    setNeedsUpdateConstraints()
  }
  
  func removeAll() {
    photosCollection.removeAll()
    
    photosHeight.constant = 0
    setNeedsUpdateConstraints()
  }
  
  private func configureViews() {
    textView.translatesAutoresizingMaskIntoConstraints = false
    textView.textColor = .black
    textView.numberOfLines = 0
    textView.font = .openSans(of: 14)
    peekView.addSubview(textView)
    
    headingLabel.translatesAutoresizingMaskIntoConstraints = false
    headingLabel.textColor = .black
    headingLabel.font = .openSansBold(of: 18)
    headingLabel.numberOfLines = 2
    headingLabel.textAlignment = .center
    peekView.addSubview(headingLabel)
    
    subheadingLabel.translatesAutoresizingMaskIntoConstraints = false
    subheadingLabel.textColor = .black
    subheadingLabel.font = .openSansSemiBold(of: 14)
    subheadingLabel.numberOfLines = 1
    subheadingLabel.textAlignment = .center
    peekView.addSubview(subheadingLabel)
    
    photosCollection.translatesAutoresizingMaskIntoConstraints = false
    photosCollection.isScrollEnabled = false
    contentView.addSubview(photosCollection)
  }
  
  private func constrainViews() {
    photosHeight = photosCollection.heightAnchor.constraint(equalToConstant: 200)
    
    NSLayoutConstraint.activate([
      headingLabel.topAnchor.constraint(equalTo: peekView.topAnchor),
      headingLabel.leftAnchor.constraint(equalTo: peekView.leftAnchor),
      headingLabel.rightAnchor.constraint(equalTo: peekView.rightAnchor),
      
      subheadingLabel.topAnchor.constraint(equalTo: headingLabel.bottomAnchor),
      subheadingLabel.leftAnchor.constraint(equalTo: peekView.leftAnchor),
      subheadingLabel.rightAnchor.constraint(equalTo: peekView.rightAnchor),
      
      textView.topAnchor.constraint(equalTo: subheadingLabel.bottomAnchor),
      textView.leftAnchor.constraint(equalTo: peekView.leftAnchor),
      textView.rightAnchor.constraint(equalTo: peekView.rightAnchor),
      textView.bottomAnchor.constraint(equalTo: peekView.bottomAnchor),
      
      photosCollection.topAnchor.constraint(equalTo: contentView.topAnchor),
      photosCollection.leftAnchor.constraint(equalTo: contentView.leftAnchor),
      photosCollection.rightAnchor.constraint(equalTo: contentView.rightAnchor),
      photosCollection.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
      photosHeight,
      ])
    
    setNeedsUpdateConstraints()
  }
}

private class PullTab: UIView {
  init() {
    super.init(frame: .zero)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configureViews() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = .lightGray
    layer.cornerRadius = 2.0
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      widthAnchor.constraint(equalToConstant: 50),
      heightAnchor.constraint(equalToConstant: 5),
      ])
  }
}
