//
//  MapView.swift
//  blog
//
//  Created by Nicholas Trampe on 9/11/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit
import GoogleMaps

struct Marker {
  let id: String
  let title: String?
  let description: String?
  let date: String?
  let latitude: Double
  let longitude: Double
}

extension Marker: Equatable {
  public static func == (lhs: Marker, rhs: Marker) -> Bool {
    return  lhs.title == rhs.title &&
            lhs.description == rhs.description &&
            lhs.date == rhs.date &&
            lhs.latitude == rhs.latitude &&
            lhs.longitude == rhs.longitude
  }
}

protocol MapViewObserver: class {
  func didTap(marker: Marker)
  func didCloseMarker()
}

protocol MapView {
  var isGestureInteractionEnabled: Bool { get set }
  
  var zoom: Float { get }
  var latitude: Double { get }
  var longitude: Double { get }
  
  func addMarker(marker: Marker)
  func moveCamera(latitude: Double, longitude: Double, zoom: Float, animated: Bool)
  func removeFocus()
}

class WWMapView: UIView, MapView {
  weak var observer: MapViewObserver? = nil
  
  private let map = GMSMapView()
  private var currentMarker: GMSMarker? = nil
  
  var isGestureInteractionEnabled: Bool {
    get {
      return map.settings.scrollGestures || map.settings.rotateGestures || map.settings.zoomGestures
    }
    
    set {
      map.settings.setAllGesturesEnabled(newValue)
    }
  }
  
  var zoom: Float {
    get {
      return map.camera.zoom
    }
  }
  
  var latitude: Double {
    get {
      return map.camera.target.latitude
    }
  }
  
  var longitude: Double {
    get {
      return map.camera.target.longitude
    }
  }
  
  init() {
    super.init(frame: .zero)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func addMarker(marker: Marker) {
    let gms = GMSMarker(position: CLLocationCoordinate2D(latitude: marker.latitude, longitude: marker.longitude))    
    gms.title = marker.title
    gms.userData = ["marker": marker]
    gms.appearAnimation = .pop
    gms.map = map
    gms.icon = UIImage(named: "flag.png")
    gms.accessibilityLabel = "Marker \(marker.title ?? "")"
  }
  
  func moveCamera(latitude: Double, longitude: Double, zoom: Float, animated: Bool) {
    let update = GMSCameraUpdate.setTarget(CLLocationCoordinate2D(latitude: latitude, longitude: longitude), zoom: zoom) 
    
    if animated {
      map.animate(with: update)
    } else {
      map.moveCamera(update)
    }
  }
  
  func removeFocus() {
    map.selectedMarker = nil
  }
  
  func fitCamera(latitude: Double, longitude: Double, bound: Double) {
    let update = GMSCameraUpdate.fit(GMSCoordinateBounds(coordinate: CLLocationCoordinate2D(latitude: latitude - bound, longitude: longitude - bound),
                                                         coordinate: CLLocationCoordinate2D(latitude: latitude + bound, longitude: longitude + bound)))
    map.animate(with: update)
  }
  
  private func configureViews() {
    map.translatesAutoresizingMaskIntoConstraints = false
    map.accessibilityElementsHidden = false
    map.delegate = self
    addSubview(map)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      map.leftAnchor.constraint(equalTo: self.leftAnchor),
      map.rightAnchor.constraint(equalTo: self.rightAnchor),
      map.topAnchor.constraint(equalTo: self.topAnchor),
      map.bottomAnchor.constraint(equalTo: self.bottomAnchor),
      ])
  }
}

extension WWMapView: GMSMapViewDelegate { 
  func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    guard let userData = (marker.userData as? [String:Any]),
          let placeMarker = (userData["marker"] as? Marker) else { return false }
    
    mapView.selectedMarker = marker
    currentMarker = marker
    observer?.didTap(marker: placeMarker)
    return true
  }
  
  func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
    if mapView.selectedMarker == nil && currentMarker != nil {
      observer?.didCloseMarker()
      currentMarker = nil
    }
  }
}
