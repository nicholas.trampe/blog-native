//
//  ForceTouchGestureRecognizer.swift
//  blog
//
//  Created by Nicholas Trampe on 6/22/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass

class ForceTouchGestureRecognizer: UIGestureRecognizer {
  var force: CGFloat = 0
  var threshold: CGFloat = 0.2
  var distance: CGPoint {
    get {
      return CGPoint(x: currentLocation.x - initialLocation.x, y: currentLocation.y - initialLocation.y)
    }
  }
  
  private var initialLocation: CGPoint = .zero
  private var currentLocation: CGPoint = .zero
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
    guard let touch = touches.first else { return }
    setForce(with: touch)
    
    if force > threshold {
      state = .began
      initialLocation = touch.location(in: nil)
    }
  }
  
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
    guard let touch = touches.first else { return }
    setForce(with: touch)
    
    if force > threshold {
      state = .changed
      
      currentLocation = touch.location(in: nil)
      if initialLocation == .zero {
        initialLocation = currentLocation
      }
    }
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
    guard let touch = touches.first else { return }
    setForce(with: touch)
    state = .ended
    initialLocation = .zero
  }
  
  private func setForce(with touch: UITouch) {
    force = touch.maximumPossibleForce > 0 ? touch.force / touch.maximumPossibleForce : 0
  }
}
