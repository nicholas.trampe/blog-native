//
//  Service.swift
//  blog
//
//  Created by Nicholas Trampe on 8/12/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import Foundation
import Promises

enum ServiceError: Error {
  case invalidUrl
  case invalidData
  case serverError(message: String)
  case sessionError(reason: String)
  
  var localizedDescription: String {
    get {
      return errorDescription ?? "Unknown error"
    }
  }
}

extension ServiceError: LocalizedError {
  var errorDescription: String? {
    switch self {
    case .invalidUrl:
      return "The URL provided is invalid"
    case .invalidData:
      return "The data retrieved is invalid"
    case .serverError(let message):
      return message
    case .sessionError(let reason):
      return reason
    }
  }
}

extension ServiceError: Equatable {
  public static func == (lhs: ServiceError, rhs: ServiceError) -> Bool {
    return lhs.errorDescription == rhs.errorDescription
  }
}

protocol Service {
  var baseURL: String { get }
  
  func get(route: String) -> Promise<Data>
  func post(route: String, body: Any) -> Promise<Data>
  func authenticate(username: String, password: String) -> Promise<Void>
}
