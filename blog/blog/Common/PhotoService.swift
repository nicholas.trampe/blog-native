//
//  PhotoService.swift
//  blog
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit
import Promises

struct Photo: Codable {
  let _id: String
  let filename: String
  let uploaded: Date
}

enum ResultImageType {
  case success(image: UIImage)
  case failure(error: ServiceError)
}

enum ResultImageIndexType {
  case success(image: UIImage, index: Int)
  case failure(error: ServiceError, index: Int)
}

typealias PhotoServiceImageBlock = (ResultImageType) -> ()
typealias PhotoServiceImageIndexBlock = (ResultImageIndexType) -> ()

protocol PhotoService {
  func get(id: String) -> Promise<UIImage>
}

class WWPhotoService: PhotoService {
  private let service: Service
  private var cache: PhotoCache
  
  init(service: Service, cache: PhotoCache) {
    self.service = service
    self.cache = cache
  }
  
  func get(id: String) -> Promise<UIImage> {
    return Promise<UIImage>({ resolve, reject in 
      self.cache.loadPhoto(for: id).then({ image in 
        resolve(image)
      }).recover({ error -> Promise<UIImage> in
        self.fetch(id: id)
      }).then({ image in 
        resolve(image)
      }).catch({ error in 
        reject(error)
      })
    })
  }
  
  private func fetch(id: String) -> Promise<UIImage> {
    return Promise<UIImage>({ resolve, reject in
      self.service.get(route: "photos/" + id).then({ data in
        return self.cache.savePhoto(for: id, data: data)
      }).then({ image in 
        resolve(image)
      }).catch({ error in
        reject(error)
      })
    })
  }
}

