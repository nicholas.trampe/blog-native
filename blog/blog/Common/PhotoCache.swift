//
//  PhotoCache.swift
//  blog
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit
import Promises

enum PhotoCacheError: Error {
  case invalidData
  case invalidPath
  case invalidID
}

protocol PhotoCache {
  func loadPhoto(for id: String) -> Promise<UIImage>
  func savePhoto(for id: String, data: Data) -> Promise<UIImage>
}

class WWPhotoCache: PhotoCache {
  private var cache = InMemoryCache()
  private let directory: CacheDirectory
  
  init(directory: CacheDirectory) {
    self.directory = directory
  }
  
  func loadPhoto(for id: String) -> Promise<UIImage> {
    return Promise<UIImage>({ resolve, reject in
      if let image = self.load(id: id) {
        resolve(image)
      } else {
        reject(PhotoCacheError.invalidID)
      }
    })
  }
  
  func savePhoto(for id: String, data: Data) -> Promise<UIImage> {
    return Promise<UIImage>({ resolve, reject in 
      guard let path = self.imagePath(for: id) else {
        reject(PhotoCacheError.invalidPath)
        return
      }
      
      let fm = FileManager.default
      if fm.fileExists(atPath: path) {
        try! fm.removeItem(atPath: path)
      }
      
      if let image = UIImage(data: data) {
        fm.createFile(atPath: path, contents: data, attributes: nil)
        self.cache[id] = image
        resolve(image)
      } else {
        reject(PhotoCacheError.invalidData)
      }
    })
  }
  
  private func load(id: String) -> UIImage? {
    if let image = cache[id] {
      return image
    }
    
    if let path = imagePath(for: id) {
      if let image = UIImage(contentsOfFile: path) {
        cache[id] = image
        return image
      }
    }
    return nil
  }
  
  private func imagePath(for id: String) -> String? {
    return directory.url(for: id)?.path
  }
}

private class InMemoryCache {
  private var cache = NSCache<NSString,DiscardableImage>()
  
  init() {
    cache.name = "WWPhotoCache"
  }
  
  subscript(id: String) -> UIImage? {
    get {
      return cache.object(forKey: id as NSString)?.image
    }
    set(newValue) {
      if let image = newValue {
        cache.setObject(DiscardableImage(image: image), forKey: id as NSString)
      } else {
        cache.removeObject(forKey: id as NSString)
      }
    }
  }
}

private class DiscardableImage: NSObject, NSDiscardableContent {
  private var storedImage: UIImage? = nil
  private var counter: UInt8 = 0
  
  init(image: UIImage) {
    storedImage = image
  }
  
  var image: UIImage? {
    get {
      return storedImage
    }
  }
  
  func beginContentAccess() -> Bool {
    if storedImage == nil {
      return false
    }
    
    counter += 1
    
    return true
  }
  
  func endContentAccess() {
    if counter > 0 {
      counter -= 1
    }
  }
  
  func discardContentIfPossible() {
    if counter == 0 {
      storedImage = nil
    }
  }
  
  func isContentDiscarded() -> Bool {
    return storedImage == nil
  }
}
