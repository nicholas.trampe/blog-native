//
//  ContactService.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import Foundation
import Promises

protocol ContactService {
  func contact(name: String, email: String, message: String) -> Promise<Data>
}

class WWContactService: ContactService {
  private let service: Service
  
  init(service: Service) {
    self.service = service
  }
  
  func contact(name: String, email: String, message: String) -> Promise<Data> {
    let body: [String:String] = ["name": name,
                                 "email": email,
                                 "message": message]
    return service.post(route: "deets", body: body)
  }
}

