//
//  Logo.swift
//  blog
//
//  Created by Nicholas Trampe on 8/31/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

class Logo: UIView {
  private let size = CGSize(width: 210, height: 210)
  
  init() {
    super.init(frame: .zero)
    
    configureLayers()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func animate() {
    let length = 250
    let animation = CABasicAnimation(keyPath: "position.x")
    animation.duration = 15
    animation.isRemovedOnCompletion = false
    animation.fillMode = CAMediaTimingFillMode.forwards
    animation.repeatCount = 1000
    animation.autoreverses = true
    
    animation.fromValue = length
    animation.toValue = -length
    row1Layer.add(animation, forKey: "position.x")
    
    animation.fromValue = -length
    animation.toValue = length
    row2Layer.add(animation, forKey: "position.x")
    
    animation.fromValue = length
    animation.toValue = -length
    row3Layer.add(animation, forKey: "position.x")
    
    animation.fromValue = -length
    animation.toValue = length
    row4Layer.add(animation, forKey: "position.x")
    
    animation.fromValue = length
    animation.toValue = -length
    row5Layer.add(animation, forKey: "position.x")
  }
  
  private func configureLayers() {
    isUserInteractionEnabled = false
    translatesAutoresizingMaskIntoConstraints = false
    
    layer.addSublayer(borderLayer)
    borderLayer.addSublayer(oceanLayer)
    oceanLayer.addSublayer(row1Layer)
    oceanLayer.addSublayer(row2Layer)
    oceanLayer.addSublayer(row3Layer)
    oceanLayer.addSublayer(row4Layer)
    oceanLayer.addSublayer(row5Layer)
    
    oceanLayer.mask = maskLayer
    
    borderLayer.bounds.origin = CGPoint(x: (size.width / 2.0), y: (size.height / 2.0))
  }
  
  private lazy var borderLayer: CAShapeLayer = {
    let border = CAShapeLayer()
    border.path = CGPath(ellipseIn: CGRect(x: 0, y: 0, width: size.width, height: size.height), transform: nil)
    border.strokeColor = UIColor.borderPurple.cgColor
    border.lineWidth = 15
    return border
  }()
  
  private lazy var oceanLayer: CAShapeLayer = {
    let ocean = CAShapeLayer()
    ocean.path = CGPath(ellipseIn: CGRect(x: 0, y: 0, width: size.width, height: size.height), transform: nil)
    ocean.fillColor = UIColor.oceanBlue.cgColor
    return ocean
  }()
  
  private lazy var maskLayer: CAShapeLayer = {
    let mask = CAShapeLayer()
    mask.path = CGPath(ellipseIn: CGRect(x: 0, y: 0, width: size.width, height: size.height), transform: nil)
    mask.fillRule = CAShapeLayerFillRule.evenOdd
    return mask
  }()
  
  private lazy var row1Layer: CATextLayer = {
    return textLayer(with: "101101010110011011101010100100110101101010110011011101010100100110", color: UIColor.iceWhite, yPosition: -10)
  }()
  
  private lazy var row2Layer: CATextLayer = {
    return textLayer(with: "100101010011010111010010101100010100101010011010111010010101100010", color: UIColor.landGreen, yPosition: 34)
  }()
  
  private lazy var row3Layer: CATextLayer = {
    return textLayer(with: "101101001101011010110101010101101101101001101011010110101010101101", color: UIColor.landGreen, yPosition: 79)
  }()
  
  private lazy var row4Layer: CATextLayer = {
    return textLayer(with: "100110110001101011011001110010101100110110001101011011001110010101", color: UIColor.landGreen, yPosition: 125)
  }()
  
  private lazy var row5Layer: CATextLayer = {
    return textLayer(with: "101011010010101011010010101101010101011010010101011010010101101010", color: UIColor.iceWhite, yPosition: 170)
  }()
  
  private func textLayer(with text: String, color: UIColor, yPosition: CGFloat) -> CATextLayer {
    let font = CGFont("OpenSans-Bold" as CFString)
    let row = CATextLayer()
    row.string = text
    row.font = font
    row.fontSize = 36
    row.frame = CGRect(x: 0, y: yPosition, width: 1000, height: 100)
    row.foregroundColor = color.cgColor
    row.contentsScale = UIScreen.main.nativeScale
    row.masksToBounds = false
    return row
  }
}
