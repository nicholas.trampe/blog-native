//
//  HapticGenerator.swift
//  blog
//
//  Created by Nicholas Trampe on 7/5/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol HapticGenerator {
  func prepare()
  func generate()
}

class WWHapticGenerator: HapticGenerator {
  private lazy var feedbackGenerator = UIImpactFeedbackGenerator()
  
  func prepare() {
    feedbackGenerator.prepare()
  }
  
  func generate() {
    feedbackGenerator.impactOccurred()
  }
}
