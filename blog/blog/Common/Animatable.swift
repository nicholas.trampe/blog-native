//
//  Animatable.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol Animatable {
  func show(with duration: TimeInterval, _ completion: (() -> ())?)
  func hide(with duration: TimeInterval, _ completion: (() -> ())?)
}
