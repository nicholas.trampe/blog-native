//
//  PostService.swift
//  blog
//
//  Created by Nicholas Trampe on 8/12/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import Foundation
import Promises

struct Post: Codable {
  
  struct BodyComponent: Codable {
    let _id: String
    let type: String
    let content: String
  }
  
  let _id: String
  let heading: String
  let subheading: String
  let cover: String
  let date: Date
  let body: [BodyComponent]
}

extension Post: Equatable {
  static func ==(lhs: Post, rhs: Post) -> Bool {
    return lhs._id == rhs._id
  }
}

protocol PostService {
  func get() -> Promise<[Post]>
}

class WWPostService: PostService {
  private let service: Service
  private let cache: DataCache
  
  init(service: Service, cache: DataCache) {
    self.service = service
    self.cache = cache
  }
  
  func get() -> Promise<[Post]> {
    return Promise<[Post]>({ resolve, reject in
      self.service.get(route: "posts").then({ data in 
        return self.cache.saveData(data: data)
      }).recover({ error -> Promise<Data> in
        return self.cache.loadData()
      }).then({ data in 
        return self.decode(data: data)
      }).then({ places in 
        resolve(places)
      }).catch({ error in 
        reject(error)
      })
    })
  }
  
  private func decode(data: Data) -> Promise<[Post]> {
    return Promise<[Post]>({ resolve, reject in
      let decoder = JSONDecoder()
      decoder.dateDecodingStrategy = .formatted(.jsonDateFormatter)
      
      do {
        let posts = try decoder.decode([Post].self, from: data)
        resolve(posts)
      } catch {
        reject(error)
      }
    })
  }
  
  
}
