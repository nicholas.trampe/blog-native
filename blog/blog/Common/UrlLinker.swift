//
//  UrlLinker.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol UrlLinker {
  func linkToInstagram()
  func linkToFacebook()
  func linkToCouchsurfing()
  func linkToGitLab()
}

class WWUrlLinker: UrlLinker {
  func linkToInstagram() {
    let webUrl = URL(string: "https://www.instagram.com/nicholas.trampe")!
    let deepUrl = URL(string: "instagram://user?username=nicholas.trampe")!
    openUrl(webUrl, deepUrl)
  }
  
  func linkToFacebook() {
    let webUrl = URL(string: "https://www.facebook.com/nicholas.trampe")!
    openUrl(webUrl)
  }
  
  func linkToCouchsurfing() {
    let webUrl = URL(string: "https://www.couchsurfing.com/people/nicholas-trampe")!
    openUrl(webUrl)
  }
  
  func linkToGitLab() {
    let webUrl = URL(string: "https://gitlab.com/nicholas.trampe")!
    openUrl(webUrl)
  }
  
  private func openUrl(_ webUrl: URL, _ deepUrl: URL? = nil) {
    if let deepUrl = deepUrl {
      if UIApplication.shared.canOpenURL(deepUrl) {
        UIApplication.shared.open(deepUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
      } else {
        UIApplication.shared.open(webUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
      }
    } else {
      UIApplication.shared.open(webUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
    }
  }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
