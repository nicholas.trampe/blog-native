//
//  DeviceService.swift
//  blog
//
//  Created by Nicholas Trampe on 7/7/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation
import Promises

protocol DeviceService {
  func register(token: String) -> Promise<Data>
}

class WWDeviceService: DeviceService {
  private let service: Service
  
  init(service: Service) {
    self.service = service
  }
  
  func register(token: String) -> Promise<Data> {
    let body: [String:String] = ["notificationToken": token]
    return service.post(route: "registerDevice", body: body)
  }
}

