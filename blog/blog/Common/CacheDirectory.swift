//
//  CacheDirectory.swift
//  blog
//
//  Created by Nicholas Trampe on 8/6/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol CacheDirectory {
  var url: URL? { get }
  var items: [String] { get }
  
  func url(for item: String) -> URL?
  func clear()
}

struct WWCacheDirectory: CacheDirectory {
  var url: URL? {
    get {
      let fm = FileManager.default
      if  let cachePath = fm.urls(for: .cachesDirectory, in: .userDomainMask).first?.path,
        let bundleIdentifier = Bundle.main.bundleIdentifier {
        let path = "\(cachePath)/\(bundleIdentifier)/"
        return URL(fileURLWithPath: path, isDirectory: true)
      }
      return nil
    }
  }
  
  var items: [String] {
    get {
      guard let cacheURL = url else {
        return []
      }
      
      do {
        let fm = FileManager.default
        return try fm.contentsOfDirectory(atPath: cacheURL.path)
      } catch {
        return []
      }
    }
  }
  
  func url(for item: String) -> URL? {
    if let cacheURL = url {
      return cacheURL.appendingPathComponent(item)
    }
    return nil
  }
  
  func clear() {
    let fm = FileManager.default
    
    for item in items {
      if let itemURL = url(for: item) {
        if fm.fileExists(atPath: itemURL.path) {
          do {
            try FileManager.default.removeItem(at: itemURL)
          } catch {
            print("WWCacheDirectory: Error clearing data\n\(error)")
          }
        }
      }
    }
  }
}
