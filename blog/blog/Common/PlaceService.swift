//
//  PlaceService.swift
//  blog
//
//  Created by Nicholas Trampe on 9/10/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import Foundation
import Promises

struct Place: Codable {
  let _id: String
  let name: String
  let description: String
  let latitude: Double
  let longitude: Double
  let date: String
  let photos: [Photo]?
}

extension Place: Equatable {
  static func ==(lhs: Place, rhs: Place) -> Bool {
    return lhs._id == rhs._id
  }
}

protocol PlaceService {
  func get() -> Promise<[Place]>
}

class WWPlaceService: PlaceService {
  private let service: Service
  private let cache: DataCache
  
  init(service: Service, cache: DataCache) {
    self.service = service
    self.cache = cache
  }
  
  func get() -> Promise<[Place]> {
    return Promise<[Place]>({ resolve, reject in
      self.service.get(route: "places").then({ data in 
        return self.cache.saveData(data: data)
      }).recover({ error -> Promise<Data> in
        return self.cache.loadData()
      }).then({ data in 
        return self.decode(data: data)
      }).then({ places in 
        resolve(places)
      }).catch({ error in 
        reject(error)
      })
    })
  }
  
  private func decode(data: Data) -> Promise<[Place]> {
    return Promise<[Place]>({ resolve, reject in
      let decoder = JSONDecoder()
      decoder.dateDecodingStrategy = .formatted(.jsonDateFormatter)
      
      do {
        let places = try decoder.decode([Place].self, from: data)
        resolve(places)
      } catch {
        reject(error)
      }
    })
  }
}
