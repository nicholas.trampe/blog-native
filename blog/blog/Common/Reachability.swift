//
//  Reachability.swift
//  blog
//
//  Created by Nicholas Trampe on 9/8/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

// I would rather not use CocoaPods for this project, for now we'll use this simple solution
// https://stackoverflow.com/questions/30743408/check-for-internet-connection-with-swift

import Foundation
import SystemConfiguration

protocol Reachability {
  var isReachable: Bool { get }
}

class WWReachability: Reachability {
  var isReachable: Bool {
    get {
      var zeroAddress = sockaddr_in()
      zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
      zeroAddress.sin_family = sa_family_t(AF_INET)
      
      guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
          SCNetworkReachabilityCreateWithAddress(nil, $0)
        }
      }) else {
        return false
      }
      
      var flags: SCNetworkReachabilityFlags = []
      if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
        return false
      }
      
      let isReachable = flags.contains(.reachable)
      let needsConnection = flags.contains(.connectionRequired)
      
      return (isReachable && !needsConnection)
    }
  }
}
