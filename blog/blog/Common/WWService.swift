//
//  WWService.swift
//  blog
//
//  Created by Nicholas Trampe on 8/12/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import Foundation
import Promises

private struct ServerMessage: Codable {
  let message: String
}

class WWService: Service {
  public let baseURL: String
  private var token: String? {
    get {
      return UserDefaults.standard.string(forKey: "AuthenticationToken")
    }
    
    set {
      UserDefaults.standard.set(newValue, forKey: "AuthenticationToken")
    }
  }
  
  init(baseURL: String) {
    self.baseURL = baseURL
  }
  
  func get(route: String) -> Promise<Data> {
    return Promise<Data> { resolve, reject in
      guard let url = URL(string: self.baseURL + route) else {
        reject(ServiceError.invalidUrl)
        return
      }
      
      let session = URLSession.shared
      
      session.dataTask(with: url) { (data, response, error) in
        if let error = error {
          reject(ServiceError.sessionError(reason: error.localizedDescription))
        } else {
          if let data = data {
            resolve(data)
          } else {
            reject(ServiceError.invalidData)
          }
        }
        }.resume()
    }
  }
  
  func post(route: String, body: Any) -> Promise<Data> {
    return Promise<Data> { resolve, reject in 
      guard let url = URL(string: self.baseURL + route) else {
        reject(ServiceError.invalidUrl)
        return
      }
      
      var request = URLRequest(url: url)
      let session = URLSession.shared
      
      request.httpMethod = "POST"
      request.addValue("application/json", forHTTPHeaderField: "Content-Type")
      request.addValue("application/json", forHTTPHeaderField: "Accept")
      
      if let token = self.token {
        request.addValue(token, forHTTPHeaderField: "x-access-token")
      }
      
      request.httpBody = try! JSONSerialization.data(withJSONObject: body, options: [])
      
      session.dataTask(with: request) { (data, response, error) in
        if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
          if let data = data {
            let decoder = JSONDecoder()
            
            do {
              let message = try decoder.decode(ServerMessage.self, from: data)
              reject(ServiceError.serverError(message: message.message))
            } catch {
              reject(ServiceError.invalidData)
            }
          } else {
            reject(ServiceError.invalidData)
          }
        } else {
          if let error = error {
            reject(ServiceError.sessionError(reason: error.localizedDescription))
          } else {
            if let data = data {
              resolve(data)
            } else {
              reject(ServiceError.invalidData)
            }
          }
        }
      }.resume()
    }
  }
  
  func authenticate(username: String, password: String) -> Promise<Void> {
    return Promise<Void>({ resolve, reject in
      self.getToken(username: username, password: password).then({ token in
        self.token = token
        resolve(())
      }).catch({ error in
        reject(error)
      })
    })
  }
  
  private func getToken(username: String, password: String) -> Promise<String> {
    let body: [String:String] = ["username": username,
                                 "password": password]
    return Promise<String>({ resolve, reject in
      self.post(route: "authenticate", body: body).then({ data in
        if let token = String(data: data, encoding: .utf8) {
          resolve(token)
        } else {
          reject(ServiceError.invalidData)
        }
      }).catch({ error in 
        reject(error)
      })
    })
  }
}
