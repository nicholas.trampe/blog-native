//
//  PhotosCollection.swift
//  blog
//
//  Created by Nicholas Trampe on 8/2/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

protocol PhotosCollection {
  func insert(image: UIImage)
  func removeAll()
}

class WWPhotosCollection: UICollectionView, PhotosCollection {
  private let photoPadding: CGFloat = 2
  private let minimumPhotosPerRow = 3
  private let maximumPhotoSize: CGFloat = 150
  private let photosLayout: UICollectionViewFlowLayout
  private var images: [UIImage] = []
  
  public var height: CGFloat {
    get {
      return collectionViewLayout.collectionViewContentSize.height
    }
  }
  
  init() {
    photosLayout = UICollectionViewFlowLayout()
    photosLayout.itemSize = CGSize(width: maximumPhotoSize, height: maximumPhotoSize)
    photosLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    photosLayout.scrollDirection = .vertical
    photosLayout.minimumLineSpacing = photoPadding
    photosLayout.minimumInteritemSpacing = photoPadding
    super.init(frame: .zero, collectionViewLayout: photosLayout)
    
    configureViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func insert(image: UIImage) {
    images.append(image)
    insertItems(at: [IndexPath(row: images.count-1, section: 0)])
  }
  
  func removeAll() {
    images.removeAll()
    reloadData()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()

    var nPerRow = max(CGFloat(minimumPhotosPerRow), floor(self.frame.size.width / maximumPhotoSize))
    var itemSize: CGFloat = (self.frame.size.width - (photoPadding*nPerRow)) / nPerRow
    
    while itemSize > maximumPhotoSize {
      nPerRow += 1
      itemSize = (self.frame.size.width - (photoPadding*nPerRow)) / nPerRow
    }
    
    photosLayout.itemSize = CGSize(width: itemSize, height: itemSize)
  }
  
  private func configureViews() {
    translatesAutoresizingMaskIntoConstraints = false
    register(PhotoCell.self, forCellWithReuseIdentifier: "PhotoCell")
    backgroundColor = .clear
    dataSource = self
    delegate = self
  }
}


private class PhotoCell: UICollectionViewCell {
  let imageView = UIImageView(frame: .zero)
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configureViews() {
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.contentMode = .scaleAspectFill
    imageView.clipsToBounds = true
    contentView.addSubview(imageView)
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
      imageView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
      imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
      imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
      ])
  }
}

extension WWPhotosCollection: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return images.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
    
    cell.imageView.image = images[indexPath.row]
    
    return cell
  }
}

extension WWPhotosCollection: UICollectionViewDelegate {
  
}
