//
//  DataCache.swift
//  blog
//
//  Created by Nicholas Trampe on 9/8/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import Foundation
import Promises

enum DataCacheError: Error {
  case invalidUrl
  case invalidData
}

protocol DataCache {
  var name: String { get }
  func saveData(data: Data) -> Promise<Data>
  func loadData() -> Promise<Data>
  func clearData() -> Promise<Void>
}

class WWDataCache: DataCache {
  private let cacheUrl: URL?
  let name: String
  
  init(name: String, directory: CacheDirectory) {
    self.name = name
    self.cacheUrl = directory.url(for: name)
  }
  
  func saveData(data: Data) -> Promise<Data> {
    return Promise<Data>({ resolve, reject in 
      guard let url = self.cacheUrl else {
        throw DataCacheError.invalidUrl
      }
      
      do {
        try data.write(to: url)
        resolve(data)
      } catch {
        throw error
      }
    })
  }
  
  func loadData() -> Promise<Data> {
    return Promise<Data>({ resolve, reject in 
      guard let url = self.cacheUrl else {
        throw DataCacheError.invalidUrl
      }
      
      do {
        let data = try Data(contentsOf: url)
        resolve(data)
      } catch {
        throw error
      }
    })
  }
  
  func clearData() -> Promise<Void> {
    return Promise<Void>({ resolve, reject in 
      guard let url = self.cacheUrl else {
        throw DataCacheError.invalidUrl
      }
      
      do {
        if FileManager.default.fileExists(atPath: url.path) {
          try FileManager.default.removeItem(at: url)
        }
        resolve(())
      } catch {
        throw error
      }
    })
  }
}

