//
//  SlideUpContentView.swift
//  blog
//
//  Created by Nicholas Trampe on 8/2/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

private enum ContentState {
  case hidden
  case peeking
  case expanded
  case viewing
  case sitting
}

extension UIView {
  func addSlideUpView(_ child: SlideUpContentView) {
    addSubview(child)
    
    NSLayoutConstraint.activate([
      child.topAnchor.constraint(equalTo: self.bottomAnchor),
      child.leftAnchor.constraint(equalTo: self.leftAnchor),
      child.rightAnchor.constraint(equalTo: self.rightAnchor),
      ])
  }
}

protocol SlideUpContentViewObserver: class {
  func didPeek()
  func didExpand()
  func didSit()
  func didHide()
}

class SlideUpContentView: UIView {
  public weak var observer: SlideUpContentViewObserver? = nil
  public let peekView = UIView(frame: .zero)
  public let contentView = UIView(frame: .zero)
  public var peekPadding: CGFloat = 0
  public var expandedHeightTopPadding: CGFloat = 0
  
  private let contentPadding: CGFloat = 15
  private let upperTab = PullTab()
  private let lowerTab = PullTab()
  private var state: ContentState = .hidden
  private let translationThreshold: CGFloat = 200
  private let velocityThreshold: CGFloat = 200
  private var updatedTransform: CGAffineTransform = .identity
  private var contentPeekHeight: CGFloat {
    get {
      return -(peekView.frame.size.height + upperTab.frame.size.height + contentPadding*2 + peekPadding)
    }
  }
  private var contentExpandedHeight: CGFloat {
    get {
      guard let superview = superview else {
        return 0
      }
      
      return max(-superview.frame.size.height + expandedHeightTopPadding, -self.frame.size.height)
    }
  }
  private var contentSittingHeight: CGFloat {
    get {
      return -self.frame.size.height
    }
  }
  
  init() {
    super.init(frame: .zero)
    
    configure()
    constrain()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func hide() {
    updatedTransform = .identity
    state = .hidden
    
    UIView.animate(withDuration: 0.3, animations: { 
      self.transform = self.updatedTransform
      self.alpha = 0.0
    }) { _ in
      self.observer?.didHide()
    }
  }
  
  func peek() {
    layer.removeAllAnimations()
    layoutIfNeeded()
    updatedTransform = CGAffineTransform(translationX: 0, y: contentPeekHeight)
    state = .peeking
    
    UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: { 
      self.transform = self.updatedTransform
      self.alpha = 1.0
    }) { _ in
      self.observer?.didPeek()
    }
  }
  
  private func expand() {
    updatedTransform = CGAffineTransform(translationX: 0, y: contentExpandedHeight)
    state = .expanded
    
    UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: [], animations: { 
      self.transform = self.updatedTransform
    }) { _ in
      self.observer?.didExpand()
    }
  }
  
  private func sit() {
    updatedTransform = CGAffineTransform(translationX: 0, y: contentSittingHeight)
    state = .sitting
    
    UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: { 
      self.transform = self.updatedTransform
    }) { _ in
      self.observer?.didSit()
    }
  }
  
  private func slideUpAndOut() {
    guard let superview = superview else {
      preconditionFailure("Attempting to animate when not in a view")
    }
    
    updatedTransform = .identity
    state = .hidden
    
    UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: [], animations: { 
      self.transform = CGAffineTransform(translationX: 0, y: -superview.frame.size.height - self.frame.size.height)
      self.alpha = 0.0
    }) { _ in
      self.transform = .identity
      self.updatedTransform = .identity
      self.observer?.didHide()
    }
  }
  
  @objc private func handlePan(sender: UIPanGestureRecognizer) {
    guard let superview = superview else {
      preconditionFailure("Attempting to animate when not in a view")
    }
    
    let translation = sender.translation(in: superview).y
    let velocity = sender.velocity(in: superview).y
    
    switch sender.state {
    case .began,.changed:
      self.transform = self.updatedTransform.translatedBy(x: 0, y: translation)
    case .ended:
      self.updatedTransform = self.transform
      
      if state == .expanded && self.updatedTransform.ty < contentExpandedHeight {
        state = .viewing
      } else if state == .sitting && self.updatedTransform.ty > contentSittingHeight {
        state = .viewing
      }
      
      switch state {
      case .hidden:
        break
      case .peeking:
        if translation > translationThreshold || velocity > velocityThreshold {
          hide()
        } else if translation < -translationThreshold || velocity < -velocityThreshold {
          expand()
        } else {
          peek()
        }
      case .expanded:
        if translation > translationThreshold || velocity > velocityThreshold {
          hide()
        } else {
          expand()
        }
      case .viewing:
        if self.updatedTransform.ty < contentSittingHeight {
          sit()
        } else if self.updatedTransform.ty > contentExpandedHeight {
          expand()
        }
      case .sitting:
        if velocity < -velocityThreshold {
          slideUpAndOut()
        } else {
          sit()
        }
      }
      
    default:
      break
    }
  }
  
  @objc private func handleTap(sender: UITapGestureRecognizer) {
    if sender.state == .ended {
      if state == .peeking {
        expand()
      }
    }
  }
  
  private func configure() {
    backgroundColor = .white
    layer.cornerRadius = 15.0
    
    addSubview(upperTab)
    addSubview(lowerTab)
    
    peekView.translatesAutoresizingMaskIntoConstraints = false
    peekView.backgroundColor = .clear
    addSubview(peekView)
    
    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.backgroundColor = .clear
    addSubview(contentView)
    
    let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(sender:)))
    addGestureRecognizer(pan)
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
    addGestureRecognizer(tap)
  }
  
  private func constrain() {
    NSLayoutConstraint.activate([
      upperTab.topAnchor.constraint(equalTo: self.topAnchor, constant: contentPadding),
      upperTab.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      
      peekView.topAnchor.constraint(equalTo: upperTab.bottomAnchor, constant: contentPadding),
      peekView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: contentPadding),
      peekView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -contentPadding),
      
      contentView.topAnchor.constraint(equalTo: peekView.bottomAnchor, constant: contentPadding),
      contentView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: contentPadding),
      contentView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -contentPadding),
      
      lowerTab.topAnchor.constraint(equalTo: contentView.bottomAnchor, constant: contentPadding),
      lowerTab.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -contentPadding),
      lowerTab.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      ])
  }
}

private class PullTab: UIView {
  init() {
    super.init(frame: .zero)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configureViews() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = .lightGray
    layer.cornerRadius = 2.0
  }
  
  private func constrainViews() {
    NSLayoutConstraint.activate([
      widthAnchor.constraint(equalToConstant: 50),
      heightAnchor.constraint(equalToConstant: 5),
      ])
  }
}
