//
//  Configuration.swift
//  blog
//
//  Created by Nicholas Trampe on 7/8/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

class Configuration {
  static var dictionary: [String : Any] {
    get {
      guard let path = Bundle.main.path(forResource: "config", ofType: "plist"),
      let dict = NSDictionary(contentsOfFile: path) as? [String:Any] else {
              return [:]
      }
      
      return dict
    }
  }
  
  static var baseURL: String = Configuration.dictionary["baseURL"] as! String
  static var mapsKey: String = Configuration.dictionary["mapsKey"] as! String
}
