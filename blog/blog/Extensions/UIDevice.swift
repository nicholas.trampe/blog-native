//
//  UIDevice.swift
//  blog
//
//  Created by Nicholas Trampe on 7/30/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

extension UIDevice {
  static var isPhone: Bool {
    get {
      return UIDevice.current.userInterfaceIdiom == .phone
    }
  }
  
  static var isPad: Bool {
    get {
      return UIDevice.current.userInterfaceIdiom == .pad
    }
  }

  static func versionUnder(_ version: String) -> Bool {
    let result = UIDevice.current.systemVersion.compare(version, options: .numeric)
    return result == .orderedAscending
  }
}
