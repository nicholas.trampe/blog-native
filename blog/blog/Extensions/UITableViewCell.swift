//
//  UITableViewCell.swift
//  blog
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

extension UITableViewCell {
  static var defaultCellIdentifier: String {
    get {
      return String(describing: self)
    }
  }
}
