//
//  UIImageView.swift
//  blog
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

extension UIImageView {
  func setImage(from url: URL) {
    URLSession.shared.dataTask(with: url) {
      (data, response, error) in
        guard let data = data, error == nil else { return }
        DispatchQueue.main.async() { () -> Void in
          self.image = UIImage(data: data)
        }
      }.resume()
  }
}
