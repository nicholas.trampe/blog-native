//
//  NSAttributedString.swift
//  blog
//
//  Created by Nicholas Trampe on 8/22/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

extension NSAttributedString {
  convenience init(string: String, font: UIFont?, color: UIColor) {
    var attributeFont = UIFont.systemFont(ofSize: 12)
    
    if let font = font {
      attributeFont = font
    }
    
    self.init(string: string, attributes: [NSAttributedString.Key.font:attributeFont, NSAttributedString.Key.foregroundColor:color])
  }
  
  convenience init(numberOfReturns: Int) {
    self.init(string: String(repeating: "\n", count: numberOfReturns))
  }
}
