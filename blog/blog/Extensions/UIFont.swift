//
//  UIFont.swift
//  blog
//
//  Created by Nicholas Trampe on 8/13/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

extension UIFont {
  static func openSans(of size: CGFloat) -> UIFont {
    return UIFont(name: "OpenSans", size: size)!
  }
  
  static func openSansBold(of size: CGFloat) -> UIFont {
    return UIFont(name: "OpenSans-Bold", size: size)!
  }
  
  static func openSansSemiBold(of size: CGFloat) -> UIFont {
    return UIFont(name: "OpenSans-Semibold", size: size)!
  }
  
  static func openSansExtraBold(of size: CGFloat) -> UIFont {
    return UIFont(name: "OpenSans-ExtraBold", size: size)!
  }
  
  static func openSansLight(of size: CGFloat) -> UIFont {
    return UIFont(name: "OpenSans-Light", size: size)!
  }
  
  static func printFontFamilies() {
    for family: String in UIFont.familyNames {
      print("\(family)")
      for names: String in UIFont.fontNames(forFamilyName: family) {
        print("== \(names)")
      }
    }
  }
}

enum FontAttributes {
  case heading1
  case heading2
  case heading3
  case heading4
  case heading5
  case heading6
  case heading7
  
  case body
  
  case iconExtraLarge
  case iconLarge
  case iconMedium
  case iconSmall
}

extension FontAttributes {
  var size: CGFloat {
    get {
      switch self {
      case .heading1:
        return UIDevice.isPhone ? 24 : 32
      case .heading2:
        return UIDevice.isPhone ? 22 : 30
      case .heading3:
        return UIDevice.isPhone ? 20 : 28
      case .heading4:
        return UIDevice.isPhone ? 18 : 26
      case .heading5:
        return UIDevice.isPhone ? 16 : 24
      case .heading6:
        return UIDevice.isPhone ? 14 : 22
      case .heading7:
        return UIDevice.isPhone ? 12 : 20
      case .body:
        return UIDevice.isPhone ? 18 : 26
      case .iconExtraLarge:
        return UIDevice.isPhone ? 96 : 128
      case .iconLarge:
        return UIDevice.isPhone ? 64 : 96
      case .iconMedium:
        return UIDevice.isPhone ? 32 : 64
      case .iconSmall:
        return UIDevice.isPhone ? 24 : 32
      }
    }
  }
}
