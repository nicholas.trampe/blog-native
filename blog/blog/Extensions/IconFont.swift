//
//  IconFont.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

enum IconFontIcon: String {
  case earth = "\u{e9ca}"
  case info = "\u{ea0c}"
  case arrowLeft = "\u{ea40}"
  case arrowRight = "\u{ea3c}"
  case mail = "\u{ea86}"
  case facebook = "\u{ea91}"
  case instagram = "\u{ea92}"
  case git = "\u{eae7}"
  case menu = "\u{e9bd}"
  case compass = "\u{e94a}"
  case lifebuoy = "\u{e941}"
  case bullhorn = "\u{e91a}"
  case cancel = "\u{ea0d}"
  case apple = "\u{eabe}"
  case safari = "\u{eadd}"
  case cog = "\u{e994}"
}

extension UIFont {
  static func iconFont(of size: CGFloat) -> UIFont? {
    return UIFont(name: "icomoon", size: size)
  }
}

extension UILabel {
  static func iconLabel(of size: CGFloat, with icon: IconFontIcon) -> UILabel {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.textAlignment = .center
    label.font = UIFont.iconFont(of: size)
    label.text = icon.rawValue
    return label
  }
  
  var icon: IconFontIcon? {
    get {
      if let text = self.text {
        return IconFontIcon(rawValue: text)
      }
      return nil
    }
    set {
      self.text = newValue?.rawValue
    }
  }
}

extension UIButton {
  static func iconButton(of size: CGFloat, with icon: IconFontIcon) -> UIButton {
    let button = UIButton(type: .custom)
    button.translatesAutoresizingMaskIntoConstraints = false
    button.setTitle(icon.rawValue, for: .normal)
    button.titleLabel?.font = UIFont.iconFont(of: size)
    return button
  }
}
