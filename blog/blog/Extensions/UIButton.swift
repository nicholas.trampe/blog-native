//
//  UIButton.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

enum ButtonStyle {
  case plain
  case primary
}

extension UIButton {
  convenience init(style: ButtonStyle) {
    self.init(frame: .zero)
    
    translatesAutoresizingMaskIntoConstraints = false
    setTitleColor(textColor(for: style, state: .normal), for: .normal)
    backgroundColor = fillColor(for: style)
    titleLabel?.font = .openSansSemiBold(of: 20)
    layer.cornerRadius = 5.0
    layer.borderColor = UIColor.lightGray.cgColor
    layer.borderWidth = 1.0
  }
}

private func fillColor(for style: ButtonStyle) -> UIColor {
  switch style {
  case .plain:
    return .white
  case .primary:
    return .primaryBlueNormal
  }
}

private func textColor(for style: ButtonStyle, state: UIControl.State) -> UIColor {
  switch style {
  case .plain:
    return .black
  case .primary:
    return .white
  }
}

private func font(for style: ButtonStyle) -> UIFont {
  switch style {
  case .plain, .primary:
    return .openSansSemiBold(of: 20)
  }
}
