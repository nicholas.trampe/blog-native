//
//  UIColor.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

import UIKit

extension UIColor {
  static var primaryBlueNormal: UIColor {
    get {
      return UIColor(red:0.20, green:0.48, blue:0.72, alpha:1.00)
    }
  }
  
  static var primaryBlueHighlighted: UIColor {
    get {
      return UIColor(red:0.16, green:0.38, blue:0.56, alpha:1.00)
    }
  }
  
  static var spaceBlue: UIColor {
    get {
      return UIColor(red:0.03, green:0.13, blue:0.33, alpha:1.00)
    }
  }
  
  static var oceanBlue: UIColor {
    get {
      return UIColor(red: 0.122, green: 0.439, blue: 0.655, alpha: 1.000)
    }
  }
  
  static var iceWhite: UIColor {
    get {
      return .white
    }
  }
  
  static var landGreen: UIColor {
    get {
      return UIColor(red: 0.420, green: 0.855, blue: 0.298, alpha: 1.000)
    }
  }
  
  static var borderPurple: UIColor {
    get {
      return UIColor(red: 0.051, green: 0.078, blue: 0.180, alpha: 1.000)
    }
  }
  
  static var backgroundColor: UIColor {
    get {
      if #available(iOS 13.0, *) {
        return UIColor(named: "backgroundColor")!
      }

      return .white
    }
  }
  
  static var foregroundColor: UIColor {
    get {
      if #available(iOS 13.0, *) {
        return UIColor(named: "foregroundColor")!
      }

      return .black
    }
  }
}
