//
//  ScreenshotTests.swift
//  blogUITests
//
//  Created by Nicholas Trampe on 7/6/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import XCTest

extension XCUIElement {
  func scrollToElement(element: XCUIElement) {
    while !element.visible() {
      swipeUp()
    }
  }
  
  func visible() -> Bool {
    guard self.exists && !self.frame.isEmpty else { return false }
    return XCUIApplication().windows.element(boundBy: 0).frame.contains(self.frame)
  }
}

class ScreenshotTests: XCTestCase {
  
  override func setUp() {
    super.setUp()
    
    let app = XCUIApplication()
    setupSnapshot(app)
    app.launch()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func test_MainView() {
    sleep(2)
    
    snapshot("snap0_MainScreen")
  }
  
  func test_PostViewTop() {
    sleep(2)
    
    let app = XCUIApplication()
    let mainTable = app.tables["Main Table"]
    let cell = mainTable.staticTexts["Rebuilding A School in Sindhupalchok, Nepal"]
    
    mainTable.scrollToElement(element: cell)
    
    cell.tap()
    
    sleep(4)
    
    snapshot("snap1_PostTop")
  }
  
  func test_PlacesViewMap() {
    let app = XCUIApplication()
    
    sleep(2)
    
    app.buttons["Places Button"].tap()
    
    sleep(2)
    
    let map = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element
    
    map.tap()
    map.tap()
    
    sleep(1)
    
    snapshot("snap3_PlacesMap")
  }
  
  func test_PlacesViewMarker() {
    let app = XCUIApplication()
    
    sleep(2)
    
    app.buttons["Places Button"].tap()
    
    sleep(2)
    
    let map = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element
    let marker = app.buttons["Marker Sindhupalchok, Nepal"]
    
    while !marker.visible() {
      map.swipeRight()
      sleep(1)
    }
    
    marker.tap()
    
    sleep(3)
    
    app.otherElements["Places Content View"].tap()
    
    sleep(1)
    
    snapshot("snap4_PlacesMarker")
  }
}
