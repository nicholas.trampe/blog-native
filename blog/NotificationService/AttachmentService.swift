//
//  AttachmentService.swift
//  NotificationService
//
//  Created by Nicholas Trampe on 7/8/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit
import UserNotifications

enum ResultDataType {
  case success(data: Data)
  case failure
}

enum ResultAttachmentType {
  case success(attachment: UNNotificationAttachment)
  case failure
}

typealias AttachmentServiceBlock = (ResultAttachmentType) -> ()

protocol AttachmentService {
  func get(id: String, completion: @escaping AttachmentServiceBlock)
}

class WWAttachmentService: AttachmentService {
  public let baseURL: String
  
  init(baseURL: String) {
    self.baseURL = baseURL
  }
  
  func get(id: String, completion: @escaping AttachmentServiceBlock) {
    get(route: "photos/" + id) { (result) in
      switch (result) {
      case .success(let data):
        if let attachment = self.save(data: data) {
          completion(.success(attachment: attachment))
        } else {
          completion(.failure)
        }
      case .failure:
        completion(.failure)
      }
    }
  }
  
  private func get(route: String, completion: @escaping (ResultDataType) -> ()) {
    guard let url = URL(string: baseURL + route) else {
      completion(.failure)
      return
    }
    
    let session = URLSession.shared
    
    session.dataTask(with: url) { (data, response, error) in
      if error != nil {
        DispatchQueue.main.async {
          completion(.failure)
        }
        return
      }
      
      if let data = data {
        DispatchQueue.main.async {
          completion(ResultDataType.success(data: data))
        }
      } else {
        DispatchQueue.main.async {
          completion(.failure)
        }
      }
      }.resume()
  }
  
  private func save(data: Data) -> UNNotificationAttachment? {
    var urlPath = URL(fileURLWithPath: NSTemporaryDirectory())
    let uniqueURLEnding = ProcessInfo.processInfo.globallyUniqueString + ".jpg"
    urlPath = urlPath.appendingPathComponent(uniqueURLEnding)
    
    try? data.write(to: urlPath)
    
    do {
      let attachment = try UNNotificationAttachment(identifier: "picture", url: urlPath, options: nil)
      return attachment
    }
    catch {
      return nil
    }
  }
}
