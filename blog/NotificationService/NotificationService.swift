//
//  NotificationService.swift
//  NotificationService
//
//  Created by Nicholas Trampe on 7/8/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {
  
  var contentHandler: ((UNNotificationContent) -> Void)?
  var bestAttemptContent: UNMutableNotificationContent?
  
  override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
    self.contentHandler = contentHandler
    bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
    
    guard let bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent),
      let apsData = bestAttemptContent.userInfo["aps"] as? [String: Any],
      let photoId = apsData["photo-id"] as? String else {
        return
    }
    
    let attachmentService = WWAttachmentService(baseURL: Configuration.baseURL)
    
    attachmentService.get(id: photoId) { (result) in
      switch (result) {
      case .success(let attachment):
        bestAttemptContent.attachments = [attachment]
        contentHandler(bestAttemptContent)
      case .failure:
        contentHandler(bestAttemptContent)
      }
    }
  }
  
  override func serviceExtensionTimeWillExpire() {
    // Called just before the extension will be terminated by the system.
    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
    if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
      contentHandler(bestAttemptContent)
    }
  }
  
}
